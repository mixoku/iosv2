//
//  OrderTests.swift
//  CafeAppTests
//
//  Created by Alexey Antonov on 24/01/21.
//

import XCTest

@testable import CafeApp
class OrderTests: XCTestCase {
    let pizza = Dish(
        id: 0,
        title: "Пицца маринара",
        weight: 240,
        descriptionShort: "Обычная такая себе пицца",
        descriptionFull: "",
        price: 320,
        measure: "г",
        images: [],
        optionGroups: [
            Dish.OptionGroup(
                id: 0,
                title: "Размер",
                description: "",
                required: 1,
                type: "",
                options: [
                    Dish.OptionGroup.Option(id: 0, value: "28 см", price: 0, isDefault: 1, isPlural: 0),
                    Dish.OptionGroup.Option(id: 1, value: "30 см", price: 50, isDefault: 0, isPlural: 0)
                ]),
            Dish.OptionGroup(
                id: 1,
                title: "Добавки",
                description: "",
                required: 0,
                type: "",
                options: [
                    Dish.OptionGroup.Option(id: 2, value: "Грибы", price: 30, isDefault: 0, isPlural: 1),
                    Dish.OptionGroup.Option(id: 3, value: "Анчоусы", price: 50, isDefault: 0, isPlural: 1),
                    Dish.OptionGroup.Option(id: 4, value: "Креветки", price: 70, isDefault: 0, isPlural: 1)
                ])])

    func testDishQuantity() throws {
        let dish = CartOrderItem(dish: pizza, count: 1, options: [])
        
        XCTAssertEqual(dish.count, 1)
        
        let dish2 = CartOrderItem(dish: pizza, count: 3, options: [])
        
        XCTAssertEqual(dish2.count, 3)
    }
    
    func testDishPrice() throws {
        let dish = CartOrderItem(dish: pizza, count: 1, options: [])
        
        XCTAssertEqual(dish.totalPrice, 320)
        
        let dish2 = CartOrderItem(dish: pizza, count: 3, options: [])
        
        XCTAssertEqual(dish2.totalPrice, 960)
    }
    
    func testDishOptions() throws {
        let dish = CartOrderItem(dish: pizza, count: 1, options: [])
        let dish2 = CartOrderItem(dish: pizza, count: 1, options: [Dish.OptionGroup.Option(id: 1, value: "30 см", price: 50, isDefault: 0, isPlural: 0), Dish.OptionGroup.Option(id: 3, value: "Анчоусы", price: 50, isDefault: 0, isPlural: 1)])
        
        XCTAssertEqual(dish.totalPrice, 320)
        XCTAssertEqual(dish2.totalPrice, 420)
    }

}
