//
//  FakeDataProvider.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import Foundation

let theRestaurant = Restaurant(id: 1, title: "Пушкин", description: "", siteUrl: "", logoUrl: "", colorScheme: "", phone: "", aboutCompany: "", workTime: "", aboutDelivery: "", deliveryTime: "", images: [])

let restaurantsSample: [Restaurant] = [
    Restaurant(id: 0, title: "Johnson & Johnson Суши и пицца", description: "Ресторан доставки горячей пиццы и роллов для всей семьи.", siteUrl: "url", logoUrl: "imageExample", colorScheme: "BADA55", phone: "88000-533-3535", aboutCompany: "kruto delaut", workTime: "ПН — ПТ 9:00 — 22:00", aboutDelivery: "fast and furious", deliveryTime: "till midnight", images: []),
    Restaurant(id: 1, title: "Sushi Daily", description: "Ресторан доставки горячей пиццы и роллов для всей семьи.", siteUrl: "url", logoUrl: "imageExample", colorScheme: "BA3355", phone: "88000-533-3535", aboutCompany: "kruto delaut", workTime: "ПН — ПТ 9:00 — 22:00", aboutDelivery: "fast and furious", deliveryTime: "till midnight", images: []),
    Restaurant(id: 3, title: "HAHAHA", description: "Ресторан доставки горячей пиццы и роллов для всей семьи.", siteUrl: "url", logoUrl: "imageExample", colorScheme: "BA0F55", phone: "88000-533-3535", aboutCompany: "kruto delaut", workTime: "ПН — ПТ 9:00 — 22:00", aboutDelivery: "fast and furious", deliveryTime: "till midnight", images: [])
]

let promosSample: [Promo] = [
    Promo(id: 0,
          title: "Пицца Маргарита \nв подарок 0",
          imagePath: "imageExample",
          subtitle: "При заказе через приложение любых блюд стоимостью на 800 ₽, дарим подарок: Пицца Маргарита 30 см",
          description: "С 1.12.2020 по 20.12.2020 во всех кафе, если продукт имеется в наличии. Подробные правила акции смотрите на сайте."),
    Promo(id: 1,
          title: "Пицца Маргарита \nв подарок 1",
          imagePath: "imageExample",
          subtitle: "При заказе через приложение любых блюд стоимостью на 800 ₽, дарим подарок: Пицца Маргарита 30 см",
          description: "С 1.12.2020 по 20.12.2020 во всех кафе, если продукт имеется в наличии. Подробные правила акции смотрите на сайте."),
    Promo(id: 2,
          title: "Пицца Маргарита \nв подарок 2",
          imagePath: "imageExample",
          subtitle: "При заказе через приложение любых блюд стоимостью на 800 ₽, дарим подарок: Пицца Маргарита 30 см",
          description: "С 1.12.2020 по 20.12.2020 во всех кафе, если продукт имеется в наличии. Подробные правила акции смотрите на сайте.")
]

let pizza = Dish(
    id: 0,
    title: "Пицца маринара",
    weight: 240,
    descriptionShort: "Обычная такая себе пицца",
    descriptionFull: "",
    price: 320,
    measure: "г",
    images: [],
    optionGroups: [
        Dish.OptionGroup(
            id: 0,
            title: "Размер",
            description: "",
            required: 1,
            type: "",
            options: [
                Dish.OptionGroup.Option(id: 0, value: "28 см", price: 0, isDefault: 1, isPlural: 0),
                Dish.OptionGroup.Option(id: 1, value: "30 см", price: 50, isDefault: 0, isPlural: 0)
            ]),
        Dish.OptionGroup(
            id: 1,
            title: "Добавки",
            description: "",
            required: 0,
            type: "",
            options: [
                Dish.OptionGroup.Option(id: 2, value: "Грибы", price: 30, isDefault: 0, isPlural: 1),
                Dish.OptionGroup.Option(id: 3, value: "Анчоусы", price: 50, isDefault: 0, isPlural: 1),
                Dish.OptionGroup.Option(id: 4, value: "Креветки", price: 70, isDefault: 0, isPlural: 1)
            ])])


let fakeOrder = Order(id: 100, comment: "order", status: "created", dishes: [Order.OrderDish(dishId: 20200, title: "pizza", price: 2100, count: 2, options: [])], deliveryAddressId: 29, sum: 1890, deliveryInfo: Order.DeliveryInfo(isFree: false, sum: 300), createdAt: 4, updatedAt: 39, deliveryAddress: Order.Address(address: "Hohlovskaya 20"))
