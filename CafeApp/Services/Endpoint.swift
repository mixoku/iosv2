//
//  Endpoint.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import Foundation
import Combine

var decoder: JSONDecoder {
    let theDecoder = JSONDecoder()
    theDecoder.keyDecodingStrategy = .convertFromSnakeCase
    
    return theDecoder
    
}

extension JSONDecoder {
    func decode(_ type: Decodable.Protocol, from: Data) throws -> Decodable {
        if type != PaymentCard.self {
            return try self.decode(type, from: from)
        } else {
            return try self.decode(type, from: from)
        }
    }
}

var encoder: JSONEncoder {
    let theEncoder = JSONEncoder()
    theEncoder.keyEncodingStrategy = .convertToSnakeCase
    
    return theEncoder
}



var yandexAPIKey: String { "a0c13b2d-2632-4a5d-a018-5339a4704a05" }

enum Endpoint {
    // Auth routes
    case requestCode(phone: String)
    case login(phone: String, code: String)
    case logout
    
    // Menu routes
    case restaurant(id: Int)
    case restaurantMenu(restaurantId: Int)
    case addressInZone(id: Int, lat: String, lng: String)
    
    // User restaurants routes
    case getUserRestaurants
    case addRestaurant(id: Int)
    case deleteRestaurant(id: Int)
    
    // Service zone routes
    case getServiceZone(restaurantId: Int)
    
    // Profile routes
    case getProfile
    case updateProfile(user: User)
    case getAddresses
    case deleteAddress(id: Int)
    case addAddress(address: DeliveryAddress)
    
    // Order routes
    case orderHistory(restaurantId: Int)
    case cancelOrder(restaurantId: Int, orderId: Int)
    case calcOrder(restaurantId: Int, order: Order)
    case sendOrder(restaurantId: Int, order: Order)
    
    // Payment routes
    case orderPayment(restaurantId: Int, orderId: Int)
    case paymentSuccess(url: URL)
    case addCard
    case cardsList
    case removeCard(id: Int)
    
    // Geocoder
    case findAddress(lat: String, lng: String)
    case findPoint(address: String)
    
    private var baseUrl: URL { URL(string: "https://stage-api-v10-myc.wwda.ru/1.0")! }
    
    /// Get full URL of the selected route
    var url: URL {
        switch self {
        case .requestCode(_):
            return URL(string: "\(baseUrl)/auth/send-confirm-code")!
        case .login(_, _):
            return URL(string:"\(baseUrl)/auth/login")!
        case .logout:
            return URL(string:"\(baseUrl)/auth/logout")!
        case .getUserRestaurants:
            return URL(string: "\(baseUrl)/user/restaurants")!
        case .addRestaurant(let id):
            return URL(string: "\(baseUrl)/user/restaurant/\(id)/add")!
        case .deleteRestaurant(let id):
            return URL(string: "\(baseUrl)/user/restaurant/\(id)/")!
        case .restaurant(let id):
            return URL(string: "\(baseUrl)/restaurant/\(id)")!
        case .restaurantMenu(let id):
            return URL(string: "\(baseUrl)/restaurant/\(id)/menu")!
        case .getServiceZone(let restaurantId):
            return URL(string: "\(baseUrl)/restaurant/\(restaurantId)/service-zone")!
        case .getProfile, .updateProfile(_):
            return URL(string: "\(baseUrl)/user/me")!
        case .calcOrder(let id, _):
            return URL(string: "\(baseUrl)/restaurant/\(id)/order-calc")!
        case .sendOrder(let id, _):
            return URL(string: "\(baseUrl)/restaurant/\(id)/order")!
        case .orderPayment(let restaurantId, let id):
            return URL(string: "\(baseUrl)/restaurant/\(restaurantId)/order/\(id)/pay")!
        case .orderHistory(let id):
            return URL(string: "\(baseUrl)/restaurant/\(id)/order")!
        case .cancelOrder(let restaurantId, let orderId):
            return URL(string: "\(baseUrl)/restaurant/\(restaurantId)/order/\(orderId)")!
        case .paymentSuccess(let url):
            return url
        case .cardsList:
            return URL(string: "\(baseUrl)/user/cards")!
        case .addCard:
            return URL(string: "\(baseUrl)/user/card/add")!
        case .removeCard(let id):
            return URL(string: "\(baseUrl)/user/card/\(id)")!
        case .getAddresses, .addAddress(_):
            return URL(string: "\(baseUrl)/user/address")!
        case .deleteAddress(let id):
            return URL(string: "\(baseUrl)/user/address/\(id)")!
        case .addressInZone(let id, _, _):
            return URL(string: "\(baseUrl)/restaurant/\(id)/service-zone")!
        case .findAddress(let lat, let lng):
            return URL(string: "https://geocode-maps.yandex.ru/1.x/?apikey=\(yandexAPIKey)&format=json&geocode=\(lat),\(lng)")!
        case .findPoint(let address):
            return URL(string: "https://geocode-maps.yandex.ru/1.x/?apikey=\(yandexAPIKey)&format=json&geocode=\(address.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)")!
            
        }
    }
    
    /// Get method of the selected route
    var method: String {
        switch self {
        case .login(_, _), .requestCode(_), .updateProfile(_), .addAddress(_), .addressInZone(_, _, _), .calcOrder(_, _), .sendOrder(_, _):
            return "POST"
        case .deleteAddress(_), .cancelOrder(_, _), .removeCard(_), .deleteRestaurant(_):
            return "DELETE"
        default:
            return "GET"
        }
    }
    
    /// Get token if exists
    var token: String {
        UserDefaults.standard.string(forKey: "token") ?? ""
    }
    
    /// Get parameters of the selected route
    private var body: Data? {
        switch self {
        case .requestCode(let phone):
            return try? encoder.encode(["phone": phone])
        case .login(let phone, let code):
            return try? encoder.encode(["phone": phone, "code": code])
        case .updateProfile(let user):
            return try? encoder.encode(["username": user.username ?? "", "email": user.email ?? "", "birthday": user.birthday ?? ""])
        case .addAddress(let address):
            return try? encoder.encode(address)
        case .addressInZone(_, let lat, let lng):
            return try? encoder.encode(["lat": lat, "lon": lng])
        case .calcOrder(_, let order), .sendOrder(_, let order):
            return try? encoder.encode(order)
        default:
            return nil
        }
    }
    
    /// If method requires auth
    var requiresAuth: Bool {
        switch self {
        case .getProfile, .updateProfile, .logout, .orderHistory(_), .getAddresses, .deleteAddress(_), .addAddress(_), .cancelOrder(_, _), .calcOrder(_, _), .sendOrder(_, _), .orderPayment(_, _), .paymentSuccess(_), .addCard, .cardsList, .removeCard(_), .getUserRestaurants, .addRestaurant(_), .deleteRestaurant(_):
            return true
        default:
            return false
        }
    }
    
    /// Get URLRequest for the selected route
    var request: URLRequest {
        var req = URLRequest(url: url)
        req.httpMethod = method
        req.httpBody = body
        req.addValue(
          "application/json",
          forHTTPHeaderField: "Content-Type")
        if self.requiresAuth {
            req.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        return req
    }
    
    /// Basic fetch method for the selected route
    func fetch<T: Decodable>() -> AnyPublisher<T, Error> {
        return
            URLSession.shared.dataTaskPublisher(for: request)
            .map(\.data)
            .decode(type: T.self, decoder: decoder)
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
    
    func getHttpResponse() -> AnyPublisher<HTTPURLResponse?, URLError> {
        URLSession.shared.dataTaskPublisher(for: request)
            .map(\.response)
            .map { response -> HTTPURLResponse? in
                return response as? HTTPURLResponse
            }
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
}

