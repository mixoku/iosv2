//
//  OrderStore.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import Foundation

final class OrderStore: ObservableObject {
    @Published private(set) var dishes = [CartOrderItem]() {
        didSet {
            totalSum = dishes.reduce(0) { $0 + $1.totalPrice }
        }
    }
    
    @Published private(set) var totalSum = 0
    
    func addToCart(cartOrderItem: CartOrderItem) {
        if dishes.contains(where: { $0.dish == cartOrderItem.dish && $0.options == cartOrderItem.options }) {
            dishes = dishes.map { $0.dish == cartOrderItem.dish && $0.options == cartOrderItem.options ? CartOrderItem(dish: cartOrderItem.dish, count: $0.count + cartOrderItem.count, options: cartOrderItem.options) : $0 }
        } else {
            dishes.append(cartOrderItem)
        }
    }
    
    func addToCart(dish: Dish, count: Int = 1, options: [Dish.OptionGroup.Option] = []) {
        if dishes.contains(where: { $0.dish == dish && $0.options == options }) {
            dishes = dishes.map { $0.dish == dish && $0.options == options ? CartOrderItem(dish: dish, count: $0.count + count, options: options) : $0 }
        } else {
            dishes.append(CartOrderItem(dish: dish, count: count, options: options))
        }
    }
    
    func changeCountFor(dish: Dish, count: Int) {
        if count != 0 {
            dishes = dishes.map { $0.dish == dish ? CartOrderItem(dish: dish, count: count, options: $0.options) : $0 }
        } else {
            removeFromCart(dish: dish)
        }
    }
    
    func changeOptionsFor(dish: Dish, options: [Dish.OptionGroup.Option]) {
        dishes = dishes.map { $0.dish == dish ? CartOrderItem(dish: dish, count: $0.count, options: options) : $0 }
    }
    
    func removeFromCart(dish: Dish) {
        if let indexToRemove = dishes.firstIndex(where: { $0.dish == dish }) {
            dishes.remove(at: indexToRemove)
        }
    }
    
    func clearCart() {
        dishes.removeAll()
    }
    
    func prepareForSending() -> [Order.OrderDish] {
        dishes.map { (dish: CartOrderItem) -> Order.OrderDish in
            Order.OrderDish(dishId: dish.dish.id, title: nil, price: nil, count: dish.count, options: dish.options.map { Order.OrderDish.Option(dishOptionId: $0.id)})
        }
    }
}
