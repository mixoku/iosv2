//
//  GeocoderModels.swift
//  MyCafe
//
//  Created by Alexey Antonov on 20/01/21.
//

import Foundation

struct GeocoderAddress: Codable {
    struct Response: Codable {
        struct GeoCollection: Codable {
            struct FeatureMember: Codable {
                struct GeoObject: Codable {
                    struct Point: Codable {
                        let pos: String
                    }
    
                    let name: String
                    let Point: Point
                }
    
                let GeoObject: GeoObject
            }

            let featureMember: [FeatureMember]
        }
        
        let GeoObjectCollection: GeoCollection
    }

    let response: Response
}
