//
//  Promo.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import Foundation

struct Promo: Codable, Identifiable {
    let id: Int
    let title: String
    let imagePath: String
    let subtitle: String
    let description: String
}
