//
//  MenuSection.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import Foundation

struct MenuSection: Identifiable, Codable {
    let id: Int
    let title: String
    let description: String?
    let dishes: [Dish]
}
