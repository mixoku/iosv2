//
//  APIImage.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import Foundation

struct APIImage: Codable, Identifiable, Hashable {
    struct Images: Codable, Hashable {
        let small: String
        let big: String
    }
    
    let id: Int
    let title: String
    let description: String?
    let images: Images
}
