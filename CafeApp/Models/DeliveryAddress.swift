//
//  DeliveryAddress.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import Foundation

struct DeliveryAddress: Identifiable, Codable, Hashable {
    let id: Int?
    let isDefault: Int
    let address: String
    let apartment: String?
    let intercom: String?
    let floor: String?
    let entrance: String?
    let comment: String?
    let lat: String?
    let lng: String?
}
