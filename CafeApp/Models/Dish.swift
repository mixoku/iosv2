//
//  Dish.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import Foundation

struct Dish: Codable, Identifiable, Hashable {
    struct OptionGroup: Identifiable, Codable, Hashable {
        let id: Int
        let title: String
        let description: String?
        let required: Int
        let type: String
        let options: [Option]
        
        struct Option: Identifiable, Codable, Hashable {
            let id: Int
            let value: String
            let price: Int
            let isDefault: Int
            let isPlural: Int
        }
    }
    
    let id: Int
    let title: String
    let weight: Int?
    let descriptionShort: String?
    let descriptionFull: String?
    let price: Int
    let measure: String
    let images: [APIImage]
    let optionGroups: [OptionGroup]
}
