//
//  Order.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import Foundation

struct Order: Identifiable, Codable {
    struct OrderDish: Codable {
        struct Option: Codable {
            let dishOptionId: Int
        }
        
        let dishId: Int
        let title: String?
        let price: Int?
        let count: Int
        let options: [Option]
    }
    
    struct DeliveryInfo: Codable {
        let isFree: Bool
        let sum: Int
    }
    
    struct Address: Codable {
        let address: String
    }
    
    let id: Int?
    let comment: String?
    let status: String?
    var dishes: [OrderDish]
    let deliveryAddressId: Int
    var sum: Int?
    let deliveryInfo: DeliveryInfo?
    let createdAt: Int?
    let updatedAt: Int?
    let deliveryAddress: Address?
    
    var statusDescription: String {
        switch status {
        case "new":
            return "Новый"
        case "awaiting_payment":
            return "Ожидание оплаты"
        case "accepting":
            return "Принят"
        case "in_process":
            return "В работе"
        case "on_delivery":
            return "Доставляется"
        case "completed":
            return "Доставлен"
        case "canceled":
            return "Отменён"
        default:
            return "Неизвестен"
        }
    }
}

enum OrderStatus: Int {
    case block = 10
    case new = 80
    case active = 20
    case inactive = 30
    case deleted = 40
    case process = 50
    case delivery = 60
    case completed = 70
}
