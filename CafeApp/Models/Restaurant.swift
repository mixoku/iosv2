//
//  Restaurant.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import Foundation
import SwiftUI

struct Restaurant: Codable, Identifiable {
    let id: Int
    let title: String
    let description: String
    let siteUrl: String
    let logoUrl: String
    let colorScheme: String
    let phone: String
    let aboutCompany: String?
    let workTime: String?
    let aboutDelivery: String?
    let deliveryTime: String?
    let images: [APIImage]
    
    var themeColor: Color {
        Color(hex: colorScheme)
    }
    
    var phoneUrl: URL? {
        URL(string: "tel://\(phone)")
    }
}

extension Restaurant {
    static var lastId: Int? {
        UserDefaults.standard.integer(forKey: "lastCafeId") == 0 ? nil : UserDefaults.standard.integer(forKey: "lastCafeId")
    }
}
