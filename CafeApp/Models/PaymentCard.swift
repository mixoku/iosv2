//
//  PaymentCard.swift
//  CafeApp
//
//  Created by Alexey Antonov on 28/01/21.
//

import Foundation

struct PaymentCard: Codable, Identifiable {
    let id: Int
    let pan: String
    let cardName: String?
    let cardType: String
    let isDefault: Bool
    
    private enum CodingKeys: String, CodingKey {
            case id
            case isDefault = "default"
            case pan
            case cardName
            case cardType
    }
}
