//
//  ServiceZone.swift
//  CafeApp
//
//  Created by Alexey Antonov on 25/01/21.
//

import Foundation

struct ServiceZone: Codable, Identifiable {
    let id: Int
    let price: Int
    let isFree: Int
    let minPrice: Int
    let freeMinPrice: Int?
    let polygon: [[Double]]?
    
    var isFreeZone: Bool {
        isFree == 1
    }
}

struct Coords: Codable {
    let lat: String
    let lon: String
}
