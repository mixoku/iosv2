//
//  User.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import Foundation

struct User: Codable, Identifiable {
    let id: Int?
    var username: String?
    var phone: String?
    var email: String?
    var birthday: String?
    var accessToken: String?
    let message: String?
    let name: String?
}
