//
//  CartOrder.swift
//  CafeApp
//
//  Created by Alexey Antonov on 24/01/21.
//

import Foundation

struct CartOrderItem: Codable, Hashable {
    let dish: Dish
    var count: Int
    var options: [Dish.OptionGroup.Option]
    
    var totalPrice: Int {
        options.reduce(dish.price) { $0 + $1.price } * count
    }
}
