//
//  CartTabView.swift
//  MyCafe
//
//  Created by Михаил on 12.12.2020.
//

import SwiftUI

struct CartTabView: View {
    @EnvironmentObject var orderStore: OrderStore
    @State var minimalDeliveryPrice: Int = 1200
    @Binding var showFullCart: Bool
    
    var body: some View {
        ZStack {
            Rectangle()
                .shadow(radius: 3, y: -7)
            VStack {
                Divider()
                if minimalDeliveryPrice > orderStore.totalSum {
                    Text("До минимальной суммы доставки — \(minimalDeliveryPrice - orderStore.totalSum) ₽")
                        .font(body2FontRubik)
                        .foregroundColor(.gray)
                        //.frame(height: 30, alignment: .leading)
                        .padding(.horizontal, 35)
                        .padding(.bottom, 5)
                } else {
                    Spacer()
                        //.frame(height: 30, alignment: .leading)
                        .padding(.horizontal, 35)
                        .padding(.bottom, 5)
                }
                
                Button(action: {
                    showFullCart = true
                }) {
                    RoundedRectangleButton(orderCost: orderStore.totalSum, title: "В корзину")
                        .padding(.bottom)
                }
                Spacer()
                
            }
            
        }
        .frame(height: 75)
        .foregroundColor(Color.mainBackgroundColor)
    }
}
