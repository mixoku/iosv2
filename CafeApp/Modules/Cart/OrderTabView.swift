//
//  OrderTabView.swift
//  MyCafe
//
//  Created by Михаил on 18.12.2020.
//

import SwiftUI

struct OrderTabView: View {
    let items: [CartOrderItem]
    var order: [Order.OrderDish]
    var orderCost: Int
    @State var minimalDeliveryPrice: Int = 1200
    @Binding var showModal: Bool
    @Binding var deliveryType: Int
    
    var body: some View {
        ZStack {
            Rectangle()
            VStack {
                Divider()
                HStack {
                    Text("\(orderCost) ₽")
                        .font(header2FontRubikBold)
                        //.padding([.leading], 20)
                        .foregroundColor(.mainAccentColor)
                    Spacer(minLength: 100)
                    if deliveryType == 0 {
                        NavigationLink(
                            destination: NavigationDetailView(content: DeliveryView(order: order, orderSum: orderCost, items: items, showModal: $showModal), fullScreen: false),
                            label: {
                                OrderRoundedRectangleButton()
                            })
                    } else {
                        NavigationLink(
                            destination: NavigationDetailView(content: PickUpView(order: order, orderSum: orderCost, items: items, showModal: $showModal), fullScreen: false),
                            label: {
                                OrderRoundedRectangleButton()
                            })
                    }
                }
                .padding()
                Spacer()
            }
            .frame(height: 90)
        }
        .frame(height: 90)
        .foregroundColor(Color.mainBackgroundColor)
    }
}

struct OrderTabView_Previews: PreviewProvider {
    static var previews: some View {
        OrderTabView(items: [], order: [], orderCost: 1000, showModal: .constant(true), deliveryType: .constant(0))
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
            .previewDisplayName("CartTabView")
    }
}

