//
//  OrderModalView.swift
//  MyCafe
//
//  Created by Михаил on 15.12.2020.
//

import SwiftUI

struct OrderModalView: View {
    @EnvironmentObject var orderStore: OrderStore
    @Binding var showModal: Bool
    var freeDeliveryPrice = 1000
    @State var deliveryPrice = 120
    @State var deliveryType = 0
    
    var deliveryDescription: String {
        if isDeliveryForFree {
            return "Доставка бесплатно. \nУра!"
        } else {
            return "Закажите ещё на \(freeDeliveryPrice - orderStore.totalSum)₽ для \nбесплатной доставки"
        }
    }
    
    var isDeliveryForFree: Bool {
        if freeDeliveryPrice > orderStore.totalSum {
            return false
        } else {
            return true
        }
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                VStack {
                    ScrollView {
                        HStack {
                            Text("Заказ")
                                .font(header2FontRubikBold)
                                .foregroundColor(.mainAccentColor)
                            Spacer()
                            Button(action: {
                                orderStore.clearCart()
                                showModal = false
                            }) {
                                Text("Очистить")
                                    .foregroundColor(.themeColor)
                            }
                        }
                        .padding(.horizontal)
                        .padding(.vertical, 20)
                        VStack {
                            ForEach(orderStore.dishes, id: \.dish.id) { item in
                                OrderCell(item: Binding<CartOrderItem>(get: {
                                    item
                                }, set: { orderStore.changeCountFor(dish: item.dish, count: $0.count)}))
                            }
                        }
                        .padding()
                        HStack {
                            Text("Получение")
                                .font(header2FontRubikBold)
                                .foregroundColor(.mainAccentColor)
                            Spacer()
                        }
                        .padding([.leading, .trailing, .top])
                        .padding(.bottom, 20)
                        OrderSwitcher(items: ["Доставка", "Самовывоз"], chosenOrderType: $deliveryType)
                            .animation(.easeInOut)
                            .padding(.horizontal)
                        if deliveryType == 0 {
                            HStack {
                                Text("Стоимость доставки")
                                .font(body1FontRubikMedium)
                                Spacer()
                                
                                Text("\(isDeliveryForFree ? 0 : deliveryPrice)₽")
                                    .font(body1FontRubikMedium)
                            }
                            .padding(.horizontal)
                            HStack {
                                Text(deliveryDescription)
                                    .font(body2FontRubik)
                                    .foregroundColor(.gray)
                                Spacer()
                            }
                            .padding(.horizontal)
                            .padding(.bottom)
                        } else {
                            HStack {
                                Text("При самовывозе вы не платите доставку.")
                                    .font(body1FontRubikRegular)
                                Spacer()
                            }
                            .padding()
                            
                            
                        }
                        Spacer()
                        Rectangle()
                            .foregroundColor(Color.mainBackgroundColor.opacity(0.00001))
                            .frame(height: 100, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    }
                }
                    VStack {
                        Spacer()
                        OrderTabView(items: orderStore.dishes, order: orderStore.prepareForSending(), orderCost: orderStore.totalSum, showModal: $showModal, deliveryType: $deliveryType)
                            .shadow(color: .clear, radius: 0)
                    }
                }
                .ignoresSafeArea(edges: .bottom)
                .shadow(color: Color.gray.opacity(0.2), radius: 10, y: -10)
                .navigationBarHidden(true)
            }
    }
}

struct OrderModalView_Previews: PreviewProvider {
    static var previews: some View {
        OrderModalView(showModal: .constant(true))
    }
}

