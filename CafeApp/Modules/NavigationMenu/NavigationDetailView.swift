//
//  NavigationDetailView.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import SwiftUI

struct NavigationDetailView<Content: View>: View {
    @Environment(\.presentationMode) var presentationMode
    let content: Content
    let fullScreen: Bool
    
    var body: some View {
        VStack(alignment: .leading) {
            if fullScreen {
                ZStack {
                    content
                    VStack {
                        HStack {
                            Button(action: {
                                presentationMode.wrappedValue.dismiss()
                            }, label: {
                                Image("arrow.left.cafe")
                                    .font(.system(size: 30))
                                    .foregroundColor(.themeColor)
                                    .padding()
                                    .background(Circle().fill(Color.white))
                            })
                            Spacer()
                        }.padding()
                        Spacer()
                    }
                }
            } else {
                HStack {
                    Button(action: {
                        presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Image("arrow.left.cafe")
                            .font(.system(size: 30))
                            .foregroundColor(.themeColor)
                    })
                    Spacer()
                }.padding()
                content
            }
            Spacer()
        }.edgesIgnoringSafeArea(.bottom)
        .navigationBarHidden(true)
    }
}

struct NavigationDetailView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationDetailView(content: Text("OK"), fullScreen: false)
    }
}
