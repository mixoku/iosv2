//
//  NavigationMenuView.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import SwiftUI
import Combine
import ACarousel

struct NavigationMenuView: View {
    @ObservedObject var carouselVM: RestaurantCarouselViewModel
    @Binding var showNavigation: Bool
    var restaurant: Restaurant
    @State private var currentIndex = 0
    
    init(showNavigation: Binding<Bool>, restaurant: Restaurant) {
        _showNavigation = showNavigation
        self.restaurant = restaurant
        self.carouselVM = RestaurantCarouselViewModel(restaurant: restaurant)
        
        currentIndex = carouselVM.restaurants.firstIndex { $0.id == restaurant.id } ?? 0
    }
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack {
                    if carouselVM.restaurants.count > 0 {
                        ACarousel(carouselVM.restaurants, index: $currentIndex, sidesScaling: 0.8, isWrap: false) { cafe in
                            RestaurantCard(isMain: $currentIndex, showModal: $showNavigation, restaurant: cafe).environmentObject(carouselVM)
                        }.frame(height: 200)
                        .padding(.vertical)
                        .onAppear {
                            currentIndex = 0
                        }
                    }
    //                        ZStack(alignment: .topLeading) {
    //                            Rectangle()
    //                                .fill(cafe.themeColor)
    //                            Text(cafe.title).padding()
    //                            if cafe.id != restaurant.id {
    //                                Button(action: {
    //                                    generalViewModel.selectRestaurant(with: cafe.id) { _ in
    //                                        viewModel.changeRestaurant(for: cafe.id)
    //                                        orderStore.clearCart()
    //                                        showNavigation = false
    //                                    }
    //                                }, label: {
    //                                    Text("Назначить текущим")
    //                                })
    //                            }
    //                        }
                        Spacer()
                        Button(action: {
                            showNavigation = false
                        }, label: {
                            MenuButton(imageName: "list.dash", itemName: "Меню")
                        })
                        NavigationLink(
                            destination: NavigationDetailView(content: OrderHistoryView(viewModel: OrderHistoryViewModel(restaurantId: restaurant.id)), fullScreen: false),
                            label: {
                                MenuButton(imageName: "bag.cafe", itemName: "Мои заказы")
                            })
                        NavigationLink(
                            destination: NavigationDetailView(content: ProfileView(), fullScreen: false),
                            label: {
                                MenuButton(imageName: "person.cafe", itemName: "Профиль")
                            })
                        NavigationLink(
                            destination: NavigationDetailView(content: PromoListView(showDetail: $showNavigation), fullScreen: false),
                            label: {
                                MenuButton(imageName: "tag.cafe", itemName: "Акции")
                            })
                        NavigationLink(
                            destination: NavigationDetailView(content: AboutView(restaurant: restaurant), fullScreen: false),
                            label: {
                                MenuButton(imageName: "info.circle", itemName: "О ресторане")
                            })
                        Spacer()
                        SupportField()
                            .padding()
                }
            }.navigationBarHidden(true)
        }
    }
}



struct NavigationMenuView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationMenuView(showNavigation: .constant(true), restaurant: theRestaurant)
    }
}
