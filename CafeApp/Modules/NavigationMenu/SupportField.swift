//
//  SupportField.swift
//  CafeApp
//
//  Created by Михаил on 31.01.2021.
//

import SwiftUI

struct SupportField: View {
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 16.0)
                .foregroundColor(.mainBackgroundColor)
                .shadow(color: Color.gray.opacity(0.5), radius: 20, x: 0, y: 0)
            VStack(alignment: .leading) {
                Text("Поддержка")
                    .font(body1FontRubikMedium)
                Text("Свяжитесь с нами, если у вас возникли \nвопросы или проблемы с заказом")
                    .font(body2FontRubik)
                    .lineLimit(2)
                    .font(.footnote)
                    .foregroundColor(.gray)
                    .multilineTextAlignment(.leading)
                    .frame(height: 40)
                HStack {
                    RoundedButton(isWithImage: true, imageName: "message", label: "Чат").layoutPriority(6)
                    RoundedButton(isWithImage: true, imageName: "phone", label: "")
                        .layoutPriority(4)
                }
                
            }
            .padding()
           
        }
    }
}

struct SupportField_Previews: PreviewProvider {
    static var previews: some View {
        SupportField()
    }
}
