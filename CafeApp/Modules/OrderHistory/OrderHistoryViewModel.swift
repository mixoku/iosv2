//
//  OrderHistoryViewModel.swift
//  MyCafe
//
//  Created by Alexey Antonov on 19/01/21.
//

import Foundation

final class OrderHistoryViewModel: ViewModel {
    @Published private(set) var orders = [Order]()
    private let restaurantId: Int
    
    @Published private(set) var isLoggedIn: Bool
    
    init(restaurantId: Int) {
        self.restaurantId = restaurantId
        self.isLoggedIn = !(UserDefaults.standard.string(forKey: "token")?.isEmpty ?? true)
    }
    
    func loadData() {
        fetch(from: Endpoint.orderHistory(restaurantId: restaurantId)) { [weak self] (result: [Order]) in
            self?.orders = result
        }
    }
    
    func updateToken() {
        self.isLoggedIn = !(UserDefaults.standard.string(forKey: "token")?.isEmpty ?? true)
    }
}
