//
//  OrderHistoryView.swift
//  MyCafe
//
//  Created by Михаил on 25.12.2020.
//

import SwiftUI

struct OrderHistoryView: View {
    @ObservedObject var viewModel: OrderHistoryViewModel
    @State var openedCards: [Bool] = []

    @State var showModal = false
    var body: some View {
        if viewModel.isLoggedIn {
            ScrollView(.vertical) {
                HStack {
                    Text("Мои заказы")
                        .font(header2FontRubikBold)
                        .foregroundColor(.mainAccentColor)
                    Spacer()
                }.padding(.horizontal)
                
                if viewModel.orders.count > 0 {
                    ForEach(0..<viewModel.orders.count) { order in
                        OrderCard(order: viewModel.orders[order])
                            .padding(.horizontal)
                    }
                } else {
                    Text("Вы пока ещё ничего не заказывали")
                        .padding(.top, 50)
                        .foregroundColor(.secondary)
                    Text("Может, стоит попробовать?")
                        .foregroundColor(.secondary)
                    NavigationLink(
                        destination: NavigationDetailView(content: OrderStatusScreen(order: fakeOrder, showModal: $showModal), fullScreen: false),
                        label: {
                            OrderHistoryRoundedRectangleButton(backgroundColor: Color.themeColor, title: "order Status", textColor: .mainBackgroundColor)
                        }
                    )
                    
                }
            }
            .onAppear {
                viewModel.loadData()
            }
        } else {
            LoginView() { _ in viewModel.updateToken() }
        }
    }
}

struct OrderHistoryView_Previews: PreviewProvider {
    static var previews: some View {
        OrderHistoryView(viewModel: OrderHistoryViewModel(restaurantId: 2))
    }
}

