//
//  PromoView.swift
//  MyCafe
//
//  Created by Михаил on 27.12.2020.
//

import SwiftUI

struct PromoListView: View {
    @State var promo: Promo?
    @Binding var showDetail: Bool
    
    var body: some View {
        GeometryReader {  geometry in
            ScrollView(.vertical) {
                HStack {
                    Text("Акции")
                        .font(header2FontRubikBold)
                        .foregroundColor(.mainAccentColor)
                    Spacer()
                }.padding(.horizontal)
                
                ForEach(promosSample) { promo in
                    NavigationLink(
                        destination:
                            NavigationDetailView(
                                content: PromoDetailView(promo: promo, showModal: $showDetail).ignoresSafeArea(),
                                fullScreen: true),
                        label: {
                            SmallMenuBanner(promo: promo, isBigCard: true, height: geometry.size.height/3).padding(.horizontal)
                        })
                    
                }
            }
        }
    }
}
