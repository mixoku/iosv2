//
//  StoryView.swift
//  CafeApp
//
//  Created by Михаил on 30.01.2021.
//

import SwiftUI

struct PromoStoryView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var currentStory = 0
    var promoAmount: Int = promosSample.count
    let promos: [Promo]
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Image(promos[currentStory].imagePath)
                    .resizable()
                    .scaledToFill()
                    .frame(width: geometry.size.width + 200, height: geometry.size.height + 200, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    
                VStack {
                    StoryLine(width: geometry.size.width - 40, amountOfSectors: promoAmount, currentStory: $currentStory)
                        .padding(.top)
                        .padding(.bottom, 25)
                    HStack {
                        Spacer()
                        Button(action: {
//                            navigation method
                        }) {
                            CancelButton()
                        }
                        
                    }
                    Spacer()
                    HStack {
                        Text(promos[currentStory].title)
                            .font(header1FontRubik)
                            .foregroundColor(.mainBackgroundColor)
                            .background(Color.themeColor)
                        Spacer()
                    }
                    .padding(.bottom)
                    HStack {
                        Text(promos[currentStory].subtitle)
                            .font(body1FontRubikRegular)
                            .foregroundColor(.mainBackgroundColor)
                            .background(Color.mainAccentColor)
                        Spacer()
                    }
                    .padding(.bottom)
                    Button(action: {
//                        navigation method
                    }) {
                        OrderHistoryRoundedRectangleButton(backgroundColor: .themeColor, title: "В меню", textColor: .mainBackgroundColor)
                    }
                }
                .padding([.leading, .trailing])
                .frame(width: geometry.size.width, height: geometry.size.height, alignment: .center)
                HStack {
                    Rectangle()
                        .foregroundColor(Color.white.opacity(0.1))
                        .onTapGesture {
                            if currentStory != 0 {
                                currentStory -= 1
                            } else {
//                                close stories screen
                            }
                        }
                    Rectangle()
                        .foregroundColor(Color.black.opacity(0.1))
                        .onTapGesture {
                            if currentStory != promoAmount - 1 {
                                currentStory += 1
                            } else {
//                                close stories screen
                            }
                        }
                }
                .frame(width: geometry.size.width, height: geometry.size.height - 130, alignment: .center)
            }
            .frame(width: geometry.size.width, height: geometry.size.height, alignment: .center)
        }
        
    }
}

struct StoryLine: View {
    var width: CGFloat
    var amountOfSectors: Int
    @Binding var currentStory: Int
    
    var body: some View {
        
            ZStack {
                Capsule()
                    .foregroundColor(.mainBackgroundColor)
                HStack {
                    Capsule()
                        .foregroundColor(.themeColor)
                        .frame(width: width * CGFloat(currentStory) / CGFloat(amountOfSectors))
                        .animation(.easeInOut)
                    Spacer()
                }
                
            }
            .frame(width: width, height: 5, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        
    }
}

struct PromoStoryView_Previews: PreviewProvider {
    static var previews: some View {
        PromoStoryView(promos: promosSample)
    }
}
