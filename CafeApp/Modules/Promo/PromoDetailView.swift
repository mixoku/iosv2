//
//  PromoDetailView.swift
//  MyCafe
//
//  Created by Михаил on 27.12.2020.
//

import SwiftUI

struct PromoDetailView: View {
    @Environment(\.presentationMode) var presentationMode
    let promo: Promo
    @Binding var showModal: Bool
    
    var body: some View {
        VStack(alignment: .leading) {
            Image(promo.imagePath)
                .resizable()
                .scaledToFit()
            Text(promo.title)
                .font(header1FontRubik)
                .foregroundColor(.mainBackgroundColor)
                .padding()
            Text(promo.description)
                .font(body2FontRubik)
                .foregroundColor(.mainBackgroundColor)
                .padding()
                
            Spacer()
            Button(action: {
                showModal = false
            }) {
                OrderHistoryRoundedRectangleButton(backgroundColor: .mainBackgroundColor, title: "В меню", textColor: Color.mainAccentColor)
                    .padding()
            }
        }
        .background(Color.themeColor)
        .edgesIgnoringSafeArea(.bottom)
    }
}


struct PromoDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PromoDetailView(promo: promosSample[0], showModal: .constant(true))
    }
}
