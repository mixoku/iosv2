//
//  AddressViewModel.swift
//  MyCafe
//
//  Created by Alexey Antonov on 19/01/21.
//

import Foundation
import CoreLocation

final class AddressViewModel: ViewModel {
    @Published private(set) var addresses = [DeliveryAddress]()
    @Published var showAddModal = false {
        didSet {
            getAdresses()
        }
    }
    
    let city = "Москва"
    
    @Published var addressToDelete: Int?
    @Published var needsAttention = false {
        didSet {
            if !needsAttention {
                addressToDelete = nil
            }
        }
    }
    
    func getAdresses() {
        if let token = UserDefaults.standard.string(forKey: "token"), !token.isEmpty {
            fetch(from: Endpoint.getAddresses, success: { [weak self] (addresses: [DeliveryAddress]) in
                self?.addresses = addresses
            })
        }
    }
    
    func deleteAddress(id: Int) {
        print(id)
        getHttpResponse(from: Endpoint.deleteAddress(id: id), success: { [weak self] (response) in
            if let response = response, 200...299 ~= response.statusCode {
                self?.getAdresses()
            } else {
                print(response?.statusCode)
            }
        })
    }
    
    func addAddress(addressString: String, isDefault: Bool, apartment: String, intercom: String, floor: String, entrance: String, comment: String) {
        getAddressModel(addressString: addressString, isDefault: isDefault, apartment: apartment, intercom: intercom, floor: floor, entrance: entrance, comment: comment) { [weak self] address in
            self?.getHttpResponse(from: Endpoint.addAddress(address: address), success: { [weak self] _ in self?.getAdresses()})
        }
    }
    
    func getAddressModel(addressString: String, isDefault: Bool, apartment: String, intercom: String, floor: String, entrance: String, comment: String, completion: @escaping (DeliveryAddress)->()) {
        fetch(from: Endpoint.findPoint(address: addressString)) { (response: GeocoderAddress) in
            if let lat = response.response.GeoObjectCollection.featureMember.first?.GeoObject.Point.pos.split(separator: " ")[0],
               let lng = response.response.GeoObjectCollection.featureMember.first?.GeoObject.Point.pos.split(separator: " ")[1] {
                let address = DeliveryAddress(
                    id: nil,
                    isDefault: isDefault ? 1 : 0,
                    address: addressString,
                    apartment: apartment,
                    intercom: intercom,
                    floor: floor,
                    entrance: entrance,
                    comment: comment,
                    lat: "\(lat)",
                    lng: "\(lng)")

                completion(address)
            }
        }
    }
    
    func getAddress(for position: CLLocationCoordinate2D, completion: @escaping (String)->()) {
        fetch(from: Endpoint.findAddress(lat: "\(position.latitude)", lng: "\(position.longitude)"), success: { (response: GeocoderAddress) in
            if let address = response.response.GeoObjectCollection.featureMember.first?.GeoObject.name {
                completion(address)
            }
        })
    }

}
