//
//  NewAddressViewModel.swift
//  CafeApp
//
//  Created by Alexey Antonov on 07/02/21.
//

import Foundation
import YandexMapsMobile
import Combine

final class NewAddressViewModel: ObservableObject {
    @Published private(set) var error: Error?
    @Published private(set) var suggestedAddreses = [String?]()
    @Published var searchText = "" {
        didSet {
            let suggestHandler = {(response: [YMKSuggestItem]?, error: Error?) -> Void in
                if let items = response {
                    self.suggestedAddreses = items.map(\.displayText)
                } else {
                    self.error = error
                }
            }
            
            suggestSession.suggest(withText: searchText, window: BOUNDING_BOX, suggestOptions: YMKSuggestOptions(suggestTypes: .geo, userPosition: nil, suggestWords: false), responseHandler: suggestHandler)
        }
    }
    
    let searchManager = YMKSearch.sharedInstance().createSearchManager(with: .combined)
    var suggestSession: YMKSearchSuggestSession!
    
    let BOUNDING_BOX = YMKBoundingBox(
        southWest: YMKPoint(latitude: 55.55, longitude: 37.42),
        northEast: YMKPoint(latitude: 55.95, longitude: 37.82))
    
    init() {
        suggestSession = searchManager.createSuggestSession()
    }
}
