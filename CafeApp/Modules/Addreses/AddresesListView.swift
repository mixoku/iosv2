//
//  AddressListModalView.swift
//  MyCafe
//
//  Created by Михаил on 26.12.2020.
//

import SwiftUI

struct AddressListModalView: View {
    @ObservedObject var viewModel = AddressViewModel()
    @Binding var show: Bool
    
    @State private var addressToDelete: Int?
    
    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    Text("Адрес \nдоставки")
                        .font(header2FontRubikBold)
                        .foregroundColor(.mainAccentColor)
                    Spacer()
                    Button(action: {
                        viewModel.showAddModal = true
                    }, label: {
                        ZStack {
                            Circle()
                                .foregroundColor(.themeColor)
                                .frame(width: 64)
                            Image(systemName: "plus")
                            .font(.system(size: 30))
                            .foregroundColor(.mainBackgroundColor)
                        }
                    })
                }
                .frame(height: 100)
                .padding()
                Divider()
                if viewModel.addresses.count > 0 {
                    ForEach(viewModel.addresses, id:\.self) { address in
                        VStack(alignment: .leading) {
                            Spacer()
                            HStack(alignment: .center) {
                                Text(address.address)
                                    .lineLimit(2)
                                    .font(body1FontRubikMedium)
                                    .foregroundColor(.mainAccentColor)
                                    .frame(height: 40)
                                    
                                Spacer()
                                Button(action: {
                                    addressToDelete = address.id
                                }, label: {
                                    Image(systemName: "trash")
                                        .resizable()
                                        .frame(width: 25, height: 25)
                                })
                            }
                            .padding(.horizontal)
                            Spacer()
                            Divider()
                        }.frame(height: 60)
                        .alert(item: $addressToDelete, content: { address in
                            Alert(title: Text("Вы уверены?"),
                                  primaryButton: .destructive(Text("Удалить")) {
                                    viewModel.deleteAddress(id: address)
                                  },
                                  secondaryButton: .cancel(Text("Отмена")))
                        })
                    }
                } else {
                    VStack {
                        Text("Начинать надо с малого")
                            .font(header2FontRubikRegular)
                        Text("Добавить адрес, например")
                    }
                }
                Spacer()
            }.navigationBarHidden(true)
            .sheet(isPresented: $viewModel.showAddModal, content: {
                AddAddressView(show: $viewModel.showAddModal)
            }).onAppear {
                viewModel.getAdresses()
            }
        }
    }
}

struct AddressListModalView_Previews: PreviewProvider {
    static var previews: some View {
        AddressListModalView(show: .constant(true))
    }
}
