//
//  AddAddressViewModel.swift
//  CafeApp
//
//  Created by Alexey Antonov on 27/01/21.
//

import Foundation
import Combine
import YandexMapsMobile
import CoreLocation

final class AddAddressViewModel: NSObject, ObservableObject {
    @Published var point = YMKPoint(latitude: 0, longitude: 0) {
        didSet {
            cameraPosition = YMKCameraPosition(target: point, zoom: 15, azimuth: 0, tilt: 0)
        }
    }
    @Published var currentAddress: String = "" {
        didSet {
            getPoint(for: currentAddress)
        }
    }
    @Published var error: AppError?
    @Published var cameraPosition: YMKCameraPosition = YMKCameraPosition(target: YMKPoint(latitude: 0, longitude: 0), zoom: 15, azimuth: 0, tilt: 0)
    
    @Published private(set) var zones = [YMKPolygon]()
    
    override init() {
        super.init()
        
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
        Endpoint.getServiceZone(restaurantId: Restaurant.lastId!)
            .fetch()
            .sink { (completion) in
                switch completion {
                case .failure(let error):
                    print(error)
                case .finished:
                    break
                }
            } receiveValue: { [weak self] (serviceZones: [ServiceZone]) in
                print(serviceZones)
                for zone in serviceZones {
                    if let polygon = zone.polygon {
                        var yandexPolygon = [YMKPoint]()
                        for point in polygon {
                            yandexPolygon.append(YMKPoint(latitude: point[0], longitude: point[1]))
                        }
                        self?.zones.append(YMKPolygon(outerRing: YMKLinearRing(points: yandexPolygon), innerRings: []))
                    }
                }
            }
            .store(in: &cancellables)

    }
    
    private func getAddress(for position: YMKPoint) {
        Endpoint.findAddress(lat: "\(position.latitude)", lng: "\(position.longitude)")
            .fetch()
            .sink { (completion) in
                switch completion {
                case .failure(let error):
                    print(error.localizedDescription)
                case .finished:
                    break
                }
            } receiveValue: { [weak self] (response: GeocoderAddress) in
                if let address = response.response.GeoObjectCollection.featureMember.first?.GeoObject.name {
                    self?.currentAddress = address
                } else {
                    self?.error = .badCoding
                }
            }
            .store(in: &cancellables)
    }
    
    private func getPoint(for address: String) {
        Endpoint.findPoint(address: address)
            .fetch()
            .sink { [weak self] (completion) in
                switch completion {
                case .failure(let error):
                    self?.error = .generalError(error: error)
                case .finished:
                    break
                }
            } receiveValue: { [weak self] (response: GeocoderAddress) in
                print(response)
                if let point = response.response.GeoObjectCollection.featureMember.first?.GeoObject.Point.pos, let lat = Double(point.split(separator: " ")[1]), let lng = Double(point.split(separator: " ")[0]) {
                    self?.point = YMKPoint(latitude: lat, longitude: lng)
                } else {
                    self?.error = .badCoding
                }
            }
            .store(in: &cancellables)
    }
    
    private var cancellables = Set<AnyCancellable>()
    
    deinit {
        for cancellable in cancellables {
            cancellable.cancel()
        }
    }
}

extension AddAddressViewModel: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.point = YMKPoint(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            getAddress(for: self.point)
            manager.stopUpdatingLocation()
        } else {
            error = .LocationDisabled
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.error = .generalError(error: error)
    }
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.requestLocation()
        }
    }
}
