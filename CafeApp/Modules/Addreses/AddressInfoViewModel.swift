//
//  AddressInfoViewModel.swift
//  CafeApp
//
//  Created by Alexey Antonov on 09/02/21.
//

import Foundation

final class AddressInfoViewModel: ViewModel {
    @Published var apartment = ""
    @Published var entrance = ""
    @Published var intercom = ""
    @Published var floor = ""
    @Published var comment = ""
    
    func addAddress(lat: String, lng: String, address: String, completion: @escaping (Bool)->()) {
        let address = DeliveryAddress(id: nil, isDefault: 0, address: address, apartment: apartment, intercom: intercom, floor: floor, entrance: entrance, comment: comment, lat: lat, lng: lng)
        
        getHttpResponse(from: Endpoint.addAddress(address: address)) { [weak self] (response) in
            if let status = response?.statusCode, 200...299 ~= status {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
}
