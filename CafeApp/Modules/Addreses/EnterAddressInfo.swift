//
//  EnterAddressInfo.swift
//  CafeApp
//
//  Created by Alexey Antonov on 09/02/21.
//

import SwiftUI
import YandexMapsMobile

struct EnterAddressInfo: View {
    let point: YMKPoint
    @State var addressLine: String
    @ObservedObject var viewModel = AddressInfoViewModel()
    @Binding var show: Bool
    
    var body: some View {
        VStack(alignment:.leading) {
            YandexMapsView(point: point, cameraPosition: YMKCameraPosition(target: point, zoom: 15, azimuth: 0, tilt: 0), polygons: [])
                .aspectRatio(1, contentMode: .fit)
                .cornerRadius(16)
                .padding()
            Group {
                Text("Адрес доставки")
                    .font(.footnote)
                    .padding(.bottom, 5)
                TextField("Адрес", text: $addressLine)
                    .disabled(true)
            }.padding(.horizontal)
            HStack(alignment: .bottom) {
                VStack(alignment: .leading) {
                    Text("кв/офис")
                        .font(.footnote)
                        .padding(.bottom, 5)
                    TextField("кв/офис", text: $viewModel.apartment)
                }
                TextField("Домофон", text: $viewModel.intercom)
                TextField("Подъезд", text: $viewModel.entrance)
                TextField("Этаж", text: $viewModel.floor)
            }.padding(.horizontal)
            .padding(.vertical)
            TextField("Комментарий", text: $viewModel.comment)
                .padding()
            Spacer()
            Button(action: {
                viewModel.addAddress(lat: point.latitude.description, lng: point.longitude.description, address: addressLine) { result in
                    if result {
                        show = false
                    }
                }
            }, label: {
                HStack {
                    Spacer()
                    Text("Сохранить")
                    Spacer()
                }
                .padding()
                .background(Color.themeColor)
                .foregroundColor(.white)
                .cornerRadius(16)
            }).padding()
        }
    }
}

struct EnterAddressInfo_Previews: PreviewProvider {
    static var previews: some View {
        EnterAddressInfo(point: YMKPoint(latitude: 55.713024, longitude: 37.859835), addressLine: "Москва, ул. Оренбургская, 13к2", show: .constant(true))
    }
}
