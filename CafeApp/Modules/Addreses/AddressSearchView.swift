//
//  AddressSearchView.swift
//  CafeApp
//
//  Created by Alexey Antonov on 07/02/21.
//

import SwiftUI
import YandexMapsMobile

struct AddressSearchView: View {
    @ObservedObject var viewModel = NewAddressViewModel()
    @Binding var selectedAddress: String
    @Binding var showModal: Bool
    
    var body: some View {
            VStack(alignment: .leading, spacing: nil) {
                TextField("Введите адрес для поиска", text: $viewModel.searchText)
                    .padding()
                Divider()
                    .padding(.bottom)
                if viewModel.suggestedAddreses.count > 0 {
                    ForEach(viewModel.suggestedAddreses, id: \.self) { address in
                        Text(address ?? "")
                            .onTapGesture {
                                selectedAddress = address ?? ""
                                showModal = false
                            }
                        }
                } else {
                    Text(viewModel.searchText.isEmpty ? "Начните вводить адрес" : "Ничего не найдено")
                }
        }
    }
}

struct AddressSearchView_Previews: PreviewProvider {
    static var previews: some View {
        AddressSearchView(selectedAddress: .constant(""), showModal: .constant(true))
    }
}
