//
//  AddAddressView.swift
//  CafeApp
//
//  Created by Alexey Antonov on 27/01/21.
//

import SwiftUI
import YandexMapsMobile

struct AddAddressView: View {
    @ObservedObject var viewModel = AddAddressViewModel()
    @State var showAdd = false
    @Binding var show: Bool
    
    var body: some View {
        NavigationView {
            ZStack {
                YandexMapsView(point: viewModel.point, cameraPosition: viewModel.cameraPosition, polygons: viewModel.zones)
                    .edgesIgnoringSafeArea(.all)
                    .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height, alignment: .center)
                VStack {
                    Spacer()
                    Text("\(viewModel.currentAddress)")
                    Button(action: {
                        showAdd = true
                    }, label: {
                        Text("Изменить адрес")
                            .padding(.horizontal)
                            .padding(.vertical, 10)
                            .background(Color.themeColor.opacity(0.8))
                            .foregroundColor(.white)
                            .cornerRadius(40)
                })
                    Spacer()
                    NavigationLink(
                        destination: NavigationDetailView(content: EnterAddressInfo(point: viewModel.point, addressLine: viewModel.currentAddress, show: $show), fullScreen: false), label: {
                        HStack {
                            Spacer()
                            Text("Далее")
                            Spacer()
                        }.padding()
                        .background(Color.themeColor.opacity(0.8))
                        .foregroundColor(.white)
                        .cornerRadius(16)
                    }).padding(20)
                }
            }.sheet(isPresented: $showAdd, content: {
                AddressSearchView(selectedAddress: $viewModel.currentAddress, showModal: $showAdd)
            }).navigationBarHidden(true)
        }
    }
}

struct AddAddressView_Previews: PreviewProvider {
    static var previews: some View {
        AddAddressView(show: .constant(true))
    }
}

struct YandexMapsView: UIViewRepresentable {
    var point: YMKPoint
    var cameraPosition: YMKCameraPosition
    var polygons: [YMKPolygon]
    
    var mapView = YMKMapView()
    
    func makeCoordinator() -> YandexMapsView.Coordinator {
        return Coordinator(mapView: mapView)
    }
    
    
    func makeUIView(context: Context) -> YMKMapView {
        for zone in polygons {
            mapView.mapWindow.map.mapObjects.addPolygon(with: zone)
        }
        
        return mapView
    }
    
    func updateUIView(_ uiView: YMKMapView, context: Context) {
        uiView.mapWindow.map.move(with: cameraPosition)
    }
    
    class Coordinator: NSObject {
        let mapView: YMKMapView
        
        init(mapView: YMKMapView) {
            self.mapView = mapView
        }
    }
}

extension YandexMapsView.Coordinator: YMKUserLocationObjectListener {
    func onObjectAdded(with view: YMKUserLocationView) {
        print("OK")
        //view.arrow.setIconWith(UIImage(systemName:"mappin.and.ellipse")!)

        let pinPlacemark = view.pin.useCompositeIcon()

        pinPlacemark.setIconWithName("icon",
                                     image: UIImage(systemName:"mappin.and.ellipse")!,
            style:YMKIconStyle(
                anchor: CGPoint(x: 0, y: 0) as NSValue,
                rotationType:YMKRotationType.rotate.rawValue as NSNumber,
                zIndex: 0,
                flat: true,
                visible: true,
                scale: 2,
                tappableArea: nil))

        view.accuracyCircle.fillColor = UIColor.blue
    }
    
    func onObjectRemoved(with view: YMKUserLocationView) {
        
    }
    
    func onObjectUpdated(with view: YMKUserLocationView, event: YMKObjectEvent) {
        
    }
    
    
}
