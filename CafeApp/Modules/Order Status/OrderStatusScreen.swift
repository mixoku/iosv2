//
//  OrderStatusScreen.swift
//  MyCafe
//
//  Created by Михаил on 16.01.2021.
//

import SwiftUI

enum Stages {
    case accepting, cooking, delivery, done
}

struct OrderStatusScreen: View {
    @ObservedObject var viewModel: OrderStatusViewModel
    
    @Binding var showModal: Bool
    @State var stageNumber : Int = 0
    
    var orderStatusStage: Stages {
        switch stageNumber {
        case 0:
            return .accepting
        case 1:
            return .cooking
        case 2:
            return .delivery
        case 3:
            return .done
        default:
            return .accepting
        }
    }
    
    var textsOfOrderStatus = ["Принимаем заказ", "Готовим заказ", "Курьер в дороге", "Заказ доставлен!"]
    
    var textsOfDescription = ["В ближайшее время мы начнём готовить ваш заказ.", "Мы готовим ваш заказ и уже скоро передадим его курьеру.", "Ваш заказ уже у курьера, он доставит его в ближайшее время.", "Приятного аппетита, надеемся, что вам всё понравилось. Вы можете оставить для нас свой отзыв."]
    
    var estimatedTime: Int = 20
    
    var descriptionOfStatusOrder : String {
        switch orderStatusStage {
        case .accepting:
            return textsOfDescription[0]
        case .cooking:
            return textsOfDescription[1]
        case .delivery:
            return textsOfDescription[2]
        case .done:
            return textsOfDescription[3]
        }
    }
    
    var titleOfStatusOrder : String {
        switch orderStatusStage {
        case .accepting:
            return textsOfOrderStatus[0]
        case .cooking:
            return textsOfOrderStatus[1]
        case .delivery:
            return textsOfOrderStatus[2]
        case .done:
            return textsOfOrderStatus[3]
        }
    }
    
    init(order: Order, showModal: Binding<Bool>) {
        print(order.id)
        _showModal = showModal
        viewModel = OrderStatusViewModel(order: order)
    }
    
    var body: some View {
            GeometryReader { geometry in
//                HStack {
                    ScrollView(.vertical) {
        //                if viewModel.error != nil {
        //                    Text("Ошибка!")
        //                }
                        VStack {
                            Spacer(minLength: geometry.size.height/5)
                            Text(titleOfStatusOrder)
                                .font(header1FontRubik)
                                .padding(.bottom, 20)
                            OrderStatusView(screenWidth: geometry.size.width, stage: orderStatusStage)
                                .padding(.bottom, 30)
                            Text(descriptionOfStatusOrder)
                                .font(body1FontRubikRegular)
                                .foregroundColor(.gray)
                                .frame(width: geometry.size.width * 0.6, alignment: .center)
                                .multilineTextAlignment(.center)
                                .frame(height: 100)
                                .padding(.bottom, 10)
                            VStack {
                                if stageNumber != 3 {
                                Text("Время доставки:")
                                    .font(body1FontRubikRegular)
                                    .padding()
                                Text("\(estimatedTime) мин")
                                    .font(header2FontRubikBold)
                                } else {
                                    Spacer()
                                }
                            }
                            .frame(height: 100)
                            .padding(.bottom, 10)
                            Spacer()
                            OrderStatusHStackButtons(stageNumber: $stageNumber, showModal: $showModal).environmentObject(viewModel)
                                .padding(.horizontal)
                            if stageNumber != 3 {
                                OrderDeliveryCard(order: viewModel.order)
//                                    .padding()
                                SupportField()
                                    .padding()
                            }
                        }
//                    }
//                    .padding()
                    .ignoresSafeArea(edges: .bottom)
            }
            .frame(width: geometry.size.width, height: geometry.size.height, alignment: .leading)
        }
    }
}

struct OrderStatusHStackButtons: View {
    @Binding var stageNumber: Int
    @Binding var showModal: Bool
    @EnvironmentObject var viewModel: OrderStatusViewModel
    
    var body: some View {
        HStack {
            switch stageNumber {
            case 0:
                Button(action: {
                    viewModel.cancelOrder { successful in
                        if successful {
                            showModal = false
                        } else {
                            
                        }
                    }
                }) {
                    RoundedRectangleFilledButton(title: "Отменить заказ", fontOfTitle: body1FontRubikMedium, colorOfButton: Color.themeColor.opacity(0.2), colorOfTitle: Color.themeColor)
                }
                Spacer()
                Button(action: {
                    if stageNumber != 3 {
                        stageNumber += 1
                    } else {
                        stageNumber = 0
                    }
                }) {
                    RoundedRectangleFilledButton(title: "Переключиться между стадиями", fontOfTitle: body1FontRubikMedium, colorOfButton: Color.themeColor.opacity(0.2), colorOfTitle: Color.themeColor)
                }
            case 1, 2:
                Button(action: {
                    if stageNumber != 3 {
                        stageNumber += 1
                    } else {
                        stageNumber = 0
                    }
                }) {
                    RoundedRectangleFilledButton(title: "Переключиться между стадиями", fontOfTitle: body1FontRubikMedium, colorOfButton: Color.themeColor.opacity(0.2), colorOfTitle: Color.themeColor)
                }
            case 3:
                Button(action: {
                    showModal = false
                }) {
                    RoundedRectangleFilledButton(title: "Оставьте нам отзыв", fontOfTitle: body1FontRubikMedium, colorOfButton: Color.themeColor, colorOfTitle: .mainBackgroundColor)
                }
            default:
                Button(action: {
                    showModal = false
                }) {
                    RoundedRectangleFilledButton(title: "Оставьте нам отзыв", fontOfTitle: body1FontRubikMedium, colorOfButton: Color.themeColor, colorOfTitle: .mainBackgroundColor)
                }
            }
            
        }
        .frame(height: 51)
    }
}

struct OrderDeliveryCard: View {
    let order: Order

    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 16.0)
                .foregroundColor(.mainBackgroundColor)
                .shadow(color: Color.gray.opacity(0.5), radius: 20, x: 0, y: 0)
            VStack(alignment: .leading) {
                HStack {
                    Image("bag.cafe")
                        .font(.system(size: 24))
                        .foregroundColor(Color.gray.opacity(0.5))
                    Text("Заказ")
                        .font(body1FontRubikMedium)
                    Spacer()
                }
                .frame(height: 30)
                .padding(.bottom)
                VStack(alignment: .leading) {
                    ForEach (order.dishes, id: \.dishId) { dish in
                        HStack {
                            Text("\(dish.count)x")
                                .font(body1FontRubikRegular)
                                .frame(width: 30)
                                .padding(.trailing, 10)
                            HStack {
                                Text("\(dish.title ?? "")")
                                    .font(body1FontRubikRegular)
                                    .padding(.trailing, 10)
                                Spacer()
                            }
                            Text("\(dish.price! * dish.count) ₽")
                                .font(body1FontRubikRegular)
                        }
                        .padding(.bottom, 5)
                    }
                }
                
            }
            .padding()
        }
        .padding()
    }
}


//struct OrderDeliveryCard: View {
//    var width: CGFloat
//    var height: CGFloat
//    var order: OrderStore
//    var body: some View {
//        ZStack {
//            RoundedRectangle(cornerRadius: 16)
//                .frame(width: width, height: height, alignment: .center)
//                .foregroundColor(.mainBackgroundColor)
//            HStack {
//                VStack {
//                    HStack {
//                        Image("bag.cafe")
//                            .font(.system(size: 24))
//                            .foregroundColor(Color.gray.opacity(0.8))
//                        Text("Заказ")
//                            .font(body1FontRubikMedium)
////                        List {
//                            ForEach (order.getOrderDishes().keys.sorted(by:{ $0.id > $1.id}), id: \.self) { dish in
//                                HStack {
//                                    Text("\(order.orderScope[dish]!)x")
//                                    Text("\(dish.title)")
//
//                                }
//                            }
////                        }
//
//                    }
//                }
//                Spacer()
//            }
//
//        }
//        .frame(width: width, height: height, alignment: .center)
//    }
//}


struct OrderStatusScreen_Previews: PreviewProvider {
    static var previews: some View {
        OrderStatusScreen(order: Order(id: nil, comment: nil, status: nil, dishes: [], deliveryAddressId: 0, sum: 0, deliveryInfo: nil, createdAt: nil, updatedAt: nil, deliveryAddress: nil), showModal: .constant(true))
        
//        OrderDeliveryCard()
//            .previewLayout(PreviewLayout.sizeThatFits)
//            .padding()
    }
}
