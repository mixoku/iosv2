//
//  OrderStatusView.swift
//  MyCafe
//
//  Created by Михаил on 16.01.2021.
//
import SwiftUI

struct OrderStatusView: View {
    var imageNames: [String] = ["clock", "pot", "currier", "shoppingBag"]
    
    var height: CGFloat = 100
    
    var firstOffset : CGFloat {
        return screenWidth - circleRadius/2
    }
    var offsetXStep : CGFloat {
        return screenWidth * 0.5 + circleRadius/2
    }
    @State var screenWidth: CGFloat = UIScreen.main.bounds.width
    var circleRadius : CGFloat {
        return screenWidth / 4
    }
//    var firstOffset : CGFloat = 310
//    var offsetXStep : CGFloat = 220
    var stage : Stages
    
    var offsetX : CGFloat {
        switch stage {
        case .accepting:
            return firstOffset
        case .cooking:
            return firstOffset  - offsetXStep
        case .delivery:
            return firstOffset - offsetXStep*2
        case .done:
            return firstOffset - offsetXStep*3
        }
    }
    
    var stageNumber : Int {
        switch stage {
        case .accepting:
            return 0
        case .cooking:
            return 1
        case .delivery:
            return 2
        case .done:
            return 3
        }
    }
    
    func countOffsetOfNonActive(stageFor: Int) -> CGFloat {
        switch stageNumber - stageFor  {
        case 0:
            return 0
        case -1:
            return -100
        default:
            return 100
        }
    }
    
    var body: some View {
//        ScrollView(.horizontal) {
        HStack {
            ZStack {
//                    background
                HStack {
                    Rectangle()
                        .frame(width: screenWidth/2 , height: 5)
                        .foregroundColor(Color.white.opacity(0.8))
                    Rectangle()
                        .frame(width: screenWidth/2 + circleRadius/2, height: 5)
                        .foregroundColor(Color.gray.opacity(0.8))
                    Rectangle()
                        .frame(width: screenWidth/2 + circleRadius/2, height: 5)
                        .foregroundColor(Color.red.opacity(0.8))
                    Rectangle()
                        .frame(width: screenWidth/2 + circleRadius/2, height: 5)
                        .foregroundColor(Color.yellow.opacity(0.8))
                    Rectangle()
                        .frame(width: screenWidth/2 , height: 5)
                        .foregroundColor(Color.white.opacity(0.8))
                }
                .frame(width: screenWidth * 2.5)
                HStack {
                    Rectangle()
                        .frame(width: screenWidth/2 + circleRadius/2, height: 5)
                        .foregroundColor(Color.white.opacity(0.8))
                    ZStack {
                        Rectangle()
                            .foregroundColor(Color.gray.opacity(0.8))
                            .frame(width: (screenWidth/2 + circleRadius/2) * 3 , height: 5)
                        HStack {
                            Rectangle()
                                .frame(width: screenWidth * 0.66 * CGFloat(stageNumber), height: 5)
                                .animation(.easeIn)
                                .foregroundColor(.themeColor)
                            Spacer()
                        }
                        
                    }
                    .frame(width: (screenWidth/2 + circleRadius/2) * 3 , height: 5)
                    Rectangle()
                        .frame(width: screenWidth/2 + circleRadius/2, height: 5)
                        .foregroundColor(Color.white.opacity(0.8))
                }
                HStack {
                    Rectangle()
                        .frame(width: screenWidth/2 - circleRadius/2, height: 5)
                        .foregroundColor(Color.white.opacity(0.0001))

                    ForEach(0 ..< 4) { stageFor in
                        OrderStatusCircle(imageName: imageNames[stageFor], radius: screenWidth / 4, isActive: stageNumber >= stageFor ? true : false)
                            .animation(.easeInOut)
                            .offset(x: countOffsetOfNonActive(stageFor: stageFor))

                        Rectangle()
                            .frame(width: screenWidth/2 - circleRadius/2, height: 5)
                            .foregroundColor(Color.white.opacity(0.0001))
                    }
                    
                }
            }
            .frame(width: screenWidth * 2.5)
        }
        .frame(width: screenWidth)
        .offset(x: offsetX)
        .animation(.easeInOut)
    }
}

struct OrderStatusView_Previews: PreviewProvider {
    static var previews: some View {
        OrderStatusView(screenWidth: UIScreen.main.bounds.width, stage: .accepting)
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
        OrderStatusView(screenWidth: UIScreen.main.bounds.width, stage: .cooking)
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
        OrderStatusView(screenWidth: UIScreen.main.bounds.width, stage: .delivery)
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
        OrderStatusView(screenWidth: UIScreen.main.bounds.width, stage: .done)
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
    }
}
