//
//  OrderStatusViewModel.swift
//  CafeApp
//
//  Created by Alexey Antonov on 30/01/21.
//

import Foundation

final class OrderStatusViewModel: ViewModel {
    private(set) var order: Order
    
    init(order: Order) {
        self.order = order
        
        super.init()
    }
    
    func cancelOrder(completion: @escaping (Bool)->()) {
        if let restaurantId = Restaurant.lastId, let orderId = order.id {
            getHttpResponse(from: Endpoint.cancelOrder(restaurantId: restaurantId, orderId: orderId)) { (response) in
                if let code = response?.statusCode, 200...299 ~= code {
                    completion(true)
                    print("Order \(orderId) is successfully canceled")
                } else {
                    print(response)
                    completion(false)
                }
            }
        }
    }
    
    
}
