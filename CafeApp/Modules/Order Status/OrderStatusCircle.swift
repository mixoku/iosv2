//
//  OrderStatusCircle.swift
//  MyCafe
//
//  Created by Михаил on 16.01.2021.
//

import SwiftUI

struct OrderStatusCircle: View {
    
    var imageName: String
    var radius: CGFloat
    
    var circleColor: Color {
        switch isActive {
        case true:
            return Color.themeColor
        default:
            return Color.gray
        }
    }
    var imageColor: Color {
        switch isActive {
        case true:
            return Color.mainBackgroundColor
        default:
            return Color.gray
        }
    }
    var imageSize: CGFloat {
        return 32
    }
    var isActive: Bool
    
    var body: some View {
        ZStack {
            Circle()
                .frame(width: isActive ? radius : radius * 0.66, height: isActive ? radius : radius * 0.66)
                .foregroundColor(circleColor)
                .animation(.easeInOut)
            Image(imageName)
                .foregroundColor(imageColor)
                .font(.system(size: imageSize))
                .scaleEffect(isActive ? 1 : 0.66)
                .animation(.easeInOut)
        }
    }
}

struct OrderStatusCircle_Previews: PreviewProvider {
    static var previews: some View {
        OrderStatusCircle(imageName: "pot", radius: 96, isActive: true)
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
    }
}
