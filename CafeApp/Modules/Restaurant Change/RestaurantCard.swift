//
//  RestaurantCard.swift
//  CafeApp
//
//  Created by Михаил on 31.01.2021.
//

import SwiftUI

struct RestaurantCard: View {
    @Binding var isMain: Int
    @Binding var showModal: Bool
    var restaurant: Restaurant

    var cornerRadius: CGFloat = 16
    
    var body: some View {
        ZStack(alignment: .leading) {
            RoundedRectangle(cornerRadius: cornerRadius)
                .foregroundColor(restaurant.themeColor)
            VStack(alignment: .leading) {
                HStack {
                    NavigationLink(
                        destination: RestaurantSelectView(showModal: $showModal, restaurant: restaurant)
                            .navigationBarHidden(true),
                        label: {
                            AsyncImage(urlString: restaurant.logoUrl) {
                                Image("imageExample")
                                    .resizable()
                            }
                            .frame(width: 50, height: 50)
                            .clipShape(Circle())
                        })
                    Spacer()
                    NavigationLink(
                        destination: RestaurantSelectView(showModal: $showModal, restaurant: restaurant)
                            .navigationBarHidden(true),
                        label: {
                            Image(systemName: "gear")
                                .font(.system(size: 30))
                                .foregroundColor(.mainBackgroundColor)
                        })
                }
                
                NavigationLink(
                    destination: RestaurantSelectView(showModal: $showModal, restaurant: restaurant)
                        .navigationBarHidden(true),
                    label: {
                            Text(restaurant.title)
                                .font(header2FontRubikBold)
                                .foregroundColor(.mainBackgroundColor)
                    })
                HStack {
                    Text(restaurant.description != "" ? restaurant.description : "Restaurant Description")
                        .foregroundColor(.mainBackgroundColor)
                        .font(body1FontRubikRegular)
                        .lineLimit(3)
//                        .padding()
                    Spacer()
                }
                Spacer()
            }
            .padding()
        }
    }
}

struct RestaurantCard_Previews: PreviewProvider {
    static var previews: some View {
        RestaurantCard(isMain: .constant(0), showModal: .constant(false), restaurant: theRestaurant)
            .frame(width: UIScreen.main.bounds.width - 40, height: 200)
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
    }
}
