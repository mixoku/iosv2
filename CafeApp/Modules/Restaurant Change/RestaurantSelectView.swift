//
//  RestaurantSelectView.swift
//  MyCafe
//
//  Created by Михаил on 07.01.2021.
//

import SwiftUI

struct RestaurantSelectView: View {
    @EnvironmentObject var generalViewModel: GeneralViewModel
    @EnvironmentObject var orderStore: OrderStore
    @EnvironmentObject var menuVM: MenuViewModel
    @Environment(\.presentationMode) var presentationMode
    @Binding var showModal: Bool
    @State var restaurant: Restaurant
    @State var next: Restaurant?
    
    var restaurantColor: Color { restaurant.themeColor }
    var restaurantName: String { restaurant.title }
    var restaurantDescription: String { restaurant.description }
    var resturantWorkingHours: String { restaurant.deliveryTime ?? "" }
    
    @State var offsetX = UIScreen.main.bounds.width * 0.8
    @State var draggedOffsetX = UIScreen.main.bounds.width * 0.8
    @State var scaleEffectOfCard : CGFloat = 1
    @State var opacityOfCard : Double = 1
    
    @State var changeRestaurant: Bool = false
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                restaurantColor
                    .ignoresSafeArea()
                VStack(alignment: .leading) {
                    HStack {
                        Button(action: {
                            presentationMode.wrappedValue.dismiss()
                        }) {
                            Image("arrow.left.cafe")
                            .font(.system(size: 30))
                            .foregroundColor(.mainBackgroundColor)
                        }
                        Spacer()
                    }
//                    .padding(.top, 40)
                    .padding()
                    HStack {
//                        Text(String(Int(offsetX)))
                        Text("Выбор \nресторана")
                            .foregroundColor(.mainBackgroundColor)
                            .font(header2FontRubikBold)
                        Spacer()
                        Button(action: {
                            
                        }) {
                            ZStack {
                                Circle()
                                    .foregroundColor(.mainBackgroundColor)
                                    .frame(width: 64)
                                Image(systemName: "plus")
                                .font(.system(size: 30))
                                    .foregroundColor(restaurant.themeColor)
                            }
                        }
                    }
                    .frame(height: geometry.size.height/10)
                    .padding()
                    ZStack {
                        Circle()
                            .foregroundColor(.mainBackgroundColor)
                            .frame(width: 64)
                        AsyncImage(urlString: restaurant.images.first?.images.small ?? "") {
                            Image("imageExample")
                                .resizable()
                        }
                        //.scaledToFill()
                        .aspectRatio(1,contentMode: .fit)
                        .cornerRadius(32)
                        .clipped()
                    }
                    .frame(height: 64)
                    .padding()
                    HStack {
                        Text(restaurantName)
                            .foregroundColor(.mainBackgroundColor)
                            .font(header1FontRubik)
//                            .frame(width: geometry.size.width*0.66)
                        Spacer(minLength: 100)
                    }
                    .padding()
                    HStack {
                        Text(restaurantDescription)
                            .foregroundColor(.mainBackgroundColor)
                            .font(body1FontRubikMedium)
//                            .frame(width: geometry.size.width*0.7)
                        Spacer(minLength: 100)
                    }
                    .padding()
                    HStack {
                        Text(resturantWorkingHours)
                            .foregroundColor(.mainBackgroundColor)
                            .font(body2FontRubik)
                        Spacer()
                    }
                    .padding()
                    Spacer()
                    if restaurant.id != Restaurant.lastId {
                        Button(action:  {
                            orderStore.clearCart()
                            generalViewModel.selectRestaurant(with: restaurant.id) { (cafe) in
                                if let cafe = cafe {
                                    orderStore.clearCart()
                                    menuVM.changeRestaurant(for: cafe.id)
                                    showModal = false
                                }
                            }
                        }) {
                            RoundedRectangleFilledButton(title: "Выбрать", fontOfTitle: body1FontRubikMedium, colorOfButton: .mainBackgroundColor, colorOfTitle: .mainAccentColor)
                        }
                        .frame(height: 51, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .padding()
                    }
                }
                
                .frame(width: geometry.size.width)
                Circle()
                    .frame(width: changeRestaurant ? 100 : 0.001, height: changeRestaurant ? 100 : 0.001, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .scaleEffect(changeRestaurant ? 1 : 1000)
                    .animation(.easeInOut(duration: 2))
                    .foregroundColor(restaurant.themeColor)
//                    .foregroundColor(viewModel.restaurants[viewModel.nextRestaurantIndex].themeColor)
                    .opacity(changeRestaurant ? 0 : 1)
                    .animation(Animation.easeInOut(duration: 2))
                if next != nil {
                    RestaurantCard(isMain: .constant(0), showModal: $showModal, restaurant: next!)
                        .frame(width: 300, height: 200)
                        .offset(x: offsetX)
                        .scaleEffect(scaleEffectOfCard)
                        .opacity(opacityOfCard)
                        .gesture(
                            DragGesture()
                                .onChanged({ value in
                                    
                                    if value.location.x > 25 {
                                        offsetX = value.location.x
                                        opacityOfCard = Double( value.location.x * 2 / geometry.size.width)
    //                                    scaleEffectOfCard =  geometry.size.width * 0.3 / value.location.x
                                        print(value.location.x)
                                    } else {
                                        if value.location.x < 25 {
                                            offsetX = 25
                                            
                                        }
                                    }
                                })
                                .onEnded({ value in
                                    if value.location.x > geometry.size.width / 2 {
                                        withAnimation {
                                        offsetX = geometry.size.width * 0.8
                                            opacityOfCard = 1
                                        }
                                        
    //                                    scaleEffectOfCard =  geometry.size.width * 0.3 / value.location.x
                                    } else {
                                        if value.location.x < geometry.size.width {
                                            changeRestaurant = true
    //                                        if currentRestaurant == 0 {
    //                                            currentRestaurant = 1
    //                                            nextRestaurant = 0
    //                                        } else {
    //                                            currentRestaurant = 0
    //                                            nextRestaurant = 1
    //                                        }
                                            print("OK")
                                            offsetX = geometry.size.width * 0.8
                                            opacityOfCard = 1
                                            
                                        }
                                    }
                                    print(value.location.x)
                                    draggedOffsetX = offsetX
                                })
                    )
                } else {
                    /*@START_MENU_TOKEN@*/EmptyView()/*@END_MENU_TOKEN@*/
                }
                    
            }
            .background(restaurantColor)
            .ignoresSafeArea(edges: .bottom)
        }
        .background(restaurantColor)
//        .ignoresSafeArea(edges: .top)
    }
}



struct RestaurantSelectView_Previews: PreviewProvider {
    static var previews: some View {
        RestaurantSelectView(showModal: .constant(true), restaurant: restaurantsSample[0]).environmentObject(RestaurantCarouselViewModel(restaurant: theRestaurant))
    }
}
