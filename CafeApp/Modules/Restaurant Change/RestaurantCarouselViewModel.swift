//
//  RestaurantCarouselViewModel.swift
//  CafeApp
//
//  Created by Alexey Antonov on 26/01/21.
//

import Foundation
import Combine

final class RestaurantCarouselViewModel: ObservableObject {
    @Published private(set) var restaurants = [Restaurant]()
    @Published var activeCard: Int = 0
    @Published var screenDrag: Float = 0.0
    var nextRestaurantIndex: Int {
        activeCard + 1
    }
    
    init(restaurant: Restaurant) {
        restaurants = [Restaurant]()
//        maybe its wrong
//        restaurants = restaurantsSample
//        restaurants.append(restaurant)
        
        if let restaurantIds = UserDefaults.standard.array(forKey: "userRestaurants") as? [Int] {
            for id in restaurantIds { //.filter({ $0 != restaurant.id }) {
                Endpoint.restaurant(id: id)
                    .fetch()
                    .sink { (completion) in
                        switch completion {
                        case .failure(let error):
                            print(error.localizedDescription)
                        case .finished:
                            break
                        }
                    } receiveValue: { [weak self] (item: Restaurant) in
                        self?.restaurants.append(item)
                    }
                    .store(in: &cancellables)
            }
        }
    }
    
    private var cancellables = Set<AnyCancellable>()
    
    deinit {
        for cancellable in cancellables {
            cancellable.cancel()
        }
    }
}
