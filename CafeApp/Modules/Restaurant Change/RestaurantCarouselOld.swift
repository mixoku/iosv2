//
//  RestaurantCarousel.swift
//  CafeApp
//
//  Created by Alexey Antonov on 26/01/21.
//

import SwiftUI

enum CardPosition : CGFloat {
    case closed, main, backed
    
    func changePosition(width: CGFloat) -> CGFloat {
        switch self {
        case .closed:
            return -width * 0.66
        case .main:
            return width/8
        case .backed:
            return width/5 + 20
        }
    }
    
    func changeScale() -> CGFloat {
        switch self {
        case .closed:
            return 1
        case .main:
            return 1
        case .backed:
            return 0.8
        }
    }
}

struct RestaurantCarouselOld : View {
    @ObservedObject private var viewModel: RestaurantCarouselViewModel
    
    var row : [Int] = []
    
    @State var isMain: Int = 1
    
    init(restaurantId: Int) {
        let res = restaurantsSample.firstIndex(where: { $0.id == restaurantId })
        viewModel = RestaurantCarouselViewModel(restaurant: restaurantsSample[res ?? 0])
        isMain = res ?? 0//restaurantId
        var rowc : [Int] = []
        for i in viewModel.restaurants {
            rowc.append(i.id)
        }
        row = rowc
        //isMain = row.count - 1
        var positions: [CGFloat] = []
        var scale: [CGFloat] = []
        for i in viewModel.restaurants {
            print(row)
            switch row.firstIndex(where: {$0 == i.id})! {
            case ..<row[isMain]:
                positions.append(CardPosition.backed.changePosition(width: UIScreen.main.bounds.width))
                scale.append(CardPosition.backed.changeScale())
                print("...row[isMain]")
            case row[isMain]:
                positions.append(CardPosition.main.changePosition(width: UIScreen.main.bounds.width))
                scale.append(CardPosition.main.changeScale())
                print("row[isMain]")
            default:
                positions.append(CardPosition.closed.changePosition(width: UIScreen.main.bounds.width))
                scale.append(CardPosition.closed.changeScale())
                print("row[isMain]...")
                
            }
        }
        
        offsetX = positions
//        print("isMain positions", positions[isMain])
        draggedX = positions
        cardScale = scale
        
        var positionsC: [CardPosition] = []
        for i in viewModel.restaurants {
            switch row.firstIndex(where: {$0 == i.id})! {
            case ...row[isMain]:
                positionsC.append(.backed)
            case row[isMain]...:
                positionsC.append(.closed)
            default:
                positionsC.append(.main)
            }
        }
        
        cardStatus = positionsC
    }
    
    var restaurants : [Restaurant] {
        return viewModel.restaurants
    }
    
    func encreaseMainCard() {
//        print("ned isMain e", isMain)
//        print("ned cardStatus.count", cardStatus.count)
        if isMain != cardStatus.count - 1 {
            cardStatus[isMain] = .backed
            cardStatus[isMain + 1] = .main
//            print("ned cardStatus", cardStatus)
            if isMain + 2 < row.count {
                cardStatus[isMain + 2] = .closed
            }
            isMain += 1
            
            offsetX = offsetCount(cards: cardStatus)
            draggedX = offsetX
            cardScale = scaleCount(cards: cardStatus)
//            print(offsetX)
        }
//        print("ned translation isMain become", isMain)
    }
    
    func decreaseMainCard() {
        print("translation isMain d", isMain)
        if isMain != 0 {
            cardStatus[isMain] = .closed
            
            cardStatus[isMain - 1] = .main
            if isMain - 2 > -1 {
                cardStatus[isMain - 2] = .backed
            }
            isMain -= 1
            offsetX = offsetCount(cards: cardStatus)
            draggedX = offsetX
            cardScale = scaleCount(cards: cardStatus)
        }
        print("translation isMain become", isMain)
    }

    @State var offsetX: [CGFloat] = []
    
    @State var cardStatus: [CardPosition] = [.backed, .main, .closed]
    
    func offsetCount(cards: [CardPosition]) -> [CGFloat] {
        var xOffset: [CGFloat] = Array(repeating: 0, count: cardStatus.count)
        for i in (0 ..< cardStatus.count) {
            switch cardStatus[i] {
            case .closed:
                xOffset[i] = CardPosition.closed.changePosition(width: UIScreen.main.bounds.width)
            case .backed:
                xOffset[i] = CardPosition.backed.changePosition(width: UIScreen.main.bounds.width)
            case .main:
                xOffset[i] = CardPosition.main.changePosition(width: UIScreen.main.bounds.width)
            }
        }
        return xOffset
    }

    func scaleCount(cards: [CardPosition]) -> [CGFloat] {
        var xScale: [CGFloat] = Array(repeating: 0, count: cardStatus.count)
        for i in (0 ..< cardStatus.count) {
            switch cardStatus[i] {
            case .closed:
                xScale[i] = CardPosition.closed.changeScale()
            case .backed:
                xScale[i] = CardPosition.backed.changeScale()
            case .main:
                xScale[i] = CardPosition.main.changeScale()
            }
        }
        return xScale
    }
    
    @State var draggedX: [CGFloat] = [
        CardPosition.backed.changePosition(width: UIScreen.main.bounds.width),
        CardPosition.main.changePosition(width: UIScreen.main.bounds.width),
        CardPosition.closed.changePosition(width: UIScreen.main.bounds.width)]

    @State var cardScale: [CGFloat] =
        [CardPosition.backed.changeScale(),
        CardPosition.main.changeScale(),
        CardPosition.closed.changeScale()]
    
    @State var width: CGFloat = UIScreen.main.bounds.width
    
    @State var count: CGFloat = 0
    
    @State var show = false
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                ForEach(0 ..< restaurants.count) { card in
                    NavigationLink(
                        destination: RestaurantSelectView(showModal: $show, restaurant: restaurantsSample[0]).environmentObject(viewModel).navigationBarHidden(true),
                        label: {
                            RestaurantCard(isMain: $isMain, showModal: $show, restaurant: restaurants[card])
                                
                                .offset(x: offsetX.count > 0 ? offsetX[card] : 0)
                            .animation(.easeInOut)
                            .scaleEffect(cardScale[card])
                            .animation(.easeInOut)
                            
                        }
                    )
                }
                Rectangle()
                    .foregroundColor(Color.white.opacity(0.0001))
                    .gesture(
                        DragGesture(minimumDistance: 0, coordinateSpace: .global)
                        .onChanged({ value in
                        print("translation of \(value.predictedEndTranslation.width)")
                            if offsetX.count > 0 {
                        switch value.translation.width {
                        case -1000...0:
                            offsetX[isMain] = value.translation.width + draggedX[isMain]
//                                    if offsetX[isMain] > CardPosition.closed.changePosition(width: geometry.size.width) {
//                                        offsetX[isMain] = value.translation.width + draggedX[isMain]
//                                    } else {
//                                        offsetX[isMain] = CardPosition.closed.changePosition(width: geometry.size.width)
//                                        if isMain != cardStatus.count - 1 {
//                                            offsetX[isMain + 1] = value.translation.width + draggedX[isMain + 1]
//                                        }
//                                    }
                        case 0...1000:
                            offsetX[isMain] = value.translation.width + draggedX[isMain]
//                                    if offsetX[isMain] < CardPosition.backed.changePosition(width: geometry.size.width) {
//                                        offsetX[isMain] = value.translation.width + draggedX[isMain]
//                                    } else {
//                                        offsetX[isMain] = CardPosition.backed.changePosition(width: geometry.size.width)
//                                        if isMain != 0 {
//                                            offsetX[isMain - 1] = value.translation.width + draggedX[isMain - 1]
//                                        }
//                                    }
                                        
                        default:
                            print("FF")
                        }
                            }
                            //                        switch value.translation.x {
                            //                        case 0...100:
                            //                        }
                            //                        switch value.location.x {
                            //                        case 50...200 :
                            //                            if card != x.count - 1 && card != 0 {
                            //                                x[card+1] = value.translation.width + draggedX[card + 1]
                            //                                print("case50.200")
                            //                            }
                            //                        case 0...50 :
                            //                            if card != x.count - 1 && card != 0 {
                            //                                x[card+1] = value.translation.width + draggedX[card + 1]
                            //                                print("case0.50")
                            //                            }
                            //
                            //                        case -200...0:
                            //                            if card != x.count - 1 {
                            //                                x[card+1] = value.translation.width + draggedX[card + 1]
                            //                                print("case-200.0")
                            //                            }
                            //                        default:
                            //                            if card != x.count - 1 && card != 0 {
                            //                                x[card+1] = value.translation.width + draggedX[card + 1]
                            //                                print("cased")
                            //                            }
                            //                        }
                        print("F", value.translation.width)
                        print("offset", value.translation.width + draggedX[isMain])
                    })
                        .onEnded({ value in
                            switch value.predictedEndTranslation.width + draggedX[isMain] {
                            case -1000...0:
                                decreaseMainCard()
                            case 0...1000:
                                encreaseMainCard()
//                                                if offsetX[isMain] < CardPosition.backed.changePosition(width: geometry.size.width) {
//                                                    offsetX[isMain] = value.translation.width + draggedX[isMain]
//                                                } else {
//                                                    offsetX[isMain] = CardPosition.backed.changePosition(width: geometry.size.width)
//                                                    draggedX[isMain] = offsetX[isMain]
//                                                    encreaseMainCard()
//                                                }
                            default:
                                print("FF")
                            }
                        })
                    )
            }
            .frame(width: UIScreen.main.bounds.width, alignment: .center)
            .animation(.spring())
        }
        .frame(width: UIScreen.main.bounds.width, height: 200)
    }
}

//
//    var cornerRadius: CGFloat = 16
//    
//    var body: some View {
//        ZStack(alignment: .topLeading) {
//            RoundedRectangle(cornerRadius: cornerRadius)
//                .foregroundColor(restaurant.themeColor)
//            VStack(alignment: .leading) {
//                AsyncImage(urlString: restaurant.logoUrl) {
//                    Image("imageExample")
//                        .resizable()
//                }
//                    .frame(width: 45.29, height: 45.29)
//                    .clipShape(Circle())
//                Text(restaurant.title)
//                    .foregroundColor(.mainBackgroundColor)
//                Text(restaurant.description != "" ? restaurant.description : "Restaurant Description")
//                    .foregroundColor(.mainBackgroundColor)
//            }
//            .padding()
//        }
//    }
//}

struct RestaurantCarouselOld_Previews: PreviewProvider {
    static var previews: some View {
        RestaurantCard(isMain: .constant(0), showModal: .constant(true), restaurant: theRestaurant)
            .frame(width: UIScreen.main.bounds.width, height: 200)
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
        RestaurantCarouselOld(restaurantId: 1)
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
//            .previewDisplayName("Big Banner")
//        SnapCarousel(restaurant: Restaurant(id: 1, title: "", description: "", siteUrl: "", logoUrl: "", colorScheme: "", phone: "", aboutCompany: "", workTime: "", aboutDelivery: "", deliveryTime: "", images: []))
    }
}
