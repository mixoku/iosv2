//
//  Acquiring.swift
//  CafeApp
//
//  Created by Alexey Antonov on 25/01/21.
//

import SwiftUI
import WebKit

struct WebView: UIViewRepresentable{
    let callback: (URL)->()
    let endpoint: Endpoint
    
    @Binding var paymentSuccessful: Bool

    func makeCoordinator() -> WebView.Coordinator {
        Coordinator(self)
    }
    
    var request: URLRequest {
        endpoint.request
    }

    private let webview = WKWebView()

    fileprivate func loadRequest(in webView: WKWebView) {
        webView.load(request)
    }

    func makeUIView(context: UIViewRepresentableContext<WebView>) -> WKWebView {
        webview.navigationDelegate = context.coordinator
        loadRequest(in: webview)
        return webview
    }

    func updateUIView(_ uiView: WKWebView, context: UIViewRepresentableContext<WebView>) {
        
        loadRequest(in: uiView)
    }
    
    class Coordinator: NSObject, WKNavigationDelegate {
        let parent: WebView

        init(_ parent: WebView) {
            self.parent = parent
        }

        func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
            if let url = navigationAction.request.url, url.lastPathComponent == "success" {
                parent.callback(url)
                parent.paymentSuccessful = true
                
                decisionHandler(WKNavigationActionPolicy.allow)
            } else {
                decisionHandler(WKNavigationActionPolicy.allow)
            }
        }
     }
}

struct Acquiring: View {
    let restaurantId: Int
    let orderId: Int
    @Binding var paymentSuccessful: Bool
    let cancelCallback: ()->()
    let callback: (URL)->()
    
    var body: some View {
        VStack {
            HStack {
                Text("Оплата")
                    .font(.title)
                Spacer()
                Button(action: {
                    cancelCallback()
                }, label: {
                    Text("Изменить способ оплаты")
                })
            }
            .padding()
            WebView(callback: callback, endpoint: Endpoint.orderPayment(restaurantId: restaurantId, orderId: orderId), paymentSuccessful: $paymentSuccessful)
                .edgesIgnoringSafeArea(.bottom)
        }
    }
}
