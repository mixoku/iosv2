//
//  CardListView.swift
//  CafeApp
//
//  Created by Alexey Antonov on 28/01/21.
//

import SwiftUI

struct CardListView: View {
    @ObservedObject var viewModel = CardListViewModel()
    @State private var addURL: URL?
    
    var body: some View {
//
            if (viewModel.isLoading) {
                if viewModel.error == nil {
                    VStack {
                        
                        LoadingView()
                        
                    }
                } else {
                    ErrorView(error: viewModel.error!)
                }
            } else {
                Group {
//
                if (viewModel.cards.count > 0) {
                    List(viewModel.cards) { card in
                        HStack {
                            Image(systemName: card.isDefault ? "checkmark.circle.fill" : "")
                                .foregroundColor(.green)
                            Text("\(card.pan)")
                            Spacer()
                            Button(action: {
                                viewModel.removeCard(id: card.id)
                            }, label: {
                                Image(systemName: "trash")
                            })
                        }
                    }
                } else {
                    VStack {
                        
                        Button(action: {
                            viewModel.addCard() { url in
                                addURL = url
                            }
                        }) {
                            RoundedRectangleFilledButton(title: "Добавить карту", fontOfTitle: body1FontRubikRegular, colorOfButton: .themeColor, colorOfTitle: .mainBackgroundColor)
                            
                        }
                        .frame(height: 50)
                        .padding()
                    }
                }
            }
                .sheet(item: $addURL) { url in
                    WebView(callback: { _ in viewModel.fetchCards() }, endpoint: Endpoint.paymentSuccess(url: url), paymentSuccessful: Binding<Bool>(get: { return addURL != nil }, set: { addURL = $0 ? addURL : nil } ))
            }
        }
    }
}

extension URL: Identifiable {
    public var id: URL { self }
}

struct CardListView_Previews: PreviewProvider {
    static var previews: some View {
        CardListView()
    }
}
