//
//  NewPaymentEnter.swift
//  CafeApp
//
//  Created by Михаил on 03.02.2021.
//

import SwiftUI

struct NewPaymentEnter: View {
    var titleCardNumber: String = "Номер карты"
    @State var cardNumber: String = ""
    
    var titleCardMonth: String = "ММ"
    @State var cardMonth: String = ""
    
    var titleCardYear: String = "ГГ"
    @State var cardYear: String = ""
    
    var titleCardCVV: String = "CVV/CVC-код"
    @State var cardCVV: String = ""
    
    @State var rememberCard = true
    
    var titleOfTabButton = "Выбрать"
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                VStack {
                    VStack {
                        HStack {
                            Text("Новая карта")
                            .font(header2FontRubikBold)
                            .multilineTextAlignment(.leading)
                            .foregroundColor(.mainAccentColor)
                            Spacer()
                        }
                        FieldToEnter(titleIsText: cardNumber == "" ? false : true, title: titleCardNumber, mainText: $cardNumber)
                        .padding(.bottom, 20)
                        HStack {
                            FieldToEnter(titleIsText: cardMonth == "" ? false : true, title: "\(titleCardMonth)", mainText: $cardMonth)
                            Text("/")
                                .font(header2FontRubikBold)
                                .foregroundColor(.mainAccentColor)
                            FieldToEnter(titleIsText: cardYear == "" ? false : true, title: "\(titleCardYear)", mainText: $cardYear)
                            FieldToEnter(titleIsText: cardCVV == "" ? false : true, title: titleCardCVV, mainText: $cardCVV)
                        }
                        .padding(.bottom, 20)
                        HStack {
                            ZStack {
                                RoundedRectangle(cornerRadius: 10)
                                    .foregroundColor(rememberCard ? .themeColor : .gray)
                                if rememberCard {
                                    Image(systemName: "checkmark")
                                        .font(.system(size: 20))
                                        .foregroundColor(.mainBackgroundColor)
                                }
                                    
                            }
                            .frame(width: 30, height: 30, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .onTapGesture {
                                rememberCard.toggle()
                            }
                            Text("Запомнить карту для следующих заказов")
                                .font(body1FontRubikRegular)
                                .foregroundColor(.gray)
                        }
                    }
                    .padding()
                    Spacer()
                }
                VStack {
                    Spacer()
                    Button(action: {
                        
                    }) {
                        TabViewButtonPattern(title: titleOfTabButton)
                    }
                    
                }
                
            }
            
        }
    }
}

struct NewPaymentEnter_Previews: PreviewProvider {
    static var previews: some View {
        NewPaymentEnter()
    }
}
