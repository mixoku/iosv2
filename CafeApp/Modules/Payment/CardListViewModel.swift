//
//  CardListViewModel.swift
//  CafeApp
//
//  Created by Alexey Antonov on 28/01/21.
//

import Foundation

final class CardListViewModel: ViewModel {
    @Published private(set) var cards = [PaymentCard]()
    @Published private(set) var isLoading = true
    
    override init() {
        super.init()
        
        fetchCards()
    }
    
    func addCard(callback: @escaping (URL)->()) {
        fetch(from: Endpoint.addCard) { (result: [String: String]) in
            if let urlString = result["url"], let url = URL(string: urlString) {
                callback(url)
            }
        }
    }
    
    func removeCard(id: Int) {
        getHttpResponse(from: Endpoint.removeCard(id: id)) { [weak self] (response) in
            if let status = response?.statusCode, 200...299 ~= status {
                self?.fetchCards()
            } else {
                self?.error = .badCoding
            }
        }
    }
    
    func fetchCards() {
        isLoading = true
        
        fetch(from: Endpoint.cardsList) { [weak self] (cards: [PaymentCard]) in
            self?.cards = cards
            self?.isLoading = false
        }
    }
}
