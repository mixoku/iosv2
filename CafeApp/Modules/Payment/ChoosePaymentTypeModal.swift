//
//  ChoosePaymentType.swift
//  CafeApp
//
//  Created by Михаил on 03.02.2021.
//

import SwiftUI



struct ChoosePaymentTypeModal: View {
    @Binding var chosenTypeOfPayment: PaymentCard?
    @Binding var showModal: Bool
    
    @ObservedObject var viewModel = CardListViewModel()
    
    var body: some View {
        ModalPatternWithPlusButton(content: AnyView(
            ScrollView(.vertical) {
                ForEach(viewModel.cards) { card in
                    RoundedBubbleButtonWithShadow(content: AnyView(
                            HStack {
                                Image("debitCard")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 38, height: 30)
                                VStack(alignment: .leading) {
                                    Text("Карта")
                                        .font(body1FontRubikMedium)
                                        .foregroundColor(.mainAccentColor)
                                        Text(card.pan)
                                            .font(body2FontRubik)
                                            .foregroundColor(.gray)
                                    
                                }
                                Spacer()
                                if card.isDefault {
                                    Image(systemName: "checkmark")
                                        .font(.system(size: 25))
                                }
                            }
                            .frame(height: 50)
                        )
                    
                    
                    
                    )
                    
                    .onTapGesture {
                        chosenTypeOfPayment = card
                    }
                    
                }
                .padding(.top)
                Spacer()
                
                Spacer()
            }
        ), modalContent: AnyView(
            NewPaymentEnter()
        ), title: "Способ оплаты") { (showModal: Bool) -> Bool in
            return true
        }
    }
}

func addPaymentType(showModal: Bool) -> (Bool) {
    
    let delivery = DeliveryAddressDetails(city: "Moscow", street: "Sayanskaya", home: "21", flat: "20", comment: "Domophone is not working")
    
    print(delivery)
    return !showModal
}


struct ChoosePaymentTypeModal_Previews: PreviewProvider {
    static var previews: some View {
        ChoosePaymentTypeModal(chosenTypeOfPayment: .constant(PaymentCard(id: 0, pan: "1111", cardName: nil, cardType: "MC", isDefault: true)), showModal: .constant(true))
    }
}
