//
//  AboutView.swift
//  MyCafe
//
//  Created by Михаил on 28.12.2020.
//

import SwiftUI

struct AboutView: View {
    let restaurant: Restaurant
    
    var photoGalleryImageNames = ["imageExample", "imageExample"]
    
    var address: String = "г. Москва, ул. Ленина, 46"
    var email: String = "info@jjpizza.su"
    
    var body: some View {
        GeometryReader {  geometry in
            ScrollView(.vertical) {
                VStack(alignment: .leading) {
                    HStack {
                        Text(restaurant.title)
                            .font(header2FontRubikBold)
                            .foregroundColor(.mainAccentColor)
                            .padding(.bottom, 25)
                        Spacer()
                    }
                    
                    Text(restaurant.description)
                        .font(body1FontRubikRegular)
                        .padding(.bottom, 30)
                    ScrollView(.horizontal) {
                        HStack {
                            ForEach(photoGalleryImageNames, id:\.self) { photo in
                                PhotoRoundedRectangle(imageName: photo, width: geometry.size.width*0.75, height: geometry.size.height*0.25, cornerRadius: 16)
                            }
                        }
                        
                    }
                    .padding(.bottom, 65)
                    VStack(alignment: .leading) {
                        if restaurant.aboutDelivery != nil {
                            Text("Доставка")
                                .font(header2FontRubikBold)
                                .foregroundColor(.mainAccentColor)
                                .padding(.bottom, 25)
                            Text(restaurant.aboutDelivery!)
                                .font(body1FontRubikRegular)
                                .lineLimit(nil)
                                .frame(height: 40)
                                .padding(.bottom, 25)
                        }
                        if restaurant.deliveryTime != nil {
                            Text("Время доставки:")
                                .font(body1FontRubikMedium)
                                .foregroundColor(.mainAccentColor)
                                .padding(.bottom, 25)
                            Text(NSLocalizedString(restaurant.deliveryTime ?? "", comment: "Time") )
                                .font(body1FontRubikRegular)
                                .lineLimit(nil)
                                .fixedSize(horizontal: false, vertical: true)
                        }
//                        HStack {
//                            VStack {
//                                Text("ПН - ПТ")
//                                Text("CБ - ВС")
//                            }
//                            Spacer()
//                            VStack(alignment: .leading) {
//                                Text("9:00 - 22:00")
//                                Text("9:00 - 2:00")
//                            }
//                            Spacer()
//                        }
//                        .font(body1FontRubikRegular)
                    }
                    .padding(.bottom, 65)
                    VStack(alignment: .leading) {
                        Text("Контакты")
                            .font(header2FontRubikBold)
                            .foregroundColor(.mainAccentColor)
                            .padding(.bottom, 25)
                        HStack {
                            Image("map")
                                .font(.system(size: 25))
                                .foregroundColor(.themeColor)
                                .padding(.trailing, 25)
                            Text(address)
                                .font(body1FontRubikRegular)
                            Spacer()
                        }
                        .onTapGesture {
                            if UIApplication.shared.canOpenURL(URL(string: "https://maps.apple.com/?q=\(address)")!) {
                                UIApplication.shared.open(URL(string: "https://maps.apple.com/?q=\(address)")!)
                            }
                        }
                        .padding(.bottom, 15)
                        HStack {
                            Image("envelope")
                                .font(.system(size: 25))
                                .foregroundColor(.themeColor)
                                .padding(.trailing, 25)
                            Text(email)
                                .font(body1FontRubikRegular)
                            Spacer()
                        }.onTapGesture {
                            UIApplication.shared.open(URL(string: "mailto:\(email)")!)
                        }
                        .padding(.bottom, 15)
                        HStack {
                            Image("phone")
                                .font(.system(size: 25))
                                .foregroundColor(.themeColor)
                                .padding(.trailing, 25)
                            Text(restaurant.phone)
                                .font(body1FontRubikRegular)
                                .onTapGesture {
                                    let telephoneCode = "tel://"
                                    let formattedPhoneString = telephoneCode + (restaurant.phone)
                                        guard let phoneURL = URL(string: formattedPhoneString) else { return }
                                        UIApplication.shared.open(phoneURL)
                                }
                            Spacer()
                        }
                    }
                }
                .padding()
            }
        }
    }
}

struct AboutView_Previews: PreviewProvider {
    static var previews: some View {
        AboutView(restaurant: theRestaurant)
    }
}
