//
//  CardOfDish.swift
//  MyCafe
//
//  Created by Михаил on 05.12.2020.
//

import SwiftUI

struct ModalCardOfDish: View {
    @EnvironmentObject var orderStore: OrderStore
    @ObservedObject var viewModel: DishViewModel
    @Binding var showDetail: Bool
    var dish: Dish
    
    var imageName = "imageExample"
    var heightOfImage : CGFloat = 200
    @State var showingOptionList = 0
    
    init(dish: Dish, show: Binding<Bool>) {
        self.dish = dish
        self.viewModel = DishViewModel(dish: dish)
        _showDetail = show
        showingOptionList = dish.optionGroups.first?.id ?? 0
    }
    
    var body: some View {
        VStack {
            ScrollView(.vertical) {
                VStack(alignment: .leading) {
                    if dish.images.count > 0 {
                        ZStack {
                            AsyncImage(urlString: dish.images.first?.images.big ?? "", placeholder: {
                            ImageWithLikeModal(imageName: imageName)
                        })
                        .scaledToFill()
                        .frame(height: heightOfImage)
                            .clipped()
                        VStack {
                            HStack(alignment: .top) {
                                Spacer()
                                LikeButton()
                                    .padding()
                            }
                            Spacer()
                        }
                        }
                    } else {
                        ImageWithLikeModal(imageName: imageName)
                    }
                    Text(dish.title)
                        .foregroundColor(.mainAccentColor)
                        .font(header2FontRubikBold)
                        
                        .lineLimit(3)
                        .multilineTextAlignment(.leading)
                        .padding()
                        
                    Text(dish.descriptionShort ?? "")
                        .foregroundColor(Color.gray)
                        .font(body1FontRubikMedium)
                        .lineLimit(3)
                        .multilineTextAlignment(.leading)
                        .padding()
                    if dish.optionGroups.count > 0 {
                        ForEach(dish.optionGroups) { group in
                            OptionGroupView(
                                group: group,
//                                isOpened: Binding<Bool>(get: { showingOptionList == group.id }, set: { print($0) }),
                                selectedOptions: Binding<[Int]>(
                                    get: { viewModel.item.options.filter { group.options.contains($0)}.map(\.id) },
                                    set: { selectedOptions in
                                        viewModel.updateOptions(options: group.options.filter { selectedOptions.contains($0.id) }, in: group)
                                    }))
                                .onTapGesture {
                                    withAnimation {
                                        showingOptionList = group.id
                                    }
                                }
                        }
                    }
                }
            }
            ModalDishOrderTabBar(count: Binding<Int>(get: { viewModel.item.count }, set: { viewModel.changeCount(count: $0 )}), orderCost: Binding<Int>(get: { viewModel.item.totalPrice }, set: { print($0) })) {
                orderStore.addToCart(cartOrderItem: viewModel.item)
                showDetail = false
            }
                .frame(height: 120)
        }.ignoresSafeArea(edges: .bottom)
    }
}

struct ModalDishOrderTabBar: View {
    @Binding var count: Int
    @Binding var orderCost: Int
    var callback: ()->()
    
    var body: some View {
        VStack {
            Divider()
            HStack {
                PlusMinusButton(count: $count)
                    .padding(.horizontal)
                Spacer()
                Button(action: {
                    callback()
                }, label: {
                        HStack {
                            Image(systemName: "cart.fill.badge.plus")
                            Spacer()
                            Text("\(orderCost) ₽")
                        }
                        .font(body1FontRubikMedium)
                        .foregroundColor(.white)
                        .padding()
                        .background(Color.themeColor)
                        .cornerRadius(16)
                        .padding(.vertical)
                        .disabled(count == 0)
                        .opacity(count == 0 ? 0.2 : 1)
                }).padding(.horizontal)
            }
            .background(Color.mainBackgroundColor)
        }
    }
}

struct ModalCardOfDish_Preview: PreviewProvider {
    static var previews: some View {
        ModalCardOfDish(dish: pizza , show: .constant(true))
    }
}
