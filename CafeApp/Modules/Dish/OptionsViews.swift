//
//  OptionsViews.swift
//  CafeApp
//
//  Created by Alexey Antonov on 24/01/21.
//

import SwiftUI

struct OptionGroupView: View {
    let group: Dish.OptionGroup
//    @Binding var isOpened: Bool
    @Binding var selectedOptions: [Int]
    
    var body: some View {
        VStack {
            TitleOption(optionTitle: group.title)
//            TitleOption(isOpened: $isOpened, optionTitle: group.title)
//            if isOpened {
                if group.type == "radio" {
                    ForEach(group.options) { option in
                        CellOption(
                            isChosen: Binding<Bool>(
                                get: { selectedOptions.contains(option.id) },
                                set: {
                                    if $0 {
                                        selectedOptions.removeAll()
                                        selectedOptions.append(option.id)
                                    }
                                }), option: option)
                    }
                } else {
                    ForEach(group.options) { option in
                        CellMultipleOption(
                            isChosen: Binding<Bool>(
                                get: { selectedOptions.contains(option.id) },
                                set: { isSelected in
                                    if isSelected {
                                        selectedOptions.append(option.id)
                                        
                                    } else {
                                        if let indexToRemove = selectedOptions.firstIndex(of: option.id) {
                                            selectedOptions.remove(at: indexToRemove)
                                        }
                                    } }),
                            option: option)
                    }
                }
//            }
        }
    }
}

struct TitleOption: View {
//    @Binding var isOpened: Bool
    var optionTitle: String
    
    var body: some View {
        ZStack {
//            Rectangle()
//                .foregroundColor(Color.themeColor.opacity(0.5))
            HStack {
                Text(optionTitle)
                    .font(body1FontRubikMedium)
                    .padding()
                Spacer()
//                if isOpened {
//                    Image(systemName: "chevron.up")
//                        .font(.system(size: 20))
//                        .foregroundColor(.themeColor)
//                        .padding()
//                } else {
//                    Image(systemName: "chevron.down")
//                        .font(.system(size: 20))
//                        .foregroundColor(.themeColor)
//                        .padding()
//                }
                
            }
        }
        .frame(height: 55)
    }
}

struct CellOption: View {
    @Binding var isChosen: Bool
    let option: Dish.OptionGroup.Option
    
    var sizeOfButtonWithMark : CGFloat = 24
    
    var grayColor = Color.gray.opacity(0.5)
    
    var body: some View {
        ZStack {
            RoundedBubbleButtonWithShadow(content: AnyView(
                HStack {
                    ZStack {
                        Rectangle()
                            .foregroundColor(.clear)
                            .frame(width: sizeOfButtonWithMark + 5, height: sizeOfButtonWithMark)
                        if isChosen {
                            Image("checkmark.circle")
                                .resizable()
                                .foregroundColor(isChosen ? .mainBackgroundColor : grayColor)
                                .frame(width: sizeOfButtonWithMark, height: sizeOfButtonWithMark)
                        } else {
                            Circle()
                                .stroke(lineWidth: 2).foregroundColor(isChosen ? .themeColor : .themeColor)
                                .frame(width: sizeOfButtonWithMark - 3, height: sizeOfButtonWithMark)
                        }
                    }
                    .frame(width: sizeOfButtonWithMark + 5, height: sizeOfButtonWithMark)
                    Text(option.value)
                        .font(isChosen ? body1FontRubikMedium : body1FontRubikRegular)
                        .foregroundColor(isChosen ? .mainBackgroundColor : .mainAccentColor)
                    Spacer()
                    Text("+" + String(option.price) + "₽")
                        .font(body1FontRubikRegular)
                        .foregroundColor(isChosen ? .mainBackgroundColor : .mainAccentColor)
                }
            ), colorbackground: isChosen ? .themeColor : grayColor)
            .padding(.horizontal)
            
        }
        .frame(height: 55)
        .onTapGesture {
            isChosen.toggle()
        }
    }
}

struct CellMultipleOption: View {
    let height: CGFloat = 55
    @Binding var isChosen: Bool
    let option: Dish.OptionGroup.Option
    
    let sizeOfButtonWithMark : CGFloat = 24
    let buttonRadius : CGFloat = 6
    var grayColor = Color.gray.opacity(0.5)
    
    var body: some View {
        ZStack {
            RoundedBubbleButtonWithShadow(content: AnyView(
                HStack {
                    ZStack {
                        Rectangle()
                            .foregroundColor(.clear)
                            .frame(width: sizeOfButtonWithMark + 5, height: sizeOfButtonWithMark)
                        if isChosen {
                            Image("checkmark.roundedRect")
                                .resizable()
                                .foregroundColor(isChosen ? .mainBackgroundColor : grayColor)
                                .frame(width: sizeOfButtonWithMark, height: sizeOfButtonWithMark)
                        } else {
                            RoundedRectangle(cornerRadius: 3)
                                .stroke(lineWidth: 2).foregroundColor(isChosen ? .themeColor : .themeColor)
                                .frame(width: sizeOfButtonWithMark - 3, height: sizeOfButtonWithMark - 3)
                        }
                    }
                    .frame(width: sizeOfButtonWithMark + 5, height: sizeOfButtonWithMark)
                    Text(option.value)
                        .font(isChosen ? body1FontRubikMedium : body1FontRubikRegular)
                        .foregroundColor(isChosen ? .mainBackgroundColor : .mainAccentColor)
                    Spacer()
                    Text("+" + String(option.price) + "₽")
                        .font(body1FontRubikRegular)
                        .foregroundColor(isChosen ? .mainBackgroundColor : .mainAccentColor)
                }
            ), colorbackground: isChosen ? .themeColor : grayColor)
            .padding(.horizontal)
            
        }
        .frame(height: 55)
        .onTapGesture {
            isChosen.toggle()
        }
        .frame(height: height)
    }
}

struct OptionsPreviews: PreviewProvider {
    @State var options: [Dish.OptionGroup.Option] = [Dish.OptionGroup.Option(id: 1, value: "Средняя", price: 100, isDefault: 0, isPlural: 100), Dish.OptionGroup.Option(id: 0, value: "Большая", price: 100, isDefault: 0, isPlural: 0)]
    
    static var previews: some View {
        OptionGroupView(group: Dish.OptionGroup(
                            id: 0,
                            title: "Размер",
                            description: "",
                            required: 1,
                            type: "radio",
                            options: [
                                Dish.OptionGroup.Option(id: 0, value: "28 см", price: 0, isDefault: 1, isPlural: 0),
                                Dish.OptionGroup.Option(id: 1, value: "30 см", price: 50, isDefault: 0, isPlural: 0)
                            ]), selectedOptions: .constant([0]))
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
        OptionGroupView(group: Dish.OptionGroup(
                            id: 1,
                            title: "Добавки",
                            description: "",
                            required: 0,
                            type: "",
                            options: [
                                Dish.OptionGroup.Option(id: 2, value: "Грибы", price: 30, isDefault: 0, isPlural: 1),
                                Dish.OptionGroup.Option(id: 3, value: "Анчоусы", price: 50, isDefault: 0, isPlural: 1),
                                Dish.OptionGroup.Option(id: 4, value: "Креветки", price: 70, isDefault: 0, isPlural: 1)
                            ]), selectedOptions: .constant([3]))
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
    }
}
