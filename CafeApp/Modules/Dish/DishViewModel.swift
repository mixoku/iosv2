//
//  DishViewModel.swift
//  CafeApp
//
//  Created by Alexey Antonov on 24/01/21.
//

import Foundation

final class DishViewModel: ObservableObject {
    @Published private(set) var item: CartOrderItem
    
    
    init(dish: Dish) {
        item = CartOrderItem(dish: dish, count: 1, options: [])
        for group in dish.optionGroups {
            for option in group.options {
                if option.isDefault == 1 {
                    addOption(option: option)
                }
            }
        }
    }
    
    func changeCount(count: Int) {
        if count >= 0 {
            item.count = count
        }
    }
    
    func addOption(option: Dish.OptionGroup.Option) {
        item.options.append(option)
    }
    
    func updateOptions(options: [Dish.OptionGroup.Option], in group: Dish.OptionGroup) {
        item.options.removeAll(where: { group.options.contains($0)})
        item.options.append(contentsOf: options)
    }
}
