//
//  GeneralViewModel.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import Foundation
import SwiftUI
import Combine

final class GeneralViewModel: ViewModel {
    @Published private(set) var restaurant: Restaurant? {
        didSet {
            if let restaurant = restaurant {
                if !userRestaurants.contains(where: { $0 == restaurant.id }) {
                    userRestaurants.append(restaurant.id)
                    UserDefaults.standard.setValue(userRestaurants, forKey: "userRestaurants")
                }

                UserDefaults.standard.setValue(restaurant.id, forKey: "lastCafeId")
                
                Color.themeColor = restaurant.themeColor
            }
        }
    }
    
    @Published private(set) var userRestaurants: [Int] = [Int]()
    @Published var firstRun: Bool
    
    override init() {
        self.firstRun = Restaurant.lastId == nil
        
        super.init()
        
        if !firstRun {
            getRestaurant(for: Restaurant.lastId!)
        }
        
        self.userRestaurants = UserDefaults.standard.array(forKey: "userRestaurants") as? [Int] ?? [Int]()
    }
    
    init(id: Int) {
        self.firstRun = Restaurant.lastId == nil
        
        super.init()
        
        self.userRestaurants = UserDefaults.standard.array(forKey: "userRestaurants") as? [Int] ?? [Int]()
        
        getRestaurant(for: id)
    }
    
    private func getRestaurant(for id: Int, callback: ((Restaurant?)->())? = nil) {
        fetch(from: Endpoint.restaurant(id: id)) { [weak self] (item: Restaurant) in
            self?.restaurant = item
            callback?(item)
        }
    }
    
    private func addRestaurant(id: Int) {
        getUserRestaurants()
        if !userRestaurants.contains(id) {
            getHttpResponse(from: Endpoint.addRestaurant(id: id)) { (response: HTTPURLResponse?) in
                if let response = response, response.statusCode == 200 {
                    print("OK")
                }
            }
        }
    }
    
    private func getUserRestaurants() {
        fetch(from: Endpoint.getUserRestaurants) { (restaurants: [Restaurant]) in
            print(restaurants)
        }
    }
    
    func removeFromUserRestaurants(restaurantWithId: Int, callback: @escaping ()->()) {
        getHttpResponse(from: Endpoint.deleteRestaurant(id: restaurantWithId)) { (response: HTTPURLResponse?) in
            if let response = response, response.statusCode == 204 {
                callback()
            }
        }
    }
    
    func selectRestaurant(with id: Int, callback: @escaping (Restaurant?)->()) {
        getRestaurant(for: id, callback: callback)
    }
}
