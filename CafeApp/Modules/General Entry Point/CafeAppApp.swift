//
//  CafeAppApp.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import SwiftUI
import YandexMapsMobile

@main
struct CafeAppApp: App {
    @Environment(\.openURL) var openUrl
    @ObservedObject var generalViewModel = GeneralViewModel()
    @ObservedObject var orderStore = OrderStore()
    @ObservedObject var viewModel = MenuViewModel()
    
    init() {
        YMKMapKit.setApiKey("5e677775-c239-44f1-a0cd-8c7eb7640613")
    }
    
    var body: some Scene {
        WindowGroup {
            Group {
                if generalViewModel.error != nil {
                    ErrorView(error: generalViewModel.error!)
                } else {
                    if generalViewModel.restaurant != nil {
                        ZStack {
                            ContentView(restaurant: generalViewModel.restaurant!).environmentObject(orderStore).environmentObject(viewModel).environmentObject(generalViewModel)
                            MainGuideView(show: $generalViewModel.firstRun)
                        }
                    } else {
                        LoadingView()
                    }
                }
            }.onOpenURL(perform: { url in
                if let id = Int(url.pathComponents[1]) {
                    generalViewModel.selectRestaurant(with: id) { restaurant in
                        if let restaurant = restaurant {
                            orderStore.clearCart()
                            self.viewModel.changeRestaurant(for: restaurant.id)
                        } else {
                            self.viewModel.changeRestaurant(for: nil)
                        }
                    }
                }
            })
        }
    }
}
