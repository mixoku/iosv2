//
//  EnteringEmailView.swift
//  CafeApp
//
//  Created by Михаил on 03.02.2021.
//

import SwiftUI

struct EnteringEmailView: View {
    @Binding var emailAddress: String
    var title = "Укажите ваш e-mail адрес"
    var subtitle = "Ваш заказ оформлен, укажите ваш адрес электронной почты, чтобы мы могли прислать вам чек"
    var titleOfMessage = "Эл. почта"
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                VStack {
                    HStack {
                        VStack {
                            Text(title)
                                .font(header2FontRubikBold)
                                .foregroundColor(.mainAccentColor)
                        }
                        Spacer()
                    }
                    .padding(.bottom)
                    HStack {
                        VStack {
                            Text(subtitle)
                                .font(body1FontRubikRegular)
                                .foregroundColor(.gray)
                        }
                        Spacer()
                    }
                    .padding(.bottom)
                    Spacer()
                    FieldToEnter(titleIsText: true, title: "Ваш email", mainText: $emailAddress)
                    .frame(height: geometry.size.height/8)
                    .padding(.bottom)
                    Spacer()
                    Spacer()
                }
                .padding()
                VStack {
                    Spacer()
                    Button(action: {
                        
                    }) {
                        TabViewButtonPattern(title: "Далее")
                    }
                }
            }
            
        }
        
    }
}

struct EnteringEmailView_Previews: PreviewProvider {
    static var previews: some View {
        EnteringEmailView(emailAddress: .constant("mixokuz@gal.com"))
    }
}
