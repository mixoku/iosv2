//
//  LoginView.swift
//  MyCafe
//
//  Created by Alexey Antonov on 09/01/21.
//

import SwiftUI
import Combine

struct LoginView: View {
    @ObservedObject private var viewModel = LoginViewModel()
    @State private var phone = ""
    @State private var code = ""

    @State private var codeRequested = false
    
    var numberEnterFieldTitle = "Введите свой номер телефона"
    var SMSEnterFieldTitle = "Введите код из SMS"
    let callback: (String)->()
    
    var titles = ["Введите номер телефона", "Введите код из СМС"]
    var subtitles = ["Для оформления заказа нам необходимо знать номер вашего телефона", ""]
    var messageTitles = ["Введите свой номер телефона", "Введите код из SMS"]
    var buttonTitles = ["Получить код","Войти"]
    var isDisabled: Bool {
        return phone.count != 12 && (viewModel.expireTime == 0 || viewModel.expireTime == nil)
    }
    
    @State private var keyboardHeight: CGFloat = 0
        
    var body: some View {
        VStack {
            if viewModel.error == nil {
                GeometryReader { geometry in
                ZStack {
                    VStack {
                        HStack {
                            VStack(alignment: .leading) {
                                Text(codeRequested ? titles[1] : titles[0])
                                    .font(header2FontRubikBold)
                                    .foregroundColor(.mainAccentColor)
                            }
                            Spacer()
                        }
                        .padding(.bottom)
                        HStack {
                            VStack(alignment: .leading) {
                                Text(codeRequested ? subtitles[1] : subtitles[0])
                                    .font(body1FontRubikMedium)
                                    .foregroundColor(.gray)
                            }
                            Spacer()
                        }
                        Spacer()
                        if codeRequested {
                            HStack {
                                FieldToEnter(titleIsText: false, title: "Ваш номер телефона", mainText: $phone)
                                    .frame(height: geometry.size.height/8)
                                    .padding(.bottom)
                                Button(action: {
                                    viewModel.requestCode(for: phone)
                                    code = ""
                                })
                                {
                                    RefreshButton()
                                }
                                .padding(.bottom, 20)
                            }
                            
                        } else {
                            Color.mainBackgroundColor.opacity(0.00001)
                                .frame(height: geometry.size.height/8)
                                .padding(.bottom)
                        }
                        if !codeRequested {
                            FieldToEnter(titleIsText: false, title: codeRequested ? messageTitles[1] : messageTitles[0], mainText: codeRequested ? $code : $phone)
                                .padding(.bottom, 10)
                        }
                        
                        
                        if codeRequested {
                            CodeFieldView(maxDigits: 6, label: "Введите код из СМС", pin: "", showPin: true, isDisabled: false, isFilled: false) { otp, completionHandler in
                                
                                if otp.count == 6 && otp.firstIndex(of: "*") == nil {
                                    viewModel.login(phone: phone, code: otp) { newToken in
                                        callback(newToken)
                                    }
                                } else {
                                    print(otp)
                                }
                                // check if the otp is correct here
                                //            viewModel.restaurantCode = otp
                                //            if isCorrect(otp) {
                                // this could be a network call
                                //                completionHandler(true)
                    //                flag = true
                    //            }    else {
                    //                completionHandler(false)
                    //                flag = false
                    //            }
                            }
                            .frame(width: 300, height: 140)
                            .padding(.bottom, 20)
                        } else {
                            if keyboardHeight == 0 {
                                Color.mainBackgroundColor.opacity(0.00001)
                                    .frame(width: 300, height: 140)
                            }
                            
                        }
                        
                        Spacer()
                        Button(action: {
                            if codeRequested == false {
                                if phone.count == 12 && phone.firstIndex(of: "*") == nil
                                {
                                    viewModel.requestCode(for: phone)
                                    codeRequested = true
                                }
                            } else {
                                if code.count == 6 && code.firstIndex(of: "*") == nil {
                                    viewModel.login(phone: phone, code: code) { newToken in
                                        callback(newToken)
                                    }
                                }
                            }
                        }) {
                            OrderHistoryRoundedRectangleButton(backgroundColor:
                            codeRequested ? (code.count == 6 ? .themeColor : .gray) : (!isDisabled ? .themeColor : .gray), title: codeRequested ? buttonTitles[1] : buttonTitles[0], textColor: .mainBackgroundColor)
                                .disabled(codeRequested ? code.count != 6 : !isDisabled)
                        }
                        .padding(.bottom, keyboardHeight)
                                // 3.
                        .onReceive(Publishers.keyboardHeight) { self.keyboardHeight = $0 }
                    }
                    .padding()
                }
                }
            } else {
                ErrorView(error: viewModel.error!)
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView() { token in
            print(token)
        }
    }
}
