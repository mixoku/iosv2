//
//  LoginViewModel.swift
//  MyCafe
//
//  Created by Alexey Antonov on 09/01/21.
//

import Foundation
import Combine

final class LoginViewModel: ViewModel {
    private var time: Int? {
        didSet {
            if let time = time {
                if time > 0 {
                    expireTime = time
                    cancellable = Timer.publish(every: 1, on: RunLoop.main, in: RunLoop.Mode.default)
                        .autoconnect()
                        .sink(receiveValue: { [weak self] time in
                            self?.expireTime! -= 1
                        })
                } else {
                    cancellable?.cancel()
                }
            }
        }
    }
    
    @Published private(set) var expireTime: Int? {
        didSet {
            if expireTime == 0 {
                cancellable?.cancel()
            }
        }
    }
    
    func requestCode(for phone: String) {
        struct CodeResponse: Decodable {
            let expirationTime: Int?
            let phone: [String]?
        }
        fetch(from: Endpoint.requestCode(phone: phone)) { [weak self] (response: CodeResponse) in
            if let time = response.expirationTime {
                self?.time = time
            }
        }
    }
    
    func login(phone: String, code: String, callback: @escaping (String) -> ()) {
        struct LoginResponse: Decodable {
            let accessToken: String
        }
        
        fetch(from: Endpoint.login(phone: phone, code: code)) { (response: LoginResponse) in
            UserDefaults.standard.setValue(response.accessToken, forKey: "token")
            callback(response.accessToken)
        }
    }
    
    private var cancellable: Cancellable?
}
