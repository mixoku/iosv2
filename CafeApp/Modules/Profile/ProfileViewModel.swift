//
//  ProfileViewModel.swift
//  MyCafe
//
//  Created by Alexey Antonov on 09/01/21.
//

import Foundation
import Combine

final class ProfileViewModel: ViewModel {
    @Published var user: User = User(id: nil, username: "", phone: "", email: "", birthday: "", accessToken: nil, message: nil, name: nil)
    
    @Published var token: String {
        didSet {
            if !token.isEmpty {
                getUser()
            }
        }
    }
    
    override init() {
        let token = UserDefaults.standard.string(forKey: "token")
        if let token = token, !token.isEmpty {
            self.token = token
        } else {
            self.token = ""
        }
        
        super.init()
    }
    
    func updateToken() {
        token = UserDefaults.standard.string(forKey: "token") ?? ""
        
        if !token.isEmpty {
            getUser()
        }
    }
    
    func getUser() {
        fetch(from: Endpoint.getProfile) { [weak self] (profile: User?) in
            if profile != nil {
                self?.user = profile!
            }
        }
    }
    
    func updateUser() {
        fetch(from: Endpoint.updateProfile(user: self.user)) { [weak self] (profile: User?) in
            if profile != nil {
                self?.user = profile!
            }
        }

    }
    
    func logout() {
        getHttpResponse(from: Endpoint.logout) { [weak self] response in
            if let response = response, response.statusCode == 200 || response.statusCode == 401 {
                UserDefaults.standard.setValue("", forKey: "token")
                self?.updateToken()
            }
        }
    }
    
}
