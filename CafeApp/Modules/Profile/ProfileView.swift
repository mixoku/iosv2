//
//  ProfileView.swift
//  MyCafe
//
//  Created by Михаил on 25.12.2020.
//

import SwiftUI

struct ProfileView: View {
    @ObservedObject private var viewModel = ProfileViewModel()
    
    var phoneTitle: String = "Телефон"

    var emailTitle: String = "Почта"

    var nameTitle: String = "Имя"

    var birthdayTitle: String = "День рождения"
    
    @State var pushNotificationIsOn = true
    @State var SMSNotificationIsOn = true
    @State var emailNotificationIsOn = true
    
    @State var showAddressModal = false
    
    var body: some View {
        GeometryReader { geometry in
            if !viewModel.token.isEmpty {
//            if viewModel.token.isEmpty {
            ScrollView(.vertical) {
                VStack {
                    HStack {
                        VStack(alignment: .leading) {
                            Text("Мой профиль")
                                .font(header2FontRubikBold)
                                .foregroundColor(.mainAccentColor)
                        }
                        Spacer()
                    }
                    .padding(.bottom)
                    Group {
                        FieldToEnter(titleIsText: false, title: phoneTitle, mainText: $viewModel.user.phone.bound)
                            .disabled(true)
                            .padding(.bottom, 10)
                        FieldToEnter(titleIsText: false, title: emailTitle, mainText: $viewModel.user.email.bound)
                            .padding(.bottom, 10)
                        FieldToEnter(titleIsText: false, title: nameTitle, mainText: $viewModel.user.username.bound)
                            .padding(.bottom, 10)
                        FieldToEnter(titleIsText: false, title: birthdayTitle, mainText: $viewModel.user.birthday.bound)
                            .padding(.bottom, 20)
                    }
//                    .padding(.horizontal)
                    Group {
                        Button(action: {
                            showAddressModal.toggle()
                        }) {
                            AddressViewButton(height: geometry.size.height / 10)
                                .padding(.bottom, 20)
                        }
                        NavigationLink(
                            destination: NavigationDetailView(content: CardListView(), fullScreen: false),
                            label: {
                                AddressViewButton(systemImage: true, height: geometry.size.height / 10, imageName: "creditcard", text: "Привязанные карты")
                                    .padding(.bottom, 20)
                            })
                    }
//                    .padding(.horizontal)
                    Group {
                        HStack {
                            Text("Уведомления")
                                .font(header2FontRubikBold)
                                .foregroundColor(.mainAccentColor)
                            
                            Spacer()
                        }
                        HStack {
                            Text("Push-уведомления")
                                .font(body1FontRubikMedium)
                                .foregroundColor(.mainAccentColor)
                            Spacer()
                            Toggle("", isOn: $pushNotificationIsOn)
                                .toggleStyle(SwitchToggleStyle(tint: Color.themeColor))
                                .padding(.horizontal, 3)
                                
                        }
                        HStack {
                            Text("SMS")
                                .font(body1FontRubikMedium)
                                .foregroundColor(.mainAccentColor)
                            Spacer()
                            Toggle("", isOn: $SMSNotificationIsOn)
                                .toggleStyle(SwitchToggleStyle(tint: Color.themeColor))
                                .padding(.horizontal, 3)
                        }
                        HStack {
                            Text("Эл. почта")
                                .font(body1FontRubikMedium)
                                .foregroundColor(.mainAccentColor)
                                
                            Spacer()
                            Toggle("", isOn: $emailNotificationIsOn)
                                .toggleStyle(SwitchToggleStyle(tint: Color.themeColor))
                                .padding(.horizontal, 3)
                                
                        }
                    }
//                    .padding(.horizontal)
                }
                
//                Group {
                    OrderHistoryRoundedRectangleButton(backgroundColor: Color.themeColor, title: "Сохранить", textColor: Color.white)
                        .onTapGesture {
                            viewModel.updateUser()
                        }
                        .padding(.top)
                    OrderHistoryRoundedRectangleButton(backgroundColor: Color.themeColor.opacity(0.2), title: "Выйти из профиля", textColor: .themeColor)
                        .onTapGesture {
                            viewModel.logout()
                        }
//                }
//                .padding()
                
                
            }
            
            .padding()
                
            } else {
                LoginView() { viewModel.token = $0 }
            }
        }
        .ignoresSafeArea(edges: .bottom)
        .onAppear { if !viewModel.token.isEmpty { viewModel.getUser() } }
        .sheet(isPresented: $showAddressModal) {
            AddressListModalView(show: $showAddressModal)
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(phoneTitle: "Номер телефона", emailTitle: "Email", nameTitle: "Имя", birthdayTitle: "Дата рождения", pushNotificationIsOn: true, SMSNotificationIsOn: true, emailNotificationIsOn: true, showAddressModal: false)
    }
}
