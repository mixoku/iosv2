//
//  NavigationHeader.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import SwiftUI

struct MenuHeader: View {
    @Binding var showNavigation: Bool
    @Binding var searchText: String
    @Binding var currentSection: String
    @State private var searchEnabled = false
    @Binding var scrollToTop: Bool
    
    var body: some View {
        HStack {
            if !searchEnabled {
                Button(action: {
                    showNavigation = true
                }) {
                    Image("person.circle")
                        .foregroundColor(.themeColor)
                        .font(.title2)
                        .padding(10)
                        .background(Circle().foregroundColor(.mainBackgroundColor))
                }
                .shadow(color: .clear, radius: 5, y: 20)
                Spacer()
                Text("\(currentSection)")
                    .font(body1FontRubikMedium)
                    .shadow(color: .clear, radius: 5, y: 20)
                    .foregroundColor(.themeColor)
                    .onTapGesture {
                        scrollToTop = true
                    }
                Spacer()
                Button(action: {
                    searchEnabled = true
                }) {
                    Image("magnifyingglass")
                        .foregroundColor(.themeColor)
                        .font(.title2)
                        .padding(10)
                        .background(Circle().foregroundColor(.mainBackgroundColor))
                }
                .shadow(color: .clear, radius: 5, y: 20)
            } else {
                Group {
                    TextField("Поиск", text: $searchText)
                    Button(action: {
                        searchText = ""
                        searchEnabled = false
                    }, label: {
                        Image("xmark")
                            .accentColor(.themeColor)
                            .font(.title)
                    })
                }.padding(10)
            }
        }.padding()
        .shadow(radius: 10)
    }
}
