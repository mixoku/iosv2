//
//  MenuViewModel.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import Foundation
import Combine
import SwiftUI

final class MenuViewModel: ViewModel {
    private(set) var restaurantId: Int? {
        didSet {
            if let id = restaurantId {
                getMenu(for: id)
            } else {
                error = .noRestaurant
            }
        }
    }
    
    @Published private(set) var isLoading: Bool = true
    
    @Published private(set) var sections = [MenuSection]() {
        didSet {
            sectionBounds.removeAll()
            if sections.count == 0 {
                isLoading = true
            } else {
                isLoading = false
                
                var lastBound = 215
                for section in sections {
                    let upperBound = lastBound + 40 + section.dishes.count * 128
                    sectionBounds.append(CGFloat(lastBound)...CGFloat(upperBound))
                    lastBound = upperBound
                }
            }
            
            currentSectionIndex = sections.first?.id ?? 0
        }
    }
    
    private var sectionBounds = [ClosedRange<CGFloat>]()
    
    @Published private(set) var promos = promosSample//[Promo]()
    
    var currentSection: String = "" {
        willSet {
            self.objectWillChange.send()
        }
    }
    @Published var currentSectionIndex: Int = 0
    
    @Published var offset: CGFloat = 0 {
        didSet {
            if offset < 215 {
                currentSection = ""
                currentSectionIndex = sections.first?.id ?? 0
            } else {
                for boundsIndex in 0..<sectionBounds.count {
                    if sectionBounds[boundsIndex].contains(offset) {
                        currentSection = sections[boundsIndex].title
                        currentSectionIndex = sections[boundsIndex].id
                    }
                }
            }
        }
    }
    
    init(restaurantId: Int? = nil) {
        print(UserDefaults.standard.string(forKey: "token"))
        print(UserDefaults.standard.integer(forKey: "lastCafeId"))
        
        if let id = restaurantId {
            self.restaurantId = id
        } else {
            self.restaurantId = Restaurant.lastId
        }
        
        super.init()
        
        if let id = self.restaurantId {
            getMenu(for: id)
        }
    }
    
    func changeRestaurant(for id: Int?) {
        self.restaurantId = id
    }
    
    private func getMenu(for id: Int) {
        fetch(from: Endpoint.restaurantMenu(restaurantId: id)) { [weak self] (items: [MenuSection]) in
            self?.sections = items
        }
    }
}
