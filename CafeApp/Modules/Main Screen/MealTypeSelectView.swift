//
//  MealTypeSelectView.swift
//  MyCafe
//
//  Created by Alexey Antonov on 06/12/20.
//

import SwiftUI

struct MealTypeSelectView: View {
    let mealTypes: [MenuSection]
    @Binding var selectedSectionIndex: Int
    let callback: (Int) -> ()
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach (mealTypes) { section in
                    Text(section.title)
                        .font(header1FontRubik)
                        .foregroundColor(section.id == selectedSectionIndex ? .mainAccentColor : Color.gray.opacity(0.2))
                        .if(section.id == selectedSectionIndex) { $0.bold() }
                        .onTapGesture {
                            selectedSectionIndex = section.id
                            callback(section.id)
                        }
                        .padding(.trailing, 32)
                }
            }
            .padding()
        }
    }
}
