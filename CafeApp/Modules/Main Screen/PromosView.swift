//
//  PromosView.swift
//  MyCafe
//
//  Created by Alexey Antonov on 06/12/20.
//

import SwiftUI


struct PromosView: View {
    var promos: [Promo]
    @Binding var modalType: ModalType?
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(promos) { promo in
                    SmallMenuBanner(promo: promo, isBigCard: false, height: 136)
                        .onTapGesture {
                            modalType = .promo(promo: promo)
                        }
                }
            }
            .padding(.top, 5)
            .padding(.leading, 10)
        }
    }
}
