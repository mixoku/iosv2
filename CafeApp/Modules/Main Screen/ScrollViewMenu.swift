//
//  ScrollViewMenu.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import SwiftUI

struct ScrollViewMenu: View {
    let items: [MenuSection]
    let promos: [Promo]
    @Binding var modalType: ModalType?
    @Binding var offset: CGFloat
    @Binding var selectedCategory: Int
    @State private var showPromo: Promo?
    @Binding var scrollToTop: Bool
    
    var body: some View {
        VStack(alignment: .leading) {
            ScrollViewReader { scrollView in
            TrackableScrollView(contentOffset: $offset) {
                VStack(alignment: .leading) {
                    if promos.count > 0 {
                        PromosView(promos: promos, modalType: $modalType)
                    }
                    MealTypeSelectView(mealTypes: items, selectedSectionIndex: $selectedCategory) { id in
                        withAnimation {
                            scrollView.scrollTo(id, anchor: .top)
                        }
                    }.id(-1)
                    ForEach(items) { section in
                        Group {
                            if !section.title.isEmpty {
                                Text(section.title)
                                    .font(header1FontRubik)
                                    .foregroundColor(.gray)
                                    .id(section.id)
                            }
                            ForEach(section.dishes) { dish in
                                CellOfDish(dish: dish)
                                    .padding(0)
                                    .onTapGesture {
                                        modalType = .detail(dish: dish)
                                    }
                            }
                        }
                    }.padding(.horizontal)
                }.onChange(of: scrollToTop, perform: { value in
                    withAnimation {
                        scrollView.scrollTo(-1, anchor: .center)
                    }
                })
            }
            .onAppear {
                selectedCategory = items.first?.id ?? 0
            }
        }
        }
    }
}

struct ScrollViewMenu_Previews: PreviewProvider {
    static var previews: some View {
        ScrollViewMenu(items: [MenuSection(id: 0, title: "Еда", description: nil, dishes: [Dish(id: 0, title: "Мясо", weight: 0, descriptionShort: "", descriptionFull: "", price: 0, measure: "", images: [], optionGroups: [])]), MenuSection(id: 1, title: "Напитки", description: nil, dishes: [])], promos: promosSample, modalType: .constant(nil), offset: .constant(0), selectedCategory: .constant(0), scrollToTop: .constant(false))
    }
}
