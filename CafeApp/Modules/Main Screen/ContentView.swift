//
//  ContentView.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import SwiftUI

struct ContentView: View {
    let restaurant: Restaurant
    
    @EnvironmentObject var generalViewModel: GeneralViewModel
    @EnvironmentObject var viewModel: MenuViewModel
    @State var modalType: ModalType?
    @State private var searchText = ""
    @State var scrollToTop = false
    
    @EnvironmentObject var orderStore: OrderStore
    
    init(restaurant: Restaurant) {
        self.restaurant = restaurant
        
        UINavigationBar.appearance().barTintColor = UIColor(.mainBackgroundColor)
    }
    
    var body: some View {
        if let error = viewModel.error {
            ErrorView(error: error)
        } else {
            NavigationView {
                VStack(alignment: .leading) {
                    if viewModel.isLoading {
                        LoadingView()
                    } else {
                        MenuHeader(showNavigation: Binding<Bool>(get: { modalType == .menu }, set: { modalType = $0 ? .menu : nil }), searchText: $searchText, currentSection: $viewModel.currentSection, scrollToTop: $scrollToTop)
                        ScrollViewMenu(items: searchText == "" ? viewModel.sections : [MenuSection(id: -1, title: "", description: nil, dishes: viewModel.sections.map(\.dishes).reduce([Dish](), +).filter { $0.title.lowercased().contains(searchText.lowercased())})], promos: viewModel.promos, modalType: $modalType, offset: $viewModel.offset, selectedCategory: $viewModel.currentSectionIndex, scrollToTop: $scrollToTop)
                        if orderStore.dishes.count > 0 {
                            CartTabView(showFullCart: Binding<Bool>(get: { modalType == .cart }, set: { modalType = $0 ? .cart : nil }))
                        }
                    }
                }.navigationBarHidden(true)
            }.sheet(item: $modalType) {
                if let type = $0 {
                    switch type {
                    case .menu:
                        NavigationMenuView(showNavigation: Binding<Bool>(get: { modalType == .menu }, set: { modalType = $0 ? .menu : nil }), restaurant: restaurant).environmentObject(generalViewModel).environmentObject(viewModel)
                            .environmentObject(orderStore)
                    case .detail(let dish):
                        ModalCardOfDish(dish: dish, show: Binding<Bool>(get: { modalType == .detail(dish: dish) }, set: { modalType = $0 ? .detail(dish: dish) : nil })).environmentObject(orderStore)
                    case .promo(let promo):
                        PromoDetailView(promo: promo, showModal: Binding<Bool>(get: { modalType == .promo(promo: promo) }, set: { modalType = $0 ? .promo(promo: promo) : nil }))
                    case .cart:
                        OrderModalView(showModal: Binding<Bool>(get: { modalType == .cart }, set: { modalType = $0 ? .cart : nil })).environmentObject(orderStore)
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(restaurant: theRestaurant)
    }
}
