//
//  MainGuideView.swift
//  CafeApp
//
//  Created by Alexey Antonov on 26/01/21.
//

import SwiftUI

struct MainGuideView: View {
    @ObservedObject var viewModel = GuidesViewModel()
    @State private var opacity: Double = 0.0
    @Binding var show: Bool
    
    var body: some View {
        if !show {
        ZStack(alignment: .topLeading) {
            if viewModel.currentStep == .menu {
            Rectangle()
                .fill(Color.mainBackgroundColor.opacity(0.8))
                .clipShape(ShapeWithHole())    // clips or masks the view
                .contentShape(ShapeWithHole()) // needed for hit-testing
                .edgesIgnoringSafeArea(.bottom)
            Circle()
                .stroke(Color.themeColor, lineWidth: 3)
                .frame(width: 45, height: 45, alignment: .center)
                .offset(x: 16, y: 16)
            } else {
                Rectangle()
                    .fill(Color.mainBackgroundColor.opacity(0.8))
                    .edgesIgnoringSafeArea(.all)
            }
            GuideMessage(offset: viewModel.currentStep.figureOffset, text: viewModel.currentStep.description, step: Binding<GuideStep>(get: { viewModel.currentStep }, set: { _ in viewModel.nextStep() }))
        }
        .opacity(opacity)
        .onAppear {
            withAnimation {
                opacity = 1
            }
        }.onTapGesture {
            viewModel.nextStep()
            if viewModel.currentStep == .end {
                show = true
            }
        }
    }
    }
}

struct MainGuideView_Previews: PreviewProvider {
    static var previews: some View {
        MainGuideView(show: .constant(true))
    }
}

extension Path {
    var reversed: Path {
        let reversedCGPath = UIBezierPath(cgPath: cgPath)
            .reversing()
            .cgPath
        return Path(reversedCGPath)
    }
}

struct ShapeWithHole: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Rectangle().path(in: rect)
        let hole = Circle().path(in: CGRect(x: 16, y: 16, width: 45, height: 45)).reversed
        path.addPath(hole)
        return path
    }
}

struct GuideMessage: View {
    var offset: CGFloat
    var text: String
    @Binding var step: GuideStep
//    let rect = CGRect(x: 0, y: 0, width: , height: 100)
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                VStack {
                    
                    Spacer(minLength: 200 - offset)
                        
                    ZStack{
                        RoundedRectangle(cornerRadius: 25)
                            .foregroundColor(.themeColor)
                        Text(text)
                            .foregroundColor(.mainBackgroundColor)
                            .padding()
                    }
                    .frame(height: 150)
                    .padding()
                    .offset(y: -offset)
                    Spacer(minLength: 200 + offset)
                }
            }
        }
    }
}

//for complete remove from the screen

extension View {
    @ViewBuilder func isHidden(_ hidden: Bool, remove: Bool = false) -> some View {
        if hidden {
            if !remove {
                self.hidden()
            }
        } else {
            self
        }
    }
}

