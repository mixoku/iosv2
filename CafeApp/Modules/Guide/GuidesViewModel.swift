//
//  GuidesViewModel.swift
//  CafeApp
//
//  Created by Alexey Antonov on 26/01/21.
//

import Foundation
import SwiftUI

enum GuideStep {
    case start
    case menu
    //case search
    case end
    
    var description: String {
        switch self {
        case .start:
            return """
                Давайте быстро пройдёмся по основным функциям приложения.
                Для продолжения нажмите на любую область экрана.
                """
        case .menu:
            return """
                Вы находитесь в меню ресторана, где вы можете заказывать блюда.
                Чтобы перейти к настройкам приложения нажмите на эту иконку.
                """
//        case .search:
//            return "Для поиска нажмите на эту иконку"
        default:
            return "Вы готовы"
        }
    }
    
    var figureOffset: CGFloat {
        switch self {
        case .start:
            return 0
        default:
            return 100
        }
    }
    
    mutating func next() {
        switch self {
        case .start:
            self = .menu
        case .menu:
            self = .end
        default:
            break
        }
    }
}

final class GuidesViewModel: ObservableObject {
    @Published private(set) var currentStep: GuideStep = .start
    
    func nextStep() {
        currentStep.next()
    }
}
