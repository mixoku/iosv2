//
//  DeliverytabView.swift
//  CafeApp
//
//  Created by Михаил on 03.02.2021.
//

import SwiftUI

struct DeliveryTabView: View {
    var orderCost: Int
    @Binding var isActive: Bool
    
    var width = UIScreen.main.bounds.width - 40
    let callback: ()->()
    
    var body: some View {
        ZStack {
            Rectangle()
//                .foregroundColor(.white)
                .frame(width: width + 40, height: 90)
            VStack {
                Divider()
                HStack {
                    Button(action: {
                        callback()
                    }) {
                        OrderRoundedRectangleButton()
                            .padding()
                    }.disabled(!isActive)
                    .opacity(isActive ? 1 : 0.2)
                }
                Spacer()
            }
        }
        .frame(width: width + 40, height: 70)
        .foregroundColor(Color.mainBackgroundColor)
        .ignoresSafeArea(edges: .bottom)
    }
}

struct OrderCheckout: View {
    var isDelivery: Bool = true
    var orderPrices: OrderPrices
    var body: some View {
        VStack(spacing: 25) {
            HStack {
                Text("Блюда")
                    .font(body1FontRubikRegular)
                    .foregroundColor(.mainAccentColor)
                Spacer()
                Text(String(orderPrices.meal) + "₽")
                    .font(body1FontRubikRegular)
                    .foregroundColor(.mainAccentColor)
            }
            if isDelivery {
                HStack {
                    Text("Доставка")
                        .font(body1FontRubikRegular)
                        .foregroundColor(.mainAccentColor)
                    Spacer()
                    Text(String(orderPrices.delivery) + "₽")
                        .font(body1FontRubikRegular)
                        .foregroundColor(.mainAccentColor)
                }
            }
        }
        .padding(.bottom)
        HStack {
            Text("Итого")
                .font(header2FontRubikBold)
                .foregroundColor(.themeColor)
            Spacer()
            Text(String(orderPrices.sum) + "₽")
                .font(header2FontRubikBold)
                .foregroundColor(.themeColor)
        }
    }
}

struct DeliveryTabView_Previews: PreviewProvider {
    static var previews: some View {
        DeliveryTabView(orderCost: 1000, isActive: .constant(true), callback: { })
    }
}
