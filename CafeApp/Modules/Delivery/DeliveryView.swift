//
//  DeliveryView.swift
//  MyCafe
//
//  Created by Михаил on 20.12.2020.
//

import SwiftUI

struct OrderPrices {
    let meal: Int
    let delivery: Int
    
    var sum: Int {
        meal + delivery
    }
}

struct DeliveryView: View {
    let items: [CartOrderItem]
    let dishes: [Order.OrderDish]
    let orderSum: Int
    @ObservedObject var viewModel: CheckoutViewModel
    @State var paymentSuccessful: Bool = false
    @Binding var showModal: Bool
    @EnvironmentObject var orderStore: OrderStore
    
    init(order: [Order.OrderDish], orderSum: Int, items: [CartOrderItem], showModal: Binding<Bool>) {
        self.dishes = order
        self.orderSum = orderSum
        self.items = items
        _showModal = showModal
        viewModel = CheckoutViewModel(dishes: order)
    }
    
    @State var city = "Москва"
    @State var addressText = "г. Москва, улица Ленина, 95, кв 11"
    @State var addressComment = "Net monet. Voz'mi cartu"
    var addressTitle = "Адрес доставки"
    
    @State var timeOfDelivery = "Сегодня в 11:00"
    var timeTitle = "Время доставки"
    @State var chosenTime = 20
    
    @State var commentText = "Комментарий к заказу"
    
    @State var orderPrices: OrderPrices?
    
    @State var promoName = "Промокод"
    
    @State var paymentOption = "Apple Pay"
    @State var paymentText = "Способ оплаты"
    
    var modals = ["None", "Time","Address", "Payment"]
    @State var typeOfModal = 0
    @State var showDeliveryModal = false
    
    
    @State var finished = false
    @State var order: Order?
    
    @State var chosenTypeOfPayment: PaymentCard?
    
    var body: some View {
        if !viewModel.token.isEmpty {
//        if viewModel.token.isEmpty {
            VStack {
                if !finished {
                    ZStack {
                    VStack {
                        ScrollView(.vertical) {
                            VStack {
                                Group {
                                    HStack {
                                        Text("Доставка")
                                            .font(header2FontRubikBold)
                                            .multilineTextAlignment(.leading)
                                            .foregroundColor(.mainAccentColor)
                                        Spacer()
                                    }
                                    .padding(.bottom)
                                    HStack {
                                        Text("Адрес доставки")
                                            .font(body1FontRubikRegular)
                                            .multilineTextAlignment(.leading)
                                            .foregroundColor(Color.gray)
                                        Spacer()
                                    }
                                    Button(action: {
                                        self.typeOfModal = 1
                                        self.showDeliveryModal.toggle()
                                    }) {
                                        if viewModel.addresses.count > 0 {
                                            AddressCard(address: viewModel.addresses.first!, city: viewModel.city)
                                        } else {
                                            RoundedBubbleButtonWithShadow(content: AnyView(
                                                HStack {
                                                    Text("Пожалуйста, добавьте адрес")
                                                    .font(body1FontRubikMedium)
                                                    Spacer()
                                                }
                                            ))
                                        }
                                    }
                                    .buttonStyle(PlainButtonStyle())
                                    
                                    HStack {
                                        Text("Время доставки")
                                            .font(body1FontRubikRegular)
                                            .multilineTextAlignment(.leading)
                                            .foregroundColor(Color.gray)
                                        Spacer()
                                    }
                                    Button(action: {
                                        //self.typeOfModal = 2
                                        //self.showDeliveryModal.toggle()
                                    }) {
                                        TimeEnterField(error: viewModel.error, timeTillDelivery: chosenTime)
                                            .buttonStyle(PlainButtonStyle())
                                            .padding([.bottom])
                                    }
                                    .buttonStyle(PlainButtonStyle())

                                    
                                }
                                .padding(.horizontal)
                                HStack {
                                    Text(paymentText)
                                        .font(body1FontRubikRegular)
                                        .multilineTextAlignment(.leading)
                                        .foregroundColor(Color.gray)
                                    Spacer()
                                }.padding(.horizontal)
                                Button(action: {
                                    self.typeOfModal = 3
                                    self.showDeliveryModal.toggle()
                                }) {
                                    RoundedBubbleButtonWithShadow(content:
                                        HStack {
                                            Text(chosenTypeOfPayment?.pan ?? "Пожалуйста, добавьте метод оплаты")
                                                .font(body1FontRubikRegular)
                                                .foregroundColor(Color.mainAccentColor)
                                            Spacer()
                                        }
                                        .eraseToAnyView()
                                    )
                                }
//                                NavigationLink(
//                                    destination: NavigationDetailView(content: CardListView(), fullScreen: false),
//                                    label: {
//
//                                    }
//                                )
//                                    .buttonStyle(PlainButtonStyle())
                                .padding(.horizontal)
                                CommentField(mainText: $viewModel.comment, placeholder: "Комментарий")
                                    .padding(.horizontal)
                                OrderCheckout(orderPrices: orderPrices ?? OrderPrices(meal: orderSum, delivery: 0))
                                    .padding(.horizontal)
                                PromocodeField(promocode: $promoName)
                                    .padding()
                                Spacer()
                            }
                        }
                    }
                    //.frame(height: geometry.size.height)
                    VStack {
                        Spacer()
                        DeliveryTabView(orderCost: orderPrices?.sum ?? orderSum, isActive: Binding<Bool>(get: { viewModel.addresses.count > 0 && viewModel.error == nil  }, set: { print($0) })) {
                            viewModel.sendOrder() { order in
                                self.order = order
                                orderStore.clearCart()
                                finished = true
                            }
                        }
                        
                        .shadow(color: .clear, radius: 0)
                    }
                    .ignoresSafeArea(edges: .bottom)
                    .shadow(color: Color.gray.opacity(0.2), radius: 10, y: -10)
                }
                //.frame(width: geometry.size.width, alignment: .center)
            //}
            .navigationBarHidden(true)
            .sheet(isPresented: $showDeliveryModal) {
                switch typeOfModal {
                case 1:
                    DeliveryAddressModal()
                case 2:
                    EnterTimeModal(chosenTime: $chosenTime)
                case 3:
                    CardListView()
                    //ChoosePaymentTypeModal(chosenTypeOfPayment: $chosenTypeOfPayment, showModal: $showModal)
                
                default:
                    DeliveryAddressModal()
                }
                //DeliveryAddressModal()
            }
        } else {
            OrderStatusScreen(order: order!, showModal: $showModal)
        }
            }.onAppear {
                orderPrices = OrderPrices(meal: orderSum, delivery: viewModel.zone?.price ?? 0)
            }
        } else {
            LoginView() { viewModel.token = $0 }
        }
    }
}



struct DeliveryView_Previews: PreviewProvider {
    static var previews: some View {
        DeliveryView(order: [], orderSum: 100, items: [], showModal: .constant(true))
    }
}
