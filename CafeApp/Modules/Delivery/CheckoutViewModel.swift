//
//  CheckoutViewModel.swift
//  CafeApp
//
//  Created by Alexey Antonov on 25/01/21.
//

import Foundation

final class CheckoutViewModel: ViewModel {
    @Published var token: String = ""
    let dishes: [Order.OrderDish]
    var order: Order?
    let restaurantId = Restaurant.lastId
    
    let city = "Москва"
    @Published private(set) var addresses = [DeliveryAddress]() {
        didSet {
            if addresses.count > 0 {
                if let defaultAddress = addresses.first(where: { $0.isDefault == 1 }) {
                    getServiceZone(for: defaultAddress)
                } else {
                    if let firstAddress = addresses.first {
                        getServiceZone(for: firstAddress)
                    }
                }
            }
        }
    }
    @Published var comment: String = ""
    @Published private(set) var isSending = false
    @Published private(set) var zone: ServiceZone? {
        didSet {
            if addresses.count > 0 && zone == nil {
                error = .outOfZone
            } else {
                error = nil
            }
        }
    }
    
    @Published private(set) var continueOrder = false
    
    init(dishes: [Order.OrderDish]) {
        token = UserDefaults.standard.string(forKey: "token") ?? ""
        self.dishes = dishes
        
        super.init()
        
        if !token.isEmpty {
            getAddresses()
        }
    }
    
    func getAddresses() {
        if let token = UserDefaults.standard.string(forKey: "token"), !token.isEmpty {
            fetch(from: Endpoint.getAddresses) { [weak self] (addresses: [DeliveryAddress]) in
                self?.addresses = addresses
            }
        }
    }
    
    func getServiceZone(for address: DeliveryAddress) {
        if let lat = address.lat, let lng = address.lng, let id = restaurantId {
            
            fetch(from: Endpoint.addressInZone(id: id, lat: lat, lng: lng)) { [weak self] (zone: ServiceZone?) in
                if let zone = zone {
                    self?.zone = zone
                } else {
                    self?.error = .outOfZone
                }
            }
            
//            fetch(from: Endpoint.getServiceZone(restaurantId: id, data: coords)) { [weak self] (zone: ServiceZone?) in
//                if let zone = zone {
//                    self?.zone = zone
//                } else {
//                    self?.error = .outOfZone
//                }
//            }
        }
    }
    
//    func paymentFinished(url: URL) {
//        if url.lastPathComponent == "success" {
//            Endpoint.paymentSuccess(url: url)
//                .getHttpResponse()
//                .sink { (completion) in
//                    switch completion {
//                    case .failure(let error):
//                        print(error.localizedDescription)
//                    case .finished:
//                        break
//                    }
//                } receiveValue: { [weak self] (response) in
//                    if let statusCode = response?.statusCode, 200...299 ~= statusCode {
//                        self?.payNext { [weak self] in
//                            self?.continueOrder = true
//                        }
//                    }
//                }
//                .store(in: &cancellables)
//        }
//    }
    
    private func payNext() {
        if let order = order, let id = order.id, let cafeId = restaurantId {
            getHttpResponse(from: Endpoint.orderPayment(restaurantId: cafeId, orderId: id)) { [weak self] (response) in
                if let statusCode = response?.statusCode, 200...299 ~= statusCode {
                    self?.continueOrder = true
                    print("Payment successful")
                } else {
                    print(response?.statusCode)
                    self?.error = .PaymentFailure
                }
            }
        }
    }
    
    func sendOrder(order: Order? = nil, callback: @escaping (Order)->()) {
        let myOrder = order == nil ? Order(id: nil, comment: comment, status: nil, dishes: dishes, deliveryAddressId: addresses.first!.id!, sum: nil, deliveryInfo: nil, createdAt: nil, updatedAt: nil, deliveryAddress: nil) : order!
        
        guard let restaurantId = restaurantId else { return }
        
        fetch(from: Endpoint.sendOrder(restaurantId: restaurantId, order: myOrder)) { [weak self] (newOrder: Order) in
            self?.order = newOrder
            self?.isSending = false
            self?.payNext()
            callback(newOrder)
        } failure: { [weak self] (error) in
            if let _ = error as? DecodingError {
                self?.error = .badCoding
            }
        }

    }
}
