//
//  PickUpView.swift
//  CafeApp
//
//  Created by Михаил on 31.01.2021.
//

import SwiftUI

struct PickUpView: View {
    @ObservedObject var viewModel: CheckoutViewModel
    
    let items: [CartOrderItem]
    let dishes: [Order.OrderDish]
    let orderSum: Int
    @Binding var showModal: Bool
    
    @State var orderPrices: OrderPrices?
    
    @State var promoName = "Промокод"
    @State var paymentText = "Способ оплаты"
    
    @State var finished = false
    @State var order: Order?
    
    @State var nameOfRestaurant: String = "J&J на Новом"
    @State var address: String = "Новый Арбат, 23к2"
    
    var modals = ["None", "Address", "Payment"]
    @State var typeOfModal = 0
    @State var showDeliveryModal = false
    @State var chosenTypeOfPayment: PaymentCard?
    
    
    init(order: [Order.OrderDish], orderSum: Int, items: [CartOrderItem], showModal: Binding<Bool>) {
        self.dishes = order
        self.orderSum = orderSum
        self.items = items
        _showModal = showModal
        viewModel = CheckoutViewModel(dishes: order)
    }
    
    var body: some View {
        ZStack {
            VStack {
                ScrollView(.vertical) {
                    VStack {
                        Group {
                            HStack {
                                Text("Самовывоз")
                                .font(header2FontRubikBold)
                                .multilineTextAlignment(.leading)
                                .foregroundColor(.mainAccentColor)
                            Spacer()
                        }
                        .padding(.bottom)
                        HStack {
                            Text("Выберите, откуда хотите забрать заказ")
                                .font(body1FontRubikRegular)
                                .multilineTextAlignment(.leading)
                                .foregroundColor(Color.gray)
                            Spacer()
                        }
                        NavigationLink(
                            destination: NavigationDetailView(content: Text("Здесь будут адреса ресторанов"), fullScreen: false),
                            label: {
                                RoundedBubbleButtonWithShadow(content:
                                    VStack {
                                        RoundedRectangle(cornerRadius: 16)
                                            .foregroundColor(.gray)
                                            .frame(height: 220)
                                            .padding(.bottom)
                                        HStack {
                                            Text(address)
                                                .font(body1FontRubikMedium)
                                                .foregroundColor(.mainAccentColor)
                                            Spacer()
                                        }
                                        .padding(.bottom, 10)
                                        HStack {
                                            Text(nameOfRestaurant)
                                                .font(body2FontRubik)
                                                .foregroundColor(.mainAccentColor)
                                            Spacer()
                                        }
                                    }
                                    .eraseToAnyView()
                                )
                            }
                        )
                        .padding([.bottom])
//                        HStack {
//                            Text("Выберите, откуда хотите забрать заказ")
//                                .font(body1FontRubikMedium)
//                                .multilineTextAlignment(.leading)
//                                .foregroundColor(Color.gray)
//                            Spacer()
//                        }
//                        RoundedRectangle(cornerRadius: 16)
//                            .foregroundColor(.gray)
//                            .frame(height: 220)
//                            .padding(.bottom)
                    }
                    .padding(.horizontal)
                        HStack {
                            Text(paymentText)
                                .font(body1FontRubikRegular)
                                .multilineTextAlignment(.leading)
                                .foregroundColor(Color.gray)
                            Spacer()
                        }.padding(.horizontal)
                        Button(action: {
                            self.typeOfModal = 3
                            self.showDeliveryModal.toggle()
                        }) {
                            RoundedBubbleButtonWithShadow(content:
                                HStack {
                                    Text(chosenTypeOfPayment?.pan ?? "Выберите карту для оплаты")
                                        .font(body1FontRubikRegular)
                                        .foregroundColor(Color.mainAccentColor)
                                    Spacer()
                                }
                                .eraseToAnyView()
                            )
                        }
                        .padding(.horizontal)
//                    HStack {
//                        Text(paymentText)
//                            .font(body1FontRubikRegular)
//                            .multilineTextAlignment(.leading)
//                            .foregroundColor(Color.gray)
//                        Spacer()
//                    }
//                    .padding(.horizontal)
//                    NavigationLink(
//                        destination: NavigationDetailView(content: CardListView(), fullScreen: false),
//                        label: {
//                            RoundedBubbleButtonWithShadow(content:
//                                HStack {
//                                    Text(paymentOption)
//                                        .font(body1FontRubikRegular)
//                                        .foregroundColor(.mainAccentColor)
//                                    Spacer()
//                                }
//                                .eraseToAnyView()
//                            )
//                        }
//                    )
//                    .padding([.horizontal, .bottom])
                    
                    
        
                    OrderCheckout(isDelivery: false, orderPrices: orderPrices ?? OrderPrices(meal: orderSum, delivery: 0))
                        .padding(.horizontal)
                    PromocodeField(promocode: $promoName)
                        .padding()
                    Spacer()
                }
            }
            }
        //.frame(height: geometry.size.height)
            VStack {
                Spacer()
                DeliveryTabView(orderCost: orderPrices?.sum ?? orderSum, isActive: Binding<Bool>(get: { viewModel.addresses.count > 1 && viewModel.error == nil && chosenTypeOfPayment != nil }, set: { print($0) })) {
                    viewModel.sendOrder() { order in
                        self.order = order
                        finished = true
                    }
                }
            
                .shadow(color: .clear, radius: 0)
            }
            .ignoresSafeArea(edges: .bottom)
            .shadow(color: Color.gray.opacity(0.2), radius: 10, y: -10)
            }
            .sheet(isPresented: $showDeliveryModal) {
                switch typeOfModal {
                case 3:
                    CardListView()
                    //ChoosePaymentTypeModal(chosenTypeOfPayment: $chosenTypeOfPayment, showModal: $showModal)
                default:
                    DeliveryAddressModal()
                }
                //DeliveryAddressModal()
            }
            .onAppear {
                orderPrices = OrderPrices(meal: orderSum, delivery: viewModel.zone?.price ?? 0)
            }
    }
}

struct PickUpView_Previews: PreviewProvider {
    static var previews: some View {
        PickUpView(order: [], orderSum: 100, items: [], showModal: .constant(true))
    }
}
