//
//  DeliveryAddressModal.swift
//  MyCafe
//
//  Created by Михаил on 24.12.2020.
//

import SwiftUI

struct DeliveryAddressDetails {
    let city: String
    var street: String
    var home: String
    var flat: String
    var comment: String?
//    var location
}

struct DeliveryAddressModal: View {
    var city = "Moscow"
    var address = "Sayanskaya 14, 20"
    var addressComment = "Domophone is not working"
    var body: some View {
        ModalPatternWithPlusButton(content: AnyView(
            ScrollView(.vertical) {
                Spacer()
                AddressCard(address: DeliveryAddress(id: nil, isDefault: 0, address: address, apartment: "", intercom: "", floor: "", entrance: "", comment: addressComment, lat: "", lng: ""), city: city)
                Spacer()
            }
        ), modalContent: AnyView(
            NewAddressEnter()
        ), title: "Адрес доставки", plusMethod: addNewAddress)
    }
}

struct NewAddressEnter: View {
    @State var dismiss: Bool = false
    @State var address: String = ""
    @State var isEditing: Bool = false
    var body: some View {
        GeometryReader { geometry in
            VStack {
                HStack {
                    Image("magnifyingglass")
                        .font(.system(size: 18))
                        .foregroundColor(.themeColor)
                    TextField("Введите адрес", text: $address, onEditingChanged: {_ in isEditing = true }, onCommit: { isEditing = false})
                        .foregroundColor(.gray)
                        .frame(height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    if isEditing {
                        Button(action: {
                        
                        }) {
                            Text("Отменить")
                        }
                        .buttonStyle(PlainButtonStyle())
                    }
                }
                Divider()
                
                AddressSearchListView(searchedText: $address, isEditing: $isEditing)
            }
            .padding(.top)
            .padding()
        }
    }
}

struct AddressSearchListView: View {
    @Binding var searchedText: String
    @Binding var isEditing : Bool
    
    var nothingIsFound: Bool {
        searchedText.count < 3
//            || dishes.filter { $0.title.contains(searchedText) }.count < 1
    }
    
    
    var body: some View {
        VStack {
            AddressSearchCell(street: "Sayanskaya", house: "20k1", city: "Moscow", country: "Russia")
//            if isEditing && !nothdingIsFound {
//                ForEach(dishes.filter({searchedText.isEmpty ? false : $0.title.contains(searchedText)})) { item in
//                    Text("addressStreet")
//                }
//                Spacer()
//            } else {
//                if searchedText.count < 3 {
//                    Spacer()
//                } else {
//                    Spacer()
//                    Text("К сожалению по вашему поиску мы ничего не нашли")
//                        .font(header1FontRubik)
//                        .foregroundColor(.gray)
//                        .padding(.leading, 10)
//                    Spacer()
//                }
//            }
        }
        .padding(.top, 20)
        .frame(width: UIScreen.main.bounds.width, alignment: .leading)
    }
}

struct AddressSearchCell: View {
    var street: String
    var house: String
    var city: String
    var country: String
    var body: some View {
        VStack {
            Text("\(street), \(house)")
                .font(body1FontRubikMedium)
            Text("\(city), \(country)")
                .font(body1FontRubikRegular)
                .foregroundColor(.gray)
            Spacer()
            Divider()
        }
        .frame(height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
    }
}

func addNewAddress(showModal: Bool) -> (Bool) {
    
    let delivery = DeliveryAddressDetails(city: "Moscow", street: "Sayanskaya", home: "21", flat: "20", comment: "Domophone is not working")
    
    print(delivery)
    return !showModal
}

struct DeliveryAddressModal_Previews: PreviewProvider {
    static var previews: some View {
        DeliveryAddressModal()
    }
}
