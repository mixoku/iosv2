//
//  EnterTimeModal.swift
//  CafeApp
//
//  Created by Михаил on 03.02.2021.
//

import SwiftUI

struct EnterTimeModal: View {
    @Binding var chosenTime: Int
    
    var minimalDeliveryTime = 50
    var currentDateTime = Date()
    
    var timeOptions: [Int] {
        return [minimalDeliveryTime, minimalDeliveryTime+30, minimalDeliveryTime+60, minimalDeliveryTime+90] }
    
    var timeCounter: [Date] {
        return [ currentDateTime.addingTimeInterval(TimeInterval(timeOptions[0])), currentDateTime.addingTimeInterval(TimeInterval(timeOptions[1])), currentDateTime.addingTimeInterval(TimeInterval(timeOptions[2])), currentDateTime.addingTimeInterval(TimeInterval(timeOptions[3]))
        ]
    }
    
    var body: some View {
        ZStack {
            ScrollView(.vertical) {
                ForEach(0..<timeOptions.count) { time in
                    RoundedBubbleButtonWithShadow(content: AnyView(
                            HStack {
                                Text("через \(timeOptions[time]) минут")
//                              Text("(\(timeCounter[time])")
                                Spacer()
                                if timeOptions[time] == chosenTime {
                                    Image(systemName: "checkmark")
                                        .font(.system(size: 25))
                                }

                            }
                        )
                    )
                    .onTapGesture {
                        chosenTime = timeOptions[time]
                    }
                }
            }
            VStack {
                Spacer()
                Button(action: {
                    
                }) {
                    TabViewButtonPattern(title: "Выбрать")
                }
                
            }
        }
        
    }
}

struct EnterTimeModal_Previews: PreviewProvider {
    static var previews: some View {
        EnterTimeModal(chosenTime: .constant(50))
    }
}
