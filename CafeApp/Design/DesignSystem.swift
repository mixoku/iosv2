//
//  DesignSystem.swift
//  MyCafe
//
//  Created by Михаил on 11.12.2020.
//

import SwiftUI

let header1FontRubik: Font = Font.custom("Rubik-Bold", size: 32)
let header2FontRubikBold: Font = Font.custom("Rubik-Bold", size: 24)
let header2FontRubikRegular: Font = Font.custom("Rubik-Regular", size: 24)
let body1FontRubikMedium: Font = Font.custom("Rubik-Medium", size: 16)
let body1FontRubikRegular: Font = Font.custom("Rubik-Regular", size: 16)
let body2FontRubik: Font = Font.custom("Rubik-Regular", size: 12)
