//
//  SmallMenuBanner.swift
//  MyCafe
//
//  Created by Михаил on 05.12.2020.
//

import SwiftUI

struct SmallMenuBanner: View {
    let promo: Promo
    var isBigCard: Bool
    let height: CGFloat

    let cornerRadius: CGFloat = 18

    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: CGFloat(cornerRadius))
            Image(promo.imagePath)
                .resizable()
                .scaledToFill()
                .frame(height: height)
                .clipShape(RoundedRectangle(cornerRadius: CGFloat(cornerRadius)))
            HStack {
                VStack(alignment: .leading) {
                    Text(promo.title)
                        .font(isBigCard ? header2FontRubikBold : body1FontRubikMedium)
                        .foregroundColor(.mainBackgroundColor)
                        .background(Color.themeColor)
//                    Text(promo.subtitle)
//                        .foregroundColor(.white)
//                        .font(body2FontRubik)
                    Spacer()
                }
                .padding()
                Spacer()
            }
        }
        .foregroundColor(.themeColor)
        .frame(width: 250, height: height)
    }
}

struct SmallMenuBanner_Previews: PreviewProvider {
    static var previews: some View {
        SmallMenuBanner(promo: promosSample[0], isBigCard: true, height: 146)
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
            .previewDisplayName("Small Banner")
    }
}
