//
//  Images.swift
//  CafeApp
//
//  Created by Alexey Antonov on 24/01/21.
//

import SwiftUI

struct PhotoRoundedRectangle: View {
    var imageName: String
    var width: CGFloat
    var height: CGFloat
    var cornerRadius: CGFloat
    
    var body: some View {
            Image(imageName)
                .resizable()
                .scaledToFill()
                .frame(width: width, height: height)
                .cornerRadius(cornerRadius)
                //.clipShape(RoundedRectangle(cornerRadius: cornerRadius))
    }
}

struct ImageWithLikeModal: View {
    var imageName = "imageExample"
    var heightOfImage : CGFloat = 200
    
    var body: some View {
            ZStack {
                Image(imageName)
                    .resizable()
                    .scaledToFill()
                    .frame(height: heightOfImage)
                    .clipped()
                VStack {
                    HStack(alignment: .top) {
                        Spacer()
                        LikeButton()
                            .padding()
                    }
                    Spacer()
                }
        }
    }
}
