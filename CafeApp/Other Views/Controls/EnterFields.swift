//
//  EnterFields.swift
//  CafeApp
//
//  Created by Alexey Antonov on 24/01/21.
//

import SwiftUI

struct FieldToEnter: View {
    var titleIsText: Bool = true
    var title : String
    @Binding var mainText : String
    
    @State var heightOfField : CGFloat = 45
    var body: some View {
        ZStack {
            Rectangle()
                .frame(height: heightOfField)
                .foregroundColor(.clear)
            VStack {
                HStack {
                    VStack(alignment: .leading, spacing: 5) {
                        Text(titleIsText ? title : "")
                            .font(body2FontRubik)
                            .foregroundColor(.gray)
                        
                        TextField(titleIsText ? "" : title, text: $mainText)
                            .font(body1FontRubikRegular)
                            .foregroundColor(.mainAccentColor)
                        Spacer()
                    }
                    Spacer()
//                    Image(systemName: "chevron.down")
//                        .font(.system(size: 25))
//                        .foregroundColor(.themeColor)
                }
                Divider()
            }
        }
        .frame(height: heightOfField)
        .padding(.bottom, 10)
    }
}

struct TimeEnterField: View {
    var error: AppError?
    var timeTillDelivery: Int = 45
    var body: some View {
        ZStack {
            RoundedBubbleButtonWithShadow(content: AnyView(
                HStack {
                    Text(error == nil ? "~\(timeTillDelivery) минут" : error!.description)
                    .font(body1FontRubikMedium)
                    Spacer()
                }
            ))
        }
    }
}

struct CommentField: View {
    @Binding var mainText: String
    var placeholder: String
    
    var body: some View {
        ZStack {
            Rectangle()
                .frame(height: 55)
                .foregroundColor(.clear)
            VStack {
                HStack {
//                    VStack {
//                        Text(title)
//                            .font(body2FontRubik)
//                            .foregroundColor(.gray)
                        
                        TextField(placeholder, text: $mainText)
                            .font(body1FontRubikRegular)
                            .foregroundColor(.gray)
//                    }
                    Spacer()
//                    Image(systemName: "chevron.down")
//                        .font(.system(size: 30))
//                        .foregroundColor(.themeColor)
                }
                Divider()
            }
            
        }
    }
}

struct PromocodeField: View {
    @Binding var promocode: String
    var title = "Промокод"
    
    var body: some View {
        ZStack {
            HStack {
                VStack {
                    Spacer()
                    TextField(title, text: $promocode)
                        .font(body1FontRubikRegular)
                        .foregroundColor(Color.gray)
                    Divider()
                }
                Button(action: {
                    
                }) {
                    RoundedRectangleFilledButton(title: "Применить", fontOfTitle: body2FontRubik, colorOfButton: Color.themeColor.opacity(0.2), colorOfTitle: Color.themeColor)
                        .frame(width: 105, height: 42)
                        
                }
            }
            .frame(height: 50, alignment: .center)
            .padding(.bottom, 10)
        }
    }
}

struct CodeFieldView: View {
    var maxDigits: Int = 6
    var label = "Введите код ресторана"
    @State var pin: String = ""
    @State var showPin = true
    @State var isDisabled = false
    
    @State var isFilled = false
    
    var handler: (String, (Bool) -> Void) -> Void
    
    public var body: some View {
        VStack {
//            HStack {
//                Text(label)
//                    .font(header2FontRubikRegular)
//                    .frame(height: 100)
//                    .multilineTextAlignment(.leading)
//                Spacer()
//            }
//            .padding(.bottom)
            ZStack {
                pinDots
                backgroundField
            }
            Spacer()
//            showPinStack
        }
        .onTapGesture {
            isDisabled = !isDisabled && isFilled
        }
    }
    
    private var pinDots: some View {
        HStack {
            Spacer()
            ForEach(0..<maxDigits) { index in
                ZStack {
                    RoundedRectangle(cornerRadius: 15)
                        .frame(width: 50, height: 60)
                        .foregroundColor(Color.gray.opacity(0.5))
                    Text(String(self.getImageName(at: index)))
                        
                        .font(.system(size: 40))
//                    Image(systemName: self.getImageName(at: index))
//                        .font(.system(size: 40, weight: .thin, design: .default))
                        .foregroundColor(.mainBackgroundColor)
                }
                Spacer()
            }
        }
    }
    
    private var backgroundField: some View {
        let boundPin = Binding<String>(get: {self.pin}, set: { newValue in
            self.pin = newValue
            self.submitPin()
        })
        
        return TextField("", text: boundPin, onCommit: submitPin)
            .accentColor(.clear)
            .foregroundColor(.clear)
            .keyboardType(.numberPad)
            .disabled(isDisabled)
    }
    
//    private var showPinStack: some View {
//        HStack {
//            Spacer()
//            if !pin.isEmpty {
//                showPinButton
//            }
//        }
//        .frame(height: 100)
//        .padding([.trailing])
//    }
    
//    private var showPinButton: some View {
//        Button(action: {
//            self.showPin.toggle()
//        }, label: {
//            self.showPin ?
//                Image(systemName: "eye.slash.fill").foregroundColor(.primary) : Image(systemName: "eye.fill").foregroundColor(.primary)
//        })
//    }
    
    private func submitPin() {
        guard !pin.isEmpty else {
//            showPin = false
            return
        }
        
        if pin.count == maxDigits {
            isDisabled = true
            
            handler(pin) { isSuccess in
                if isSuccess {
                    print("pin matched")
                } else {
                    pin = ""
                    isDisabled = false
                    print("its failure case")
                }
            }
        } else {
            isDisabled = false
        }
        
        if pin.count > maxDigits {
            pin = String(pin.prefix(maxDigits))
            submitPin()
            isFilled = true
        } else {
            isFilled = false
        }
    }
    
//    in case we need circles with background
    private func getImageName(at index: Int) -> String {
        if index >= self.pin.count {
            return " "
        }

        if self.showPin {
            for i in 1...maxDigits {
                if index < self.pin.digits.count {
                    return String(self.pin.digits[index].numberString)
                } else {
                    return "."
                }
            }
        }
        return "."
    }
}

extension String {
    var digits: [Int] {
        var result = [Int]()
        
        for char in self {
            if let number = Int(String(char)) {
                result.append(number)
            }
        }
        
        return result
    }
}

extension Int {
    var numberString: String {
        guard self < 10 else {
            return "0"
        }
        return String(self)
    }
}



struct FieldToEnter_Previews: PreviewProvider {
    static var previews: some View {
        FieldToEnter(titleIsText: true, title: "Введите номер", mainText: .constant(""))
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
        FieldToEnter(titleIsText: false, title: "Введите номер", mainText: .constant(""))
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
        
        TimeEnterField()
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
        
        CommentField(mainText: .constant(""), placeholder: "comment")
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
        
        PromocodeField(promocode: .constant(""), title: "comment")
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
        
        CodeFieldView(maxDigits: 6, label: "Введите код из СМС", pin: "", showPin: true, isDisabled: false, isFilled: false) { otp, completionHandler in
//             check if the otp is correct here
//            viewModel.restaurantCode = otp
//            if isCorrect(otp) { // this could be a network call
//                completionHandler(true)
//                flag = true
//            }    else {
//                completionHandler(false)
//                flag = false
//            }
        }
//        .frame(width: 300, height: 140)
        .padding()
    }
}
