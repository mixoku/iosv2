//
//  OrderSwitcher.swift
//  MyCafe
//
//  Created by Михаил on 18.12.2020.
//

import SwiftUI

struct OrderSwitcher: View {
    @Binding var chosenOrderType: Int
    
    private static let ActiveSegmentColor: Color = Color.themeColor
    private static let BackgroundColor: Color = Color.clear
    private static let ShadowColor: Color = Color.black.opacity(0.2)
    private static let TextColor: Color = Color(.secondaryLabel)
    private static let SelectedTextColor: Color = .mainBackgroundColor
    
    private static let TextFont: Font = body1FontRubikMedium
    
    private static let SegmentCornerRadius: CGFloat = 12
    private static let ShadowRadius: CGFloat = 4
    private static let SegmentXPadding: CGFloat = 16
    private static let SegmentYPadding: CGFloat = 8
    private static let PickerPadding: CGFloat = 4
        
    private static let AnimationDuration: Double = 0.3
    
    private let items: [String]
    
    init(items: [String], chosenOrderType: Binding<Int>) {
        self._chosenOrderType = chosenOrderType
        self.items = items
    }
    
    var body: some View {
        ZStack(alignment: .leading) {
            RoundedRectangle(cornerRadius: OrderSwitcher.SegmentCornerRadius)
                .foregroundColor(Color.gray.opacity(0.2))
                .frame(minWidth: 0, maxWidth: .infinity, maxHeight: self.segmentSize.height + OrderSwitcher.PickerPadding)
            self.activeSegmentView
            HStack {
                ForEach(0..<self.items.count, id:\.self) { index in
                    self.getSegmentView(for: index)
                }
            }
            .background(OrderSwitcher.BackgroundColor)
            .clipShape(RoundedRectangle(cornerRadius: OrderSwitcher.SegmentCornerRadius))
            
        }
    }
    
    @State private var segmentSize: CGSize = .zero
    
    
    
    
    private var activeSegmentView: AnyView {
        let isInitialized: Bool = segmentSize != .zero
        if !isInitialized { return EmptyView().eraseToAnyView()}
        
        return RoundedRectangle(cornerRadius: OrderSwitcher.SegmentCornerRadius)
            .foregroundColor(OrderSwitcher.ActiveSegmentColor)
            .frame(width: self.segmentSize.width, height: self.segmentSize.height)
            .offset(x: self.computeActiveSegmentHorizontalOffset(), y: 0)
            .animation(Animation.linear(duration: OrderSwitcher.AnimationDuration))
            .eraseToAnyView()
    }

    private func computeActiveSegmentHorizontalOffset() -> CGFloat {
        CGFloat(CGFloat(self.chosenOrderType) * (self.segmentSize.width + OrderSwitcher.SegmentXPadding / 2 ))
    }
    
    private func getSegmentView(for index: Int) -> some View {
        guard index < self.items.count else {
            return EmptyView().eraseToAnyView()
        }
        let isSelected = self.chosenOrderType == index
        return
            Text(self.items[index])
            .foregroundColor(isSelected ? OrderSwitcher.SelectedTextColor : OrderSwitcher.TextColor)
            .font(OrderSwitcher.TextFont)
            .lineLimit(1)
            .padding(.vertical, OrderSwitcher.SegmentYPadding)
            .padding(.horizontal, OrderSwitcher.SegmentXPadding)
            .frame(minWidth: 0, maxWidth: .infinity)
            .modifier(SizeAwareViewModifier(viewSize: self.$segmentSize))
            .onTapGesture { self.onItemTap(index: index) }
            .eraseToAnyView()
    }
    
    private func onItemTap(index: Int) {
        guard index < self.items.count else {
            return
        }
        self.chosenOrderType = index
    }
}

struct SizePreferenceKey: PreferenceKey {
    typealias Value = CGSize
    static var defaultValue: CGSize = .zero
    static func reduce(value: inout CGSize, nextValue: () -> CGSize) {
        value = nextValue()
    }
}

struct BackgroundGeometryReader: View {
    var body: some View {
        GeometryReader { geometry in
            return Color.clear
                .preference(key: SizePreferenceKey.self, value: geometry.size)
        }
    }
}

struct SizeAwareViewModifier: ViewModifier {

    @Binding private var viewSize: CGSize

    init(viewSize: Binding<CGSize>) {
        self._viewSize = viewSize
    }

    func body(content: Content) -> some View {
        content
            .background(BackgroundGeometryReader())
            .onPreferenceChange(SizePreferenceKey.self, perform: { if self.viewSize != $0 { self.viewSize = $0 }})
    }
}

extension View {
    func eraseToAnyView() -> AnyView {
        AnyView(self)
    }
}

struct OrderSwitcher_Previews: PreviewProvider {
    
    static var previews: some View {
        OrderSwitcher(items: ["L", "F"], chosenOrderType: .constant(1))
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
            .previewDisplayName("Order Switcher")
    }
}
