//
//  Buttons.swift
//  CafeApp
//
//  Created by Alexey Antonov on 24/01/21.
//

import SwiftUI

struct AddressViewButton: View {
    var systemImage = false
    let height: CGFloat
    var imageName = "location.cafe"
    var text = "Мои адреса"
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 16)
                .foregroundColor(Color.gray.opacity(0.2))
            HStack {
                if systemImage {
                    Image(systemName: imageName)
                        .foregroundColor(.gray)
                } else {
                    Image(imageName)
                        .foregroundColor(.gray)
                }
                
                Text(text)
                    .font(body1FontRubikMedium)
                    .foregroundColor(.mainAccentColor)
                Spacer()
                Image(systemName: "chevron.right")
                    .foregroundColor(.themeColor)
                    .font(.system(size: 25))
                    
            }
            .padding()
        }
        .frame(height: height)
        
    }
}

struct OrderHistoryRoundedRectangleButton: View {
    
    var cornerRadius: CGFloat = 16
    var backgroundColor: Color
    var title: String
    var textColor: Color
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: cornerRadius)
                .frame(height: 51, alignment: .center)
                .foregroundColor(backgroundColor)
            HStack {
                Text(title)
                    .font(body1FontRubikMedium)
                    .padding(.horizontal)
                    .foregroundColor(textColor)
            }

        }
        .frame(height: 51, alignment: .center)
        .foregroundColor(.mainBackgroundColor)
//        .padding(.horizontal, 5)
        
    }
}

struct AddToCartButton: View {
    var imageName: String = "GroceryCart"
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 16)
                .frame(width: 48, height: 48)
                .foregroundColor(.themeColor)
            Image(imageName)
                .font(.system(size: 25))
                .foregroundColor(.mainBackgroundColor)
        }
    }
}

struct RefreshButton: View {
    var imageName: String = "goforward"
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 16)
                .frame(width: 48, height: 48)
                .foregroundColor(.themeColor)
            Image(systemName: imageName)
                .font(.system(size: 25))
                .foregroundColor(.mainBackgroundColor)
        }
    }
}

struct RoundedRectangleButton: View {
    var orderCost: Int
    var title: String
    var cornerRadius: CGFloat = 16
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: cornerRadius)
                .foregroundColor(.themeColor)
            HStack {
                Text(title)
                Spacer()
                Text(String(orderCost) + "₽")
                    
            }
            .font(body1FontRubikMedium)
            .padding()
            
        }
        .foregroundColor(.mainBackgroundColor)
        .padding(.horizontal, 30)
    }
}

struct MenuButton: View {
    var height: CGFloat = 70
    var imageName: String
    var itemName: String
    
    var body: some View {
        
            HStack {
                Image(imageName)
                    .font(.system(size: 24))
                    .foregroundColor(.themeColor)
                    .frame(width: 40)
                    .padding(.horizontal, 15)
                    
                Text(itemName)
                    .font(.system(size: 20))
                    .foregroundColor(.mainAccentColor)
                    .bold()
                Spacer()
            }
        .frame(height: height)
    }
}

struct RoundedButton: View {
    var height: CGFloat = 45
    var isWithImage: Bool
    var imageName: String = "message"
    var label: String = "Чат"
    
    @State var cornerRadius: CGFloat = 12
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: cornerRadius)
                .foregroundColor(Color.gray.opacity(0.2))
                .frame(height: height)
            HStack {
                if isWithImage {
                    Image(systemName: imageName)
                        .foregroundColor(.gray)
                        .font(.system(size: 20))
                }
                if !label.isEmpty {
                    Text(label)
                        .bold()
                }
            }.padding(.horizontal, 45)
        }
    }
}

struct PromoDetailButton: View {
    var width: CGFloat
    var height: CGFloat
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 16)
                .foregroundColor(Color.gray.opacity(0.2))
            HStack {
                Image(systemName: "location")
                    .foregroundColor(.gray)
                Text("Мои адреса")
                    .font(body1FontRubikMedium)
                    .foregroundColor(.mainAccentColor)
                Spacer()
                Image(systemName: "chevron.right")
                    .foregroundColor(.themeColor)
                    .font(.system(size: 25))
                    
            }
            .padding()
        }
        .frame(width: width, height: height)
        
    }
}

struct OrderRoundedRectangleButton: View {
    var cornerRadius: CGFloat = 16
    var title: String = "Далее"
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: cornerRadius)
                //.frame(height: 51, alignment: .center)
                .foregroundColor(.themeColor)
            HStack {
                Text(title)
                    .font(body1FontRubikMedium)
                    //.padding([.leading, .trailing])
                    .foregroundColor(.mainBackgroundColor)
            }

        }
        //.frame(height: 51, alignment: .center)
        .foregroundColor(.mainBackgroundColor)
        //.padding(.horizontal, 30)
        
    }
}

struct LikeButton: View {
    @State var size: CGFloat = 50
    @State var isLiked: Bool = false
//    @State var colorF = Color(red: 1, green: 0.5, blue: 0.4, opacity: 1)
    
    var body: some View {
        ZStack {
            Circle()
                .frame(width: size, height: size, alignment: .center)
                .foregroundColor(.mainBackgroundColor)
            Image(systemName: isLiked ? "heart.fill" : "heart")
                .font(.system(size: size - 23))
                .foregroundColor(.themeColor)
        }
        .onTapGesture {
            isLiked.toggle()
        }
        
        
        
    }
}

struct CancelButton: View {
    @State var size: CGFloat = 50
//    @State var isLiked: Bool = false
//    @State var colorF = Color(red: 1, green: 0.5, blue: 0.4, opacity: 1)
    
    var body: some View {
        ZStack {
            Circle()
                .frame(width: size, height: size, alignment: .center)
                .foregroundColor(.mainBackgroundColor)
            Image(systemName: "multiply")
                .font(.system(size: size - 23))
                .foregroundColor(.themeColor)
        }
    }
}

struct PlusMinusButton: View {
    @Binding var count: Int
    
    var body: some View {
        HStack {
            Button(action: {
                count -= 1
            }) {
                ZStack {
                    RoundedRectangle(cornerRadius: 12)
                        .foregroundColor(.themeColor)
                        .opacity(0.5)
                        .frame(width: 40, height: 40)
                    Text("-")
                        .font(body1FontRubikMedium)
                        .foregroundColor(.themeColor)
                }
            }
            
            ZStack {
                Text("\(count)")
                    .font(body1FontRubikMedium)
                Spacer()
            }
            .frame(width: 38)
            Button(action: {
                count += 1
                
            }) {
                ZStack {
                    RoundedRectangle(cornerRadius: 12)
                        .foregroundColor(.themeColor)
                        .opacity(0.5)
                        .frame(width: 40, height: 40)
                    Text("+")
                        .font(body1FontRubikMedium)
                        .foregroundColor(.themeColor)
                }
            }
            
        }
    }
}

struct RoundedBubbleButtonWithShadow: View {
    var content: AnyView
    var colorbackground: Color = .mainBackgroundColor
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 16)
                .foregroundColor(colorbackground)
                .shadow(color: Color.gray.opacity(0.2), radius: 20)
            content
                .padding()
        }
    }
    
}

struct RoundedRectangleFilledButton: View {
    var title: String
    var fontOfTitle: Font
    var colorOfButton: Color
    var colorOfTitle: Color
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 16)
//                .frame(width: 105, height: 42)
                .foregroundColor(colorOfButton)
            Text(title)
                .foregroundColor(colorOfTitle)
                .font(fontOfTitle)
                .padding(5)
        }
    }
}

struct AddRestaurantCardButton: View {
    var colorOfButton: Color
    var colorOfTitle: Color
    var height: CGFloat = 209
    var width: CGFloat = 303
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 16)
//                .frame(width: 105, height: 42)
                .foregroundColor(colorOfButton)
            Text("+")
                .foregroundColor(colorOfTitle)
                .font(.system(size: 100))
                
        }
        .frame(width: width, height: height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
    }
}

struct AddressViewButton_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            AddressViewButton(height: 100)
                .previewLayout(PreviewLayout.sizeThatFits)
                .padding()
            
            OrderHistoryRoundedRectangleButton(backgroundColor: Color.themeColor, title: "Order", textColor: Color.mainBackgroundColor)
                .previewLayout(PreviewLayout.sizeThatFits)
                .padding()
            
            AddToCartButton()
                .previewLayout(PreviewLayout.sizeThatFits)
                .padding()
            
            RefreshButton()
                .previewLayout(PreviewLayout.sizeThatFits)
                .padding()
            
            RoundedRectangleButton(orderCost: 100, title: "rounded")
                .previewLayout(PreviewLayout.sizeThatFits)
                .padding()
            
            MenuButton(height: 133, imageName: "cube.box", itemName: "Russia")
                .previewLayout(PreviewLayout.sizeThatFits)
                .padding()
            
            LikeButton()
                .previewLayout(PreviewLayout.sizeThatFits)
                .padding()
            
            CancelButton()
                .previewLayout(PreviewLayout.sizeThatFits)
                .padding()
            
            PlusMinusButton(count: .constant(1))
                .previewLayout(PreviewLayout.sizeThatFits)
                .padding()
            
            RoundedBubbleButtonWithShadow(content: AnyView(Text("Button")), colorbackground: .themeColor)
                .previewLayout(PreviewLayout.sizeThatFits)
                .padding()
        }
        
        RoundedRectangleFilledButton(title: "Title", fontOfTitle: body1FontRubikMedium, colorOfButton: Color.themeColor, colorOfTitle: Color.mainBackgroundColor)
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
        
        AddRestaurantCardButton(colorOfButton: Color.gray.opacity(0.5), colorOfTitle: .mainBackgroundColor)
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
    }
}
