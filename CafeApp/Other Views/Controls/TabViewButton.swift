//
//  TabViewButton.swift
//  CafeApp
//
//  Created by Михаил on 03.02.2021.
//

import SwiftUI

struct TabViewButtonPattern: View {
    var title: String = "Далее"
    var body: some View {
        ZStack {
            Rectangle()
//                .foregroundColor(.white)
                .frame(height: 90)
            VStack {
                Divider()
                HStack {
                    OrderRoundedRectangleButton(title: title)
                    .padding()
                }
                Spacer()
            }
        }
        .frame(height: 70)
        .foregroundColor(Color.mainBackgroundColor)
        .ignoresSafeArea(edges: .bottom)
    }
}


struct TabViewButtonPattern_Previews: PreviewProvider {
    static var previews: some View {
        TabViewButtonPattern()
    }
}
