import SwiftUI

struct ModalPatternWithPlusButton: View {
    var content: AnyView
    var modalContent: AnyView
    var title: String
    var plusMethod: (Bool) -> (Bool)
    @State var showModal: Bool = false
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                HStack {
                    Text(title)
                        .font(header2FontRubikBold)
                        .lineLimit(2)
                    Spacer()
                    Button(action: {
                        showModal = plusMethod(showModal)
                    }
                        
                    ) {
                        ZStack {
                            Circle()
                                .foregroundColor(.themeColor)
                                .frame(width: 80, height: 80, alignment: .center)
                            Image(systemName: "plus")
                                .foregroundColor(.mainBackgroundColor)
                                .font(.system(size: 30))
                        }
                    }
                }
                .background(Color.mainBackgroundColor.opacity(0.01))
                content
            }
            .padding()
        }
        .sheet(isPresented: $showModal, content: {modalContent})
    }
    
}
