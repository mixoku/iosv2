//
//  ModalType.swift
//  CafeApp
//
//  Created by Alexey Antonov on 24/01/21.
//

import SwiftUI

enum ModalType: Identifiable, Equatable {
    static func == (lhs: ModalType, rhs: ModalType) -> Bool {
        lhs.id == rhs.id
    }
    
    var id: Int {
        switch self {
        case .menu:
            return 0
        case .detail(_):
            return 1
        case .promo(_):
            return 2
        case .cart:
            return 3
        }
    }
    
    case menu
    case detail(dish: Dish)
    case promo(promo: Promo)
    case cart
}

extension Optional where Wrapped == ModalType {
    var isShown: Bool {
        switch self {
        case .none:
            return false
        default:
            return true
        }
    }
    
    init(show: Bool) {
        self = show ? .menu : .none
    }
}
