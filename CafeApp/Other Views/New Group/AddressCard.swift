//
//  AddressCard.swift
//  CafeApp
//
//  Created by Alexey Antonov on 25/01/21.
//

import SwiftUI

struct AddressCard: View {
    let address: DeliveryAddress
    let city: String
    
    
    
    var body: some View {
        RoundedBubbleButtonWithShadow(content: AnyView(
            VStack(alignment: .leading, spacing: 10) {
                HStack {
                    Image("location.cafe")
                        .font(.system(size: 16))
                        .foregroundColor(.themeColor)
                    Text(city)
                        .font(body1FontRubikRegular)
                    Spacer()
                }
                
                Text("\(city), \(address.address)\(address.apartment == nil ? "" : ", " + address.apartment!)")
                    .font(body1FontRubikMedium)
                if let comment = address.comment, comment != "" {
                    Text(comment)
                        .font(body2FontRubik)
                        .foregroundColor(Color.gray.opacity(0.8))
                        .lineLimit(2)
                }
                
            }
        ))
    }
}

//struct AddressCard_Previews: PreviewProvider {
//    static var previews: some View {
//        AddressCard(address: DeliveryAddress(id: 1, isDefault: 0, address: "", apartment: <#T##String?#>, intercom: <#T##String?#>, floor: <#T##String?#>, entrance: <#T##String?#>, comment: <#T##String?#>, lat: <#T##String?#>, lng: <#T##String?#>), city: <#T##String#>)
//    }
//}
