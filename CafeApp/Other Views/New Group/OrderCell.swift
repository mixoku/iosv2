//
//  OrderCell.swift
//  MyCafe
//
//  Created by Михаил on 15.12.2020.
//

import SwiftUI

struct OrderCell: View {
    @Binding var item: CartOrderItem
    
    var body: some View {
        HStack {
            AsyncImage(urlString: item.dish.images.first?.images.small ?? "", placeholder: {
                Image("imageExample")
                    .resizable()
            })
                .scaledToFill()
                .frame(width: 98, height: 98)
                .clipShape(RoundedRectangle(cornerRadius: CGFloat(12)))
                .layoutPriority(1)
            VStack(alignment: .leading) {
                Text(item.dish.title)
                    .font(body1FontRubikMedium)
                    .lineLimit(3)
                PlusMinusButton(count: $item.count)
                Spacer()
            }.layoutPriority(1)
            Spacer()
            VStack(alignment: .trailing) {
                Text("\(item.totalPrice) ₽")
                    .font(body1FontRubikMedium)
                    .padding(5)
                Spacer()
            }
        }
        .frame(height: 98)
    }
}



struct OrderCell_Previews: PreviewProvider {
    static var previews: some View {
        OrderCell(item: .constant(CartOrderItem(dish: pizza, count: 1, options: [])))
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
            .previewDisplayName("Order Dish Cell")
    }
}
