//
//  DishCell.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import SwiftUI

struct CellOfDish: View {
    @EnvironmentObject var orderStore: OrderStore
    var cornerRadius: Int = 50
    var cornerRadiusOfBuyButton: CGFloat = 16
    
    var dish: Dish
    @State var amount: Int = 0
    
    var body: some View {
            HStack {
                AsyncImage(urlString: dish.images.first?.images.small ?? "", placeholder: {
                    Image("imageExample")
                        .resizable()
                })
                .scaledToFill()
                //.aspectRatio(1.0, contentMode: .fit)
                .frame(width: 98, height: 98)
                .clipped()
                .cornerRadius(cornerRadiusOfBuyButton)
                .padding(.vertical, 15)
                VStack(alignment: .leading, spacing: 10) {
                    Text(dish.title)
                        .font(body1FontRubikMedium)
                        .foregroundColor(.mainAccentColor)
                    if dish.weight != nil {
                        if dish.weight! > 0 {
                            Text("\(dish.weight!) \(dish.measure)")
                                .font(body2FontRubik)
                                .foregroundColor(.gray)
                        }
                    }
                    Text("\(dish.price) ₽")
                        .font(body1FontRubikMedium)
                }.padding(.leading, 15)
                Spacer()
                Button(action: {
                    orderStore.addToCart(dish: dish)
                }) {
                    AddToCartButton()
                }
                .frame(width: 48, height: 72)
                .buttonStyle(PlainButtonStyle())
                .padding(.vertical, 15)
            }
            //.frame(height: heightOfCard)
    }
}
