//
//  OrderCard.swift
//  CafeApp
//
//  Created by Alexey Antonov on 24/01/21.
//

import SwiftUI

struct OrderCard: View {
    @State var cardIsOpened: Bool = false
    
    var cornerRadius : CGFloat = 16
    
    let order: Order
    //var width: CGFloat
    var orderIsFinished : Bool {
        order.status == "delivered"
    }
    var dateAndTimeOfOrder: String {
        "\(order.createdAt ?? 0)"
    }
    var listOfDishes : String {
        var list = ""
        for dish in order.dishes {
            if !list.isEmpty {
                list.append(", ")
            }
            list.append("\(dish.title ?? "")")
            
        }
        
        return list
    }
    

    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: cornerRadius)
                .foregroundColor(Color.gray.opacity(0.1))
            VStack {
                HStack {
                    Text(order.statusDescription)
                        .font(body1FontRubikMedium)
                        .foregroundColor(.mainAccentColor)
                    if orderIsFinished {
                        Text(" - \(dateAndTimeOfOrder)")
                    }
                    Spacer()
                    Text("\(order.sum ?? 0) ₽")
                        .font(body1FontRubikMedium)
                        .foregroundColor(.themeColor)
                        .padding(.trailing, 7)
                }
                .padding()
                //Spacer()
                HStack(alignment: .top) {
                    if cardIsOpened {
                        VStack {
                            ForEach (0 ..< order.dishes.count) { dish in
                                HStack {
                                    Text(order.dishes[dish].title ?? "")
                                        .font(body2FontRubik)
                                        .foregroundColor(.gray)
                                    Spacer()
                                    Text("\(order.dishes[dish].count) x \(String(order.dishes[dish].price ?? 0))₽")
                                        .font(body2FontRubik)
                                        .foregroundColor(.mainAccentColor)
                                }
                                .padding(.bottom, 5)
                            }
                            .padding(.bottom, 10)
                            HStack {
                                VStack(alignment: .leading) {
                                    Text("Адрес доставки:")
                                        .foregroundColor(.gray)
                                        .font(body2FontRubik)
                                        .padding(.bottom, 5)
                                    Text(order.deliveryAddress?.address ?? "")
                                        .font(body1FontRubikMedium)
                                        .foregroundColor(.mainAccentColor)
                                }
                                Spacer()
                                Image(systemName: "chevron.up")
                                    .font(.system(size: 12))
                                    .foregroundColor(.themeColor)
                            }
                            HStack {
                                OrderHistoryRoundedRectangleButton(backgroundColor: Color.themeColor.opacity(0.2), title: "Помощь", textColor: .themeColor)
                                Spacer()
                                OrderHistoryRoundedRectangleButton(backgroundColor: Color.themeColor, title: "Повторить", textColor: .mainBackgroundColor)
                                
                            }
                        }
                    } else {
                        HStack{
                            Text(listOfDishes)
                                .font(body2FontRubik)
                                .foregroundColor(.gray)
                            Spacer()
                            Image(systemName: "chevron.down")
                                .font(.system(size: 12))
                                .foregroundColor(.themeColor)
                        }
                        
                    }
                    Spacer()
                }
                .padding()
            }
        }
        .onTapGesture {
            cardIsOpened.toggle()
        }
    }
}

struct OrderCard_Previews: PreviewProvider {
    static var previews: some View {
        OrderCard(order: Order(id: 1, comment: "ddd", status: "ddd",dishes: [Order.OrderDish(dishId: 2, title: "Адрес доставки:", price: 45, count: 1, options: [Order.OrderDish.Option(dishOptionId: 2)])], deliveryAddressId: 2, sum: 233, deliveryInfo: Order.DeliveryInfo(isFree: true, sum: 299), createdAt: 33, updatedAt: 2, deliveryAddress: Order.Address(address: "Fgjgjd")))
    }
}

