//
//  ErrorView.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import SwiftUI

struct ErrorView: View {
    var error: AppError
    
    init(error: AppError) {
        self.error = error
        print(error)
    }
    
    var body: some View {
        VStack {
            Spacer()
            Image(systemName: error.icon)
                .resizable()
                .frame(width: 63, height: 56, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .scaledToFit()
            Text(error.description)
                .multilineTextAlignment(.center)
                .font(body2FontRubik)
                .foregroundColor(.gray)
            Spacer()
        }
        .foregroundColor(.secondary)
        
    }
}

struct ErrorView_Previews: PreviewProvider {
    static var previews: some View {
        ErrorView(error: .noConnection)
    }
}
