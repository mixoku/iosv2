//
//  LoadingView.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import SwiftUI

struct LoadingView: View {
    
    var body: some View {
        Image(systemName: "hourglass.tophalf.fill")
            .font(.system(size: 75))
            .foregroundColor(.secondary)
            .ignoresSafeArea()
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}
