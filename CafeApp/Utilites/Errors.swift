//
//  Errors.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import Foundation

enum AppError: Error {
    case noConnection
    case badCoding
    case generalError(error: Error)
    case noRestaurant
    case restaurantNotExist
    case outOfZone
    case PaymentFailure
    case LocationDisabled
    case urlerror(code: Int)
    
    var icon: String {
        switch self {
        case .noConnection:
            return "nowifisign"
        default:
            return "exclamationmark.bubble"
        }
    }
    
    var description: String {
        switch self {
        case .noConnection:
            return "Отсутствует соединение \nс интернетом"
        case .noRestaurant:
            return "Не выбран ресторан. Отсканируйте QR-код"
        case .restaurantNotExist:
            return "Выбранного ресторана нет в базе, проверьте QR-код"
        case .outOfZone:
            return "Выбранный адрес находится вне зоны доставки ресторана"
        case .PaymentFailure:
            return "Платёж не прошёл, попробуйте снова"
        case .LocationDisabled:
            return "Сервис геолокации отключен"
        default:
            return "Что-то пошло не так..."
        }
    }
}
