//
//  ViewModel.swift
//  CafeApp
//
//  Created by Alexey Antonov on 29/01/21.
//

import Foundation
import Combine

class ViewModel: ObservableObject {
    @Published var error: AppError? {
        didSet {
            if error != nil {
                print(error)
            }
        }
    }
    
    func fetch<T>(from endpoint: Endpoint, success: @escaping (T)->(), failure: ((Error)->())? = nil) where T: Decodable {
        endpoint
            .fetch()
            .sink { [weak self] completion in
                switch completion {
                case .failure(let error):
                    if error.localizedDescription == "The Internet connection appears to be offline." {
                        self?.error = .noConnection
                    } else {
                        self?.error = .generalError(error: error)
                    }
                    failure?(error)
                case .finished:
                    self?.error = nil
                }
            }
            receiveValue: { success($0) }
            .store(in: &cancellables)
    }
    
    func getHttpResponse(from endpoint: Endpoint, success: @escaping (HTTPURLResponse?)->(), failure: ((URLError)->())? = nil) {
        endpoint
            .getHttpResponse()
            .sink { [weak self] in
                switch $0 {
                case .failure(let error):
                    self?.error = .urlerror(code: error.errorCode)
                    failure?(error)
                case .finished:
                    self?.error = nil
                }
            } receiveValue: { success($0) }
            .store(in: &cancellables)
    }
    
    private var cancellables = Set<AnyCancellable>()
    
    deinit {
        for cancellable in cancellables {
            cancellable.cancel()
        }
    }
}
