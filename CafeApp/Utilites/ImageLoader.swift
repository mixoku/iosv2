//
//  ImageLoader.swift
//  CafeApp
//
//  Created by Alexey Antonov on 22/01/21.
//

import SwiftUI
import Combine
import Foundation

class ImageLoader: ObservableObject {
    @Published var image: UIImage?
    private let url: URL?
    
    private var cancellable: AnyCancellable?

    init(urlString: String) {
        self.url = URL(string: urlString)
    }

    deinit {
        cancel()
    }
    
    func load() {
        if let url = url {
            cancellable = URLSession.shared.dataTaskPublisher(for: url)
                .map { UIImage(data: $0.data)}
                .replaceError(with: nil)
                .receive(on: RunLoop.main)
                .sink(receiveValue: { [weak self] in
                    self?.image = $0
                })
        }
    }

    func cancel() {
        cancellable?.cancel()
    }
}

